﻿using API2Wallet.Class.Standard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace API2Wallet.Models.WebService.Request.CardHistory
{
    public class cmlReqCardHistory
    {
        /// <summary>
        /// รหัสบัตร
        /// </summary>
        [Required(ErrorMessage = cCS.tCS_MsgAtrRequired)]
        public string ptCrdNo { get; set; }

        /// <summary>
        /// สาขา
        /// </summary>
        [Required(ErrorMessage = cCS.tCS_MsgAtrRequired)]
        public string ptBchCode { get; set; }

        /// <summary>
        /// จำนวนรายการที่ต้องการแสดง
        /// </summary>
        [Required(ErrorMessage = cCS.tCS_MsgAtrRequired)]
        public Int32 pnQty { get; set; }

        /// <summary>
        /// Sort การเรียงลำดับข้อมูล 0 = น้อย , 1 = มาก 
        /// </summary>
        [Required(ErrorMessage = cCS.tCS_MsgAtrRequired)]
        public string ptSort { get; set; }
    }
}