﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2Wallet.Models.WebService.Response.Base
{
    public class cmlResResult<Model>
    {
        public string tMsg { get; set; }
        public string tErrDesc { get; set; }
        public Model oItem { get; set; }
        public List<Model> aoItems { get; set; }
        public int nCount { get; set; }
    }
}