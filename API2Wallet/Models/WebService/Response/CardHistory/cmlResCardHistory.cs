﻿using API2Wallet.Models.WebService.Response.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2Wallet.Models.WebService.Response.CardHistory
{
    public class cmlResCardHistory : cmlResBese
    {
        public List<cmlResCardHistoryList> roCrdHistory { get; set; }
    }
}