﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2Wallet.Models.WebService.Response.CardHistory
{
    public class cmlResCardHistoryList
    {
        /// <summary>
        /// ประเภทรายการ
        /// </summary>
        public string ptType { get; set; }

        /// <summary>
        /// วันที่เอกสาร
        /// </summary>
        public DateTime ptDocDate { get; set; }

        /// <summary>
        /// รหัสอ้างอิง
        /// </summary>
        public string ptBchRef { get; set; }

        /// <summary>
        /// เลขที่เอกสาร
        /// </summary>
        public string ptDocRef { get; set; }

        /// <summary>
        /// เครื่องจุดขาย
        /// </summary>
        public string ptPos { get; set; }
    }
}