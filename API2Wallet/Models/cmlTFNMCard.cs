﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2Wallet.Models
{
    public class cmlTFNMCard
    {
        public string FTCrdCode { get; set; }
        public string FTCrdName { get; set; }
        public string FTCrdNameOth { get; set; }
        public DateTime FDCrdStartDate { get; set; }
        public DateTime FDCrdExpireDate { get; set; }
        public DateTime FDCrdDateTopup { get; set; }
        public string FTCrFTCtyCodedCode { get; set; }
        public string FTCtyCode { get; set; }
        public string FTCgpCode { get; set; }
        public decimal FCCrdValue { get; set; }
        public string FTCrdHolderID { get; set; }
        public string FTCrdRefID { get; set; }
        public string FTCrdStaType { get; set; }
        public string FTCrdStaLocate { get; set; }
        public string FTCrdStaActive { get; set; }
        public Int64 FNCtyExpirePeriod { get; set; }
        public int FNCtyExpiredType { get; set; }
        public DateTime? FDCrdLastTopup  { get; set; }
        public string FTCtyStaAlwRet { get; set; }
        public decimal FCCtyTopupAuto { get; set; }
    }
}