﻿using API2Wallet.Class;
using API2Wallet.Class.Log;
using API2Wallet.Class.Standard;
using API2Wallet.Models;
using API2Wallet.Models.WebService.Request.Card;
using API2Wallet.Models.WebService.Request.ChangeCard;
using API2Wallet.Models.WebService.Response.Card;
using API2Wallet.Models.WebService.Response.ChangeCard;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http;

namespace API2Wallet.Controllers
{
    /// <summary>
    /// Change Card information.
    /// </summary>
    [RoutePrefix(cCS.tCS_APIVer + "/Card")]
    public class cCardController : ApiController
    {
        /// <summary>
        ///  เปลี่ยนบัตร Change Card
        /// </summary>
        /// <param name="poPara"></param>
        /// <returns>
        ///     System process status.<br/>
        ///     1   : success.<br/>
        ///     701 : validate parameter model false.
        ///     712 : Not found Card.
        ///     713 : Card Date expired.
        ///     716 : ResetExpire card unsuccess.".
        ///     802 : formate data incorrect..<br/>
        ///     900 : service process false.<br/>
        ///     904 : key not allowed to use method.<br/>
        ///     905 : cannot connect database.<br/>
        ///     906 : this time not allowed to use method.<br/>
        /// </returns>
        [Route("Change")]
        [HttpPost]
        public cmlResChangeCard POST_PUNoChangeCard([FromBody] cmlReqChangeCard poPara)
        {
            cSP oFunc;
            cCS oCons;
            cMS oMsg;
            cDatabase oDatabase;
            StringBuilder oSql;
            cChangeCard oChangeCard;
            List<cmlTSysConfig> aoSysConfig;
            cmlResChangeCard oResChangeCardErr;
            cmlResChangeCard oResult;
            cmlResChangeCard oResTopup = new cmlResChangeCard();
           // int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc;
            bool bVerifyPara;
            cmlTCNMComp oComp = new cmlTCNMComp();
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

                oResult = new cmlResChangeCard();
                oFunc = new cSP();
                oCons = new cCS();
                oMsg = new cMS();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                // Validate parameter.
                tModelErr = "";
                if (oFunc.SP_CHKbParaModel(ref tModelErr, ModelState))
                {
                    // Load configuration.
                    aoSysConfig = oFunc.SP_SYSaLoadConfiguration();

                    // Check range time use function.
                    if (oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        // Check KeyApi.
                        if (oFunc.SP_CHKbKeyApi(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            // Varify parameter value.
                            oChangeCard = new cChangeCard();
                            bVerifyPara = oChangeCard.C_DATbVerifyChangeCard(poPara, aoSysConfig, out tErrCode, out tErrDesc, out oResChangeCardErr);
                            if (bVerifyPara == true)
                            {
                                oResult.rtCode = oMsg.tMS_RespCode1;
                                oResult.rtDesc = oMsg.tMS_RespDesc1;
                                return oResult;
                            }
                            else
                            {
                                // Varify parameter value false.
                                oResult.rtCode = tErrCode;
                                oResult.rtDesc = tErrDesc;
                                return oResult;
                            }

                        }
                        else
                        {
                            // Key not allowed to use method.
                            oResult.rtCode = oMsg.tMS_RespCode904;
                            oResult.rtDesc = oMsg.tMS_RespDesc904;
                            return oResult;
                        }
                    }
                    else
                    {
                        // This time not allowed to use method.
                        oResult.rtCode = oMsg.tMS_RespCode906;
                        oResult.rtDesc = oMsg.tMS_RespDesc906;
                        return oResult;
                    }
                }
                else
                {
                    // Validate parameter model false.
                    oResult.rtCode = oMsg.tMS_RespCode701;
                    oResult.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                    return oResult;
                }
            }
            catch (Exception oEx)
            {
                // Return error.
                oResult = new cmlResChangeCard();
                oResult.rtCode = new cMS().tMS_RespCode900;
                oResult.rtDesc = new cMS().tMS_RespDesc900;
                return oResult;
            }
            finally
            {
                oFunc = null;
                oCons = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
                aoSysConfig = null;
                oResChangeCardErr = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                oChangeCard= null;
                oResult = null;
                oResTopup = null;
                oComp = null;
            }
        }

        /// <summary>
        ///  ยกเลิกบัตร Cancel Card
        /// </summary>
        /// <param name="poPara"></param>
        /// <returns>
        ///     System process status.<br/>
        ///     1   : success.<br/>
        ///     701 : validate parameter model false.
        ///     712 : Not found Card.
        ///     713 : Card Date expired.
        ///     716 : ResetExpire card unsuccess.".
        ///     802 : formate data incorrect..<br/>
        ///     900 : service process false.<br/>
        ///     904 : key not allowed to use method.<br/>
        ///     905 : cannot connect database.<br/>
        ///     906 : this time not allowed to use method.<br/>
        /// </returns>
        [Route("CancelCard")]
        [HttpPost]
        public cmlResCancelCard POST_PUNoCancelCard([FromBody] cmlReqCancelCard poPara)
        {
            cSP oFunc;
            cCS oCons;
            cMS oMsg;
            cDatabase oDatabase;
            StringBuilder oSql;
            cChangeCard oChangeCard;
            List<cmlTSysConfig> aoSysConfig;
            cmlResCancelCard oResChangeCardErr;
            cmlResCancelCard oResult;
            cmlResCancelCard oResCancelCard = new cmlResCancelCard();
            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc;
            bool bVerifyPara;
            cmlTCNMComp oComp = new cmlTCNMComp();
            cmlTFNMCard oCard = new cmlTFNMCard();
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

                oResult = new cmlResCancelCard();
                oFunc = new cSP();
                oCons = new cCS();
                oMsg = new cMS();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                // Validate parameter.
                tModelErr = "";
                if (oFunc.SP_CHKbParaModel(ref tModelErr, ModelState))
                {
                    // Load configuration.
                    aoSysConfig = oFunc.SP_SYSaLoadConfiguration();

                    // Check range time use function.
                    if (oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        // Check KeyApi.
                        if (oFunc.SP_CHKbKeyApi(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            // Varify parameter value.
                            oChangeCard = new cChangeCard();
                            bVerifyPara = oChangeCard.C_DATbVerifyCancelCard(poPara, aoSysConfig, out tErrCode, out tErrDesc, out oResChangeCardErr);
                            if (bVerifyPara == true)
                            {
                                nConTme = 0;
                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                                nCmdTme = 0;
                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                                oDatabase = new cDatabase(nConTme);
                                oSql = new StringBuilder();

                                // Get Company Code
                                oSql.Clear();
                                oSql.AppendLine("SELECT TOP 1 [FTCmpCode],[FTCmpTel],[FTCmpFax]");
                                oSql.AppendLine(",[FTBchcode],[FTCmpWhsInOrEx],[FTCmpRetInOrEx]");
                                oSql.AppendLine(",[FTCmpEmail],[FTRteCode],[FTVatCode]");
                                oSql.AppendLine(" FROM TCNMComp WITH (NOLOCK) ");
                                oComp = oDatabase.C_DAToSqlQuery<cmlTCNMComp>(oSql.ToString(), nCmdTme);

                                oSql = new StringBuilder();
                                oSql.Clear();
                                oSql.AppendLine("SELECT C.FTCrdCode,C.FDCrdExpireDate");
                                oSql.AppendLine(",ISNULL(C.FCCrdValue,0.0000) AS FCCrdValue");
                                oSql.AppendLine(",ISNULL(T.FNCtyExpirePeriod,0) AS FNCtyExpirePeriod");
                                oSql.AppendLine(",ISNULL(T.FNCtyExpiredType,0) AS FNCtyExpiredType,C.FDCrdLastTopup ");
                                oSql.AppendLine("FROM TFNMCard C WITH(NOLOCK) LEFT JOIN TFNMCardType T ON (C.FTCtyCode=T.FTCtyCode) ");
                                oSql.AppendLine("WHERE FTCrdCode='" + poPara.ptCrdCode + "'");
                                oCard = oDatabase.C_DAToSqlQuery<cmlTFNMCard>(oSql.ToString(), nCmdTme);

                                oSql.Clear();
                                oSql.AppendLine("BEGIN TRANSACTION ");
                                oSql.AppendLine("  SAVE TRANSACTION CancelCard ");
                                oSql.AppendLine("  BEGIN TRY ");

                                // Update Master Card
                                oSql.AppendLine("     UPDATE TFNMCard WITH (ROWLOCK) SET ");
                                oSql.AppendLine("     FTCrdStaActive='3'");
                                oSql.AppendLine("     WHERE FTCrdCode='" + poPara.ptCrdCode + "'");
                                oSql.AppendLine("     COMMIT TRANSACTION CancelCard");
                                oSql.AppendLine("  END TRY");

                                oSql.AppendLine("  BEGIN CATCH");
                                oSql.AppendLine("   ROLLBACK TRANSACTION CancelCard");
                                oSql.AppendLine("  END CATCH");

                                try
                                {
                                    // Confuguration database.
                                    nConTme = 0;
                                    oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                                    nCmdTme = 0;
                                    oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                                    oDatabase = new cDatabase(nConTme);
                                    nRowEff = oDatabase.C_DATnExecuteSql(oSql.ToString(), nCmdTme);

                                    if (nRowEff == 0)
                                    {
                                        oResult.rtCode = oMsg.tMS_RespCode900;
                                        oResult.rtDesc = oMsg.tMS_RespDesc900;
                                        return oResult;
                                    }
                                }
                                catch (EntityException oEtyExn)
                                {
                                    switch (oEtyExn.HResult)
                                    {
                                        case -2146232060:
                                            // Cannot connect database..
                                            oResult.rtCode = oMsg.tMS_RespCode905;
                                            oResult.rtDesc = oMsg.tMS_RespDesc905;
                                            return oResult;
                                    }
                                }

                                oResult.rtCode = oMsg.tMS_RespCode1;
                                oResult.rtDesc = oMsg.tMS_RespDesc1;
                                return oResult;
                            }
                            else
                            {
                                // Varify parameter value false.
                                oResult.rtCode = tErrCode;
                                oResult.rtDesc = tErrDesc;
                                return oResult;
                            }

                        }
                        else
                        {
                            // Key not allowed to use method.
                            oResult.rtCode = oMsg.tMS_RespCode904;
                            oResult.rtDesc = oMsg.tMS_RespDesc904;
                            return oResult;
                        }
                    }
                    else
                    {
                        // This time not allowed to use method.
                        oResult.rtCode = oMsg.tMS_RespCode906;
                        oResult.rtDesc = oMsg.tMS_RespDesc906;
                        return oResult;
                    }
                }
                else
                {
                    // Validate parameter model false.
                    oResult.rtCode = oMsg.tMS_RespCode701;
                    oResult.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                    return oResult;
                }
            }
            catch (Exception oEx)
            {
                // Return error.
                oResult = new cmlResCancelCard();
                oResult.rtCode = new cMS().tMS_RespCode900;
                oResult.rtDesc = new cMS().tMS_RespDesc900;
                return oResult;
            }
            finally
            {
                oFunc = null;
                oCons = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
                aoSysConfig = null;
                oResChangeCardErr = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                oChangeCard = null;
                oResult = null;
                oResCancelCard = null;
                oComp = null;
            }
        }
    }
}
