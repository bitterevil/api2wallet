﻿using API2Wallet.Class;
using API2Wallet.Class.ResetExpired;
using API2Wallet.Class.Standard;
using API2Wallet.Class.Topup;
using API2Wallet.Models;
using API2Wallet.Models.WebService.Request.Topup;
using API2Wallet.Models.WebService.Response.Topup;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http;

namespace API2Wallet.Controllers
{
    /// <summary>
    /// Topup information.
    /// </summary>
    [RoutePrefix(cCS.tCS_APIVer + "/Topup")]
    public class cTopupController : ApiController
    {

        /// <summary>
        /// เติมเงิน Topup
        /// </summary>
        /// <param name="poPara"></param>
        /// <returns>
        ///     System process status.<br/>
        ///     1   : success.<br/>
        ///     701 : validate parameter model false.
        ///     712 : Not found Card.
        ///     713 : Card Date expired.
        ///     716 : ResetExpire card unsuccess.".
        ///     802 : formate data incorrect..<br/>
        ///     900 : service process false.<br/>
        ///     904 : key not allowed to use method.<br/>
        ///     905 : cannot connect database.<br/>
        ///     906 : this time not allowed to use method.<br/>
        /// </returns>
        [Route("Topup")]
        [HttpPost]
        public cmlResTopup POST_PUNoInsTopup([FromBody] cmlReqTopup poPara)
        {
            cSP oFunc;
            cCS oCons;
            cMS oMsg;
            cDatabase oDatabase;
            StringBuilder oSql;
            cTopup oTopup;
            List<cmlTSysConfig> aoSysConfig;
            cmlResTopup oResTopupErr;
            cmlResTopup oResult;
            cmlResTopup oResTopup = new cmlResTopup();
            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc;
            bool bVerifyPara;
            cmlTCNMComp oComp = new cmlTCNMComp();
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

                oResult = new cmlResTopup();
                oFunc = new cSP();
                oCons = new cCS();
                oMsg = new cMS();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                // Validate parameter.
                tModelErr = "";
                if (oFunc.SP_CHKbParaModel(ref tModelErr, ModelState))
                {
                    // Load configuration.
                    aoSysConfig = oFunc.SP_SYSaLoadConfiguration();

                    // Check range time use function.
                    if (oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        // Check KeyApi.
                        if (oFunc.SP_CHKbKeyApi(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            // Varify parameter value.
                            oTopup = new cTopup();
                            bVerifyPara = oTopup.C_DATbVerifyTopup(poPara, aoSysConfig, out tErrCode, out tErrDesc, out oResTopupErr);
                            if (bVerifyPara == true)
                            {
                                nConTme = 0;
                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                                nCmdTme = 0;
                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                                oDatabase = new cDatabase(nConTme);
                                oSql = new StringBuilder();

                                // Get Company Code
                                oSql.Clear();
                                oSql.AppendLine("SELECT TOP 1 [FTCmpCode],[FTCmpTel],[FTCmpFax]");
                                oSql.AppendLine(",[FTBchcode],[FTCmpWhsInOrEx],[FTCmpRetInOrEx]");
                                oSql.AppendLine(",[FTCmpEmail],[FTRteCode],[FTVatCode]");
                                oSql.AppendLine(" FROM TCNMComp WITH (NOLOCK) ");
                                oComp = oDatabase.C_DAToSqlQuery<cmlTCNMComp>(oSql.ToString(), nCmdTme);

                                oSql.Clear();
                                oSql.AppendLine("BEGIN TRANSACTION ");
                                oSql.AppendLine("  SAVE TRANSACTION Topup ");
                                oSql.AppendLine("  BEGIN TRY ");

                                // Insert transection Sale
                                oSql.AppendLine("     INSERT INTO TFNTCrdTopUp WITH(ROWLOCK)");
                                oSql.AppendLine("     (");
                                oSql.AppendLine("	  FTBchCode,FTTxnDocType,FTCrdCode,");
                                oSql.AppendLine("	  FDTxnDocDate,FTBchCodeRef,FCTxnValue,");
                                oSql.AppendLine("	  FTTxnStaPrc,FTTxnPosCode");
                                oSql.AppendLine("     )");
                                oSql.AppendLine("     VALUES");
                                oSql.AppendLine("     (");
                                oSql.AppendLine("	  '" + oComp.FTBchcode + "','1','" + poPara.ptCrdCode + "',");
                                oSql.AppendLine("	  GETDATE(),'" + poPara.ptBchCode + "','" + poPara.pcTxnValue + "',");
                                oSql.AppendLine("	  '1','" + poPara.ptTxnPosCode + "'");
                                oSql.AppendLine("     )");

                                // Update Master Card
                                oSql.AppendLine("     UPDATE TFNMCard WITH (ROWLOCK) SET ");
                                oSql.AppendLine("     FCCrdValue=(FCCrdValue + " + poPara.pcTxnValue + "),");
                                oSql.AppendLine("     FDCrdLastTopup=GETDATE() ");
                                oSql.AppendLine("     WHERE FTCrdCode='" + poPara.ptCrdCode + "'");
                                oSql.AppendLine("     COMMIT TRANSACTION Topup");
                                oSql.AppendLine("  END TRY");

                                oSql.AppendLine("  BEGIN CATCH");
                                oSql.AppendLine("   ROLLBACK TRANSACTION Topup");
                                oSql.AppendLine("  END CATCH");

                                try
                                {
                                    // Confuguration database.
                                    nConTme = 0;
                                    oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                                    nCmdTme = 0;
                                    oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                                    oDatabase = new cDatabase(nConTme);
                                    nRowEff = oDatabase.C_DATnExecuteSql(oSql.ToString(), nCmdTme);

                                    if (nRowEff == 0)
                                    {
                                        oResult.rtCode = oMsg.tMS_RespCode900;
                                        oResult.rtDesc = oMsg.tMS_RespDesc900;
                                        return oResult;
                                    }
                                }
                                catch (EntityException oEtyExn)
                                {
                                    switch (oEtyExn.HResult)
                                    {
                                        case -2146232060:
                                            // Cannot connect database..
                                            oResult.rtCode = oMsg.tMS_RespCode905;
                                            oResult.rtDesc = oMsg.tMS_RespDesc905;
                                            return oResult;
                                    }
                                }

                                oSql = new StringBuilder();
                                oSql.Clear();
                                oSql.AppendLine("SELECT TOP 1 ISNULL(C.FCCrdValue,0) AS 'rcTxnValue'");
                                oSql.AppendLine(",T.FCCtyDeposit AS 'rcCtyDeposit'");
                                oSql.AppendLine(",ISNULL((C.FCCrdValue-T.FCCtyDeposit),0) AS 'rcTxnValueAvb'");
                                oSql.AppendLine(",C.FTCtyCode AS 'rtCtyCode'");
                                oSql.AppendLine(",FDCrdExpireDate AS 'rdCrdExpireDate'");
                                oSql.AppendLine(",L.FTCrdName AS 'rtCrdName'");
                                oSql.AppendLine("FROM TFNMCard C");
                                oSql.AppendLine("LEFT JOIN TFNMCardType T ON (C.FTCtyCode=T.FTCtyCode)");
                                oSql.AppendLine("LEFT JOIN TFNMCard_L L ON (C.FTCrdCode=L.FTCrdCode)");
                                oSql.AppendLine("WHERE  C.FTCrdCode='" + poPara.ptCrdCode + "'");

                                nConTme = 0;
                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                                nCmdTme = 0;
                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                                oDatabase = new cDatabase(nConTme);
                                oResTopup = oDatabase.C_DAToSqlQuery<cmlResTopup>(oSql.ToString(), nCmdTme);
                                oResult.rtCode = oMsg.tMS_RespCode1;
                                oResult.rtDesc = oMsg.tMS_RespDesc1;
                                oResult.rcTxnValue = oResTopup.rcTxnValue;
                                oResult.rcCtyDeposit = oResTopup.rcCtyDeposit;
                                oResult.rcTxnValueAvb = oResTopup.rcTxnValueAvb;
                                oResult.rtCtyCode = oResTopup.rtCtyCode;
                                oResult.rtCrdName = oResTopup.rtCrdName;
                                oResult.rdCrdExpireDate = oResTopup.rdCrdExpireDate;
                                return oResult;
                            }
                            else
                            {
                                // Varify parameter value false.
                                oResult.rtCode = tErrCode;
                                oResult.rtDesc = tErrDesc;
                                return oResult;
                            }

                        }
                        else
                        {
                            // Key not allowed to use method.
                            oResult.rtCode = oMsg.tMS_RespCode904;
                            oResult.rtDesc = oMsg.tMS_RespDesc904;
                            return oResult;
                        }
                    }
                    else
                    {
                        // This time not allowed to use method.
                        oResult.rtCode = oMsg.tMS_RespCode906;
                        oResult.rtDesc = oMsg.tMS_RespDesc906;
                        return oResult;
                    }
                }
                else
                {
                    // Validate parameter model false.
                    oResult.rtCode = oMsg.tMS_RespCode701;
                    oResult.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                    return oResult;
                }
            }
            catch (Exception oEx)
            {
                // Return error.
                oResult = new cmlResTopup();
                oResult.rtCode = new cMS().tMS_RespCode900;
                oResult.rtDesc = new cMS().tMS_RespDesc900;
                return oResult;
            }
            finally
            {
                oFunc = null;
                oCons = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
                aoSysConfig = null;
                oResTopupErr = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                oTopup = null;
                oResult = null;
                oResTopup = null;
                oComp = null;
            }
        }

        /// <summary>
        /// ยกเลิกเติมเงิน CancelTopup
        /// </summary>
        /// <param name="poPara"></param>
        /// <returns>
        ///     System process status.<br/>
        ///     1   : success.<br/>
        ///     701 : validate parameter model false.
        ///     712 : Not found Card.
        ///     713 : Card Date expired.
        ///     716 : ResetExpire card unsuccess.".
        ///     802 : formate data incorrect..<br/>
        ///     900 : service process false.<br/>
        ///     904 : key not allowed to use method.<br/>
        ///     905 : cannot connect database.<br/>
        ///     906 : this time not allowed to use method.<br/>
        /// </returns>
        [Route("CancelTopup")]
        [HttpPost]
        public cmlResResetAvi POST_PUNoInsCancelTopup([FromBody] cmlReqcancelTopup poPara)
        {
            cSP oFunc;
            cCS oCons;
            cMS oMsg;
            cDatabase oDatabase;
            StringBuilder oSql;
            cTopup oTopup;
            List<cmlTSysConfig> aoSysConfig;
            cmlResResetAvi oResTopupErr;
            cmlResResetAvi oResult;
            cmlResResetAvi oResTopup = new cmlResResetAvi();
            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc;
            bool bVerifyPara;
            cmlTCNMComp oComp = new cmlTCNMComp();
            cmlTFNTCrdTopup oCrdTopup = new cmlTFNTCrdTopup();
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

                oResult = new cmlResResetAvi();
                oFunc = new cSP();
                oCons = new cCS();
                oMsg = new cMS();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                // Validate parameter.
                tModelErr = "";
                if (oFunc.SP_CHKbParaModel(ref tModelErr, ModelState))
                {
                    // Load configuration.
                    aoSysConfig = oFunc.SP_SYSaLoadConfiguration();

                    // Check range time use function.
                    if (oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        // Check KeyApi.
                        if (oFunc.SP_CHKbKeyApi(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            // Varify parameter value.
                            oTopup = new cTopup();
                            bVerifyPara = oTopup.C_DATbVerifyCancelTopup(poPara, aoSysConfig, out tErrCode, out tErrDesc, out oResTopupErr);
                            if (bVerifyPara == true)
                            {
                                nConTme = 0;
                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                                nCmdTme = 0;
                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                                oDatabase = new cDatabase(nConTme);
                                oSql = new StringBuilder();

                                // Get Company Code
                                oSql.Clear();
                                oSql.AppendLine("SELECT TOP 1 [FTCmpCode],[FTCmpTel],[FTCmpFax]");
                                oSql.AppendLine(",[FTBchcode],[FTCmpWhsInOrEx],[FTCmpRetInOrEx]");
                                oSql.AppendLine(",[FTCmpEmail],[FTRteCode],[FTVatCode]");
                                oSql.AppendLine(" FROM TCNMComp WITH (NOLOCK) ");
                                oComp = oDatabase.C_DAToSqlQuery<cmlTCNMComp>(oSql.ToString(), nCmdTme);

                                // Get value Sale And Pos
                                oSql.Clear();
                                oSql.AppendLine("SELECT ISNULL(FCTxnValue,0) AS FCTxnValue,FTTxnPosCode FROM TFNTCrdTopUp");
                                oSql.AppendLine("WHERE FNTxnID=" + poPara.pnTxnID + "");
                                oCrdTopup = oDatabase.C_DAToSqlQuery<cmlTFNTCrdTopup>(oSql.ToString(), nCmdTme);

                                oSql.Clear();
                                oSql.AppendLine("BEGIN TRANSACTION ");
                                oSql.AppendLine("  SAVE TRANSACTION Topup ");
                                oSql.AppendLine("  BEGIN TRY ");

                                // Insert transection Sale
                                oSql.AppendLine("     INSERT INTO TFNTCrdTopUp WITH(ROWLOCK)");
                                oSql.AppendLine("     (");
                                oSql.AppendLine("	  FTBchCode,FTTxnDocType,FTCrdCode,");
                                oSql.AppendLine("	  FDTxnDocDate,FTBchCodeRef,FCTxnValue,");
                                oSql.AppendLine("	  FTTxnStaPrc,FTTxnPosCode");
                                oSql.AppendLine("     )");
                                oSql.AppendLine("     VALUES");
                                oSql.AppendLine("     (");
                                oSql.AppendLine("	  '" + oComp.FTBchcode + "','2','" + poPara.ptCrdCode + "',");
                                oSql.AppendLine("	  GETDATE(),'" + poPara.ptBchCode + "','" + oCrdTopup.FCTxnValue + "',");
                                oSql.AppendLine("	  '1','" + oCrdTopup.FTTxnPosCode + "'");
                                oSql.AppendLine("     )");

                                // Update Master Card
                                oSql.AppendLine("     UPDATE TFNMCard WITH (ROWLOCK) SET ");
                                oSql.AppendLine("     FCCrdValue=(FCCrdValue - " + oCrdTopup.FCTxnValue + "),");
                                oSql.AppendLine("     FDCrdLastTopup=GETDATE() ");
                                oSql.AppendLine("     WHERE FTCrdCode='" + poPara.ptCrdCode + "'");
                                oSql.AppendLine("     COMMIT TRANSACTION Topup");

                                // Update Ref Id Canceled
                                oSql.AppendLine("     UPDATE TFNTCrdTopUp WITH (ROWLOCK) SET ");
                                oSql.AppendLine("     FTTxnStaCancel='1'");
                                oSql.AppendLine("     WHERE FNTxnID=" + poPara.pnTxnID + "");
                                oSql.AppendLine("  END TRY");

                                oSql.AppendLine("  BEGIN CATCH");
                                oSql.AppendLine("   ROLLBACK TRANSACTION Topup");
                                oSql.AppendLine("  END CATCH");

                                try
                                {
                                    // Confuguration database.
                                    nConTme = 0;
                                    oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                                    nCmdTme = 0;
                                    oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                                    oDatabase = new cDatabase(nConTme);
                                    nRowEff = oDatabase.C_DATnExecuteSql(oSql.ToString(), nCmdTme);

                                    if (nRowEff == 0)
                                    {
                                        oResult.rtCode = oMsg.tMS_RespCode900;
                                        oResult.rtDesc = oMsg.tMS_RespDesc900;
                                        return oResult;
                                    }
                                }
                                catch (EntityException oEtyExn)
                                {
                                    switch (oEtyExn.HResult)
                                    {
                                        case -2146232060:
                                            // Cannot connect database..
                                            oResult.rtCode = oMsg.tMS_RespCode905;
                                            oResult.rtDesc = oMsg.tMS_RespDesc905;
                                            return oResult;
                                    }
                                }

                                oSql = new StringBuilder();
                                oSql.Clear();
                                oSql.AppendLine("SELECT TOP 1 C.FCCrdValue AS 'rcTxnValue'");
                                oSql.AppendLine(",T.FCCtyDeposit AS 'rcCtyDeposit'");
                                oSql.AppendLine(",(C.FCCrdValue-T.FCCtyDeposit) AS 'rcTxnValueAvb'");
                                oSql.AppendLine(",C.FTCtyCode AS 'rtCtyCode'");
                                oSql.AppendLine(",FDCrdExpireDate AS 'rdCrdExpireDate'");
                                oSql.AppendLine(",L.FTCrdName AS 'rtCrdName'");
                                oSql.AppendLine("FROM TFNMCard C");
                                oSql.AppendLine("LEFT JOIN TFNMCardType T ON (C.FTCtyCode=T.FTCtyCode)");
                                oSql.AppendLine("LEFT JOIN TFNMCard_L L ON (C.FTCrdCode=L.FTCrdCode)");
                                oSql.AppendLine("WHERE  C.FTCrdCode='" + poPara.ptCrdCode + "'");

                                nConTme = 0;
                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                                nCmdTme = 0;
                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                                oDatabase = new cDatabase(nConTme);
                                oResTopup = oDatabase.C_DAToSqlQuery<cmlResResetAvi>(oSql.ToString(), nCmdTme);
                                oResult.rtCode = oMsg.tMS_RespCode1;
                                oResult.rtDesc = oMsg.tMS_RespDesc1;
                                oResult.rcTxnValue = oResTopup.rcTxnValue;
                                oResult.rcCtyDeposit = oResTopup.rcCtyDeposit;
                                oResult.rcTxnValueAvb = oResTopup.rcTxnValueAvb;
                                oResult.rtCtyCode = oResTopup.rtCtyCode;
                                oResult.rtCrdName = oResTopup.rtCrdName;
                                oResult.rdCrdExpireDate = oResTopup.rdCrdExpireDate;
                                return oResult;
                            }
                            else
                            {
                                // Varify parameter value false.
                                oResult.rtCode = tErrCode;
                                oResult.rtDesc = tErrDesc;
                                return oResult;
                            }

                        }
                        else
                        {
                            // Key not allowed to use method.
                            oResult.rtCode = oMsg.tMS_RespCode904;
                            oResult.rtDesc = oMsg.tMS_RespDesc904;
                            return oResult;
                        }
                    }
                    else
                    {
                        // This time not allowed to use method.
                        oResult.rtCode = oMsg.tMS_RespCode906;
                        oResult.rtDesc = oMsg.tMS_RespDesc906;
                        return oResult;
                    }
                }
                else
                {
                    // Validate parameter model false.
                    oResult.rtCode = oMsg.tMS_RespCode701;
                    oResult.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                    return oResult;
                }
            }
            catch (Exception oEx)
            {
                // Return error.
                oResult = new cmlResResetAvi();
                oResult.rtCode = new cMS().tMS_RespCode900;
                oResult.rtDesc = new cMS().tMS_RespDesc900;
                return oResult;
            }
            finally
            {
                oFunc = null;
                oCons = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
                aoSysConfig = null;
                oResTopupErr = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                oTopup = null;
                oResult = null;
                oResTopup = null;
                oComp = null;
            }
        }

        /// <summary>
        /// แลกคืน//ResetAvailable
        /// </summary>
        /// <param name="poPara"></param>
        /// <returns>
        ///     System process status.<br/>
        ///     1   : success.<br/>
        ///     701 : validate parameter model false.
        ///     712 : Not found Card.
        ///     713 : Card Date expired.
        ///     716 : ResetExpire card unsuccess.".
        ///     802 : formate data incorrect..<br/>
        ///     900 : service process false.<br/>
        ///     904 : key not allowed to use method.<br/>
        ///     905 : cannot connect database.<br/>
        ///     906 : this time not allowed to use method.<br/>
        /// </returns>
        [Route("ResetAvailable")]
        [HttpPost]
        public cmlResResetAvi POST_PUNoResetAvailableTopup([FromBody] cmlReqResetAvi poPara)
        {
            cSP oFunc;
            cCS oCons;
            cMS oMsg;
            cDatabase oDatabase;
            StringBuilder oSql;
            cTopup oTopup;
            List<cmlTSysConfig> aoSysConfig;
            cmlResResetAvi oResTopupErr;
            cmlResResetAvi oResult;
            cmlResResetAvi oResTopup = new cmlResResetAvi();
            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc;
            bool bVerifyPara;
            cmlTCNMComp oComp = new cmlTCNMComp();
            cmlTFNMCard oCard = new cmlTFNMCard();
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

                oResult = new cmlResResetAvi();
                oFunc = new cSP();
                oCons = new cCS();
                oMsg = new cMS();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                // Validate parameter.
                tModelErr = "";
                if (oFunc.SP_CHKbParaModel(ref tModelErr, ModelState))
                {
                    // Load configuration.
                    aoSysConfig = oFunc.SP_SYSaLoadConfiguration();

                    // Check range time use function.
                    if (oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        // Check KeyApi.
                        if (oFunc.SP_CHKbKeyApi(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            // Varify parameter value.
                            oTopup = new cTopup();
                            bVerifyPara = oTopup.C_DATbVerifyResetAvi(poPara, aoSysConfig, out tErrCode, out tErrDesc, out oResTopupErr);
                            if (bVerifyPara == true)
                            {
                                nConTme = 0;
                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                                nCmdTme = 0;
                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                                oDatabase = new cDatabase(nConTme);
                                oSql = new StringBuilder();

                                // Get Company Code
                                oSql.Clear();
                                oSql.AppendLine("SELECT TOP 1 [FTCmpCode],[FTCmpTel],[FTCmpFax]");
                                oSql.AppendLine(",[FTBchcode],[FTCmpWhsInOrEx],[FTCmpRetInOrEx]");
                                oSql.AppendLine(",[FTCmpEmail],[FTRteCode],[FTVatCode]");
                                oSql.AppendLine(" FROM TCNMComp WITH (NOLOCK) ");
                                oComp = oDatabase.C_DAToSqlQuery<cmlTCNMComp>(oSql.ToString(), nCmdTme);

                                // Get value TFNMCard
                                oSql.Clear();
                                oSql.AppendLine("SELECT ISNULL(FCCrdValue,0) AS FCCrdValue FROM TFNMCard ");
                                oSql.AppendLine("WHERE FTCrdCode='" + poPara.ptCrdCode + "'");
                                oCard = oDatabase.C_DAToSqlQuery<cmlTFNMCard>(oSql.ToString(), nCmdTme);

                                oSql.Clear();
                                oSql.AppendLine("BEGIN TRANSACTION ");
                                oSql.AppendLine("  SAVE TRANSACTION Topup ");
                                oSql.AppendLine("  BEGIN TRY ");

                                // Insert transection Sale
                                oSql.AppendLine("     INSERT INTO TFNTCrdTopUp WITH(ROWLOCK)");
                                oSql.AppendLine("     (");
                                oSql.AppendLine("	  FTBchCode,FTTxnDocType,FTCrdCode,");
                                oSql.AppendLine("	  FDTxnDocDate,FTBchCodeRef,FCTxnValue,");
                                oSql.AppendLine("	  FTTxnStaPrc,FTTxnPosCode");
                                oSql.AppendLine("     )");
                                oSql.AppendLine("     VALUES");
                                oSql.AppendLine("     (");
                                oSql.AppendLine("	  '" + oComp.FTBchcode + "','5','" + poPara.ptCrdCode + "',");
                                oSql.AppendLine("	  GETDATE(),'" + poPara.ptBchCode + "','" + oCard.FCCrdValue + "',");
                                oSql.AppendLine("	  '1','" + poPara.ptTxnPosCode + "'");
                                oSql.AppendLine("     )");

                                // Update Master Card
                                oSql.AppendLine("     UPDATE TFNMCard WITH (ROWLOCK) SET ");
                                oSql.AppendLine("     FCCrdValue='0',");
                                oSql.AppendLine("     FDCrdResetDate=GETDATE() ");
                                oSql.AppendLine("     WHERE FTCrdCode='" + poPara.ptCrdCode + "'");
                                oSql.AppendLine("     COMMIT TRANSACTION Topup");
                                oSql.AppendLine("  END TRY");

                                oSql.AppendLine("  BEGIN CATCH");
                                oSql.AppendLine("   ROLLBACK TRANSACTION Topup");
                                oSql.AppendLine("  END CATCH");

                                try
                                {
                                    // Confuguration database.
                                    nConTme = 0;
                                    oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                                    nCmdTme = 0;
                                    oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                                    oDatabase = new cDatabase(nConTme);
                                    nRowEff = oDatabase.C_DATnExecuteSql(oSql.ToString(), nCmdTme);

                                    if (nRowEff == 0)
                                    {
                                        oResult.rtCode = oMsg.tMS_RespCode900;
                                        oResult.rtDesc = oMsg.tMS_RespDesc900;
                                        return oResult;
                                    }
                                }
                                catch (EntityException oEtyExn)
                                {
                                    switch (oEtyExn.HResult)
                                    {
                                        case -2146232060:
                                            // Cannot connect database..
                                            oResult.rtCode = oMsg.tMS_RespCode905;
                                            oResult.rtDesc = oMsg.tMS_RespDesc905;
                                            return oResult;
                                    }
                                }

                                oSql = new StringBuilder();
                                oSql.Clear();
                                oSql.AppendLine("SELECT TOP 1 C.FCCrdValue AS 'rcTxnValue'");
                                oSql.AppendLine(",T.FCCtyDeposit AS 'rcCtyDeposit'");
                                oSql.AppendLine(",(C.FCCrdValue-T.FCCtyDeposit) AS 'rcTxnValueAvb'");
                                oSql.AppendLine(",C.FTCtyCode AS 'rtCtyCode'");
                                oSql.AppendLine(",FDCrdExpireDate AS 'rdCrdExpireDate'");
                                oSql.AppendLine(",L.FTCrdName AS 'rtCrdName'");
                                oSql.AppendLine("FROM TFNMCard C");
                                oSql.AppendLine("LEFT JOIN TFNMCardType T ON (C.FTCtyCode=T.FTCtyCode)");
                                oSql.AppendLine("LEFT JOIN TFNMCard_L L ON (C.FTCrdCode=L.FTCrdCode)");
                                oSql.AppendLine("WHERE  C.FTCrdCode='" + poPara.ptCrdCode + "'");

                                nConTme = 0;
                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                                nCmdTme = 0;
                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                                oDatabase = new cDatabase(nConTme);
                                oResTopup = oDatabase.C_DAToSqlQuery<cmlResResetAvi>(oSql.ToString(), nCmdTme);
                                oResult.rtCode = oMsg.tMS_RespCode1;
                                oResult.rtDesc = oMsg.tMS_RespDesc1;
                                oResult.rcTxnValue = oResTopup.rcTxnValue;
                                oResult.rcCtyDeposit = oResTopup.rcCtyDeposit;
                                oResult.rcTxnValueAvb = oResTopup.rcTxnValueAvb;
                                oResult.rtCtyCode = oResTopup.rtCtyCode;
                                oResult.rtCrdName = oResTopup.rtCrdName;
                                oResult.rdCrdExpireDate = oResTopup.rdCrdExpireDate;
                                return oResult;
                            }
                            else
                            {
                                // Varify parameter value false.
                                oResult.rtCode = tErrCode;
                                oResult.rtDesc = tErrDesc;
                                return oResult;
                            }

                        }
                        else
                        {
                            // Key not allowed to use method.
                            oResult.rtCode = oMsg.tMS_RespCode904;
                            oResult.rtDesc = oMsg.tMS_RespDesc904;
                            return oResult;
                        }
                    }
                    else
                    {
                        // This time not allowed to use method.
                        oResult.rtCode = oMsg.tMS_RespCode906;
                        oResult.rtDesc = oMsg.tMS_RespDesc906;
                        return oResult;
                    }
                }
                else
                {
                    // Validate parameter model false.
                    oResult.rtCode = oMsg.tMS_RespCode701;
                    oResult.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                    return oResult;
                }
            }
            catch (Exception oEx)
            {
                // Return error.
                oResult = new cmlResResetAvi();
                oResult.rtCode = new cMS().tMS_RespCode900;
                oResult.rtDesc = new cMS().tMS_RespDesc900;
                return oResult;
            }
            finally
            {
                oFunc = null;
                oCons = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
                aoSysConfig = null;
                oResTopupErr = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                oTopup = null;
                oResult = null;
                oResTopup = null;
                oComp = null;
            }
        }

        /// <summary>
        /// เบิกบัตร//ApvOpenCardManual
        /// </summary>
        /// <param name="poPara"></param>
        /// <returns>
        ///     System process status.<br/>
        ///     1   : success.<br/>
        ///     701 : validate parameter model false.
        ///     802 : formate data incorrect..<br/>
        ///     900 : service process false.<br/>
        ///     904 : key not allowed to use method.<br/>
        ///     905 : cannot connect database.<br/>
        ///     906 : this time not allowed to use method.<br/>
        /// </returns>
        [Route("ApvOpenCard")]
        [HttpPost]
        public cmlResaoApvOpenCard POST_PUNoApvOpenCard([FromBody] List<cmlReqApvReturnCard> poPara)
        {
            cSP oFunc;
            cCS oCons;
            cMS oMsg;
            cDatabase oDatabase;
            StringBuilder oSql;
            List<cmlTSysConfig> aoSysConfig;
            cmlResaoApvOpenCard oResult;
            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi;
            cmlTCNMComp oComp = new cmlTCNMComp();
            cmlTFNMCard oCard = new cmlTFNMCard();
            cmlResApvOpenCard oApvOpenCard = new cmlResApvOpenCard();
            List<cmlResApvOpenCard> aoApvOpenCard = new List<cmlResApvOpenCard>();
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

                oFunc = new cSP();
                oCons = new cCS();
                oMsg = new cMS();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                // Validate parameter.
                tModelErr = "";
                if (oFunc.SP_CHKbParaModel(ref tModelErr, ModelState))
                {
                    // Load configuration.
                    aoSysConfig = oFunc.SP_SYSaLoadConfiguration();

                    // Check range time use function.
                    if (oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        // Check KeyApi.
                        if (oFunc.SP_CHKbKeyApi(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            oResult = new cmlResaoApvOpenCard();
                            nConTme = 0;
                            oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                            nCmdTme = 0;
                            oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                            oDatabase = new cDatabase(nConTme);
                            oSql = new StringBuilder();

                            for (int nRow = 0; nRow < poPara.Count; nRow++)
                            {
                                oApvOpenCard = new cmlResApvOpenCard();
                                oSql.Clear();
                                oSql.AppendLine("SELECT ISNULL(C.FTCrdStaLocate,2) AS FTCrdStaLocate,C.FTCrdStaType ");
                                oSql.AppendLine("FROM TFNMCard C WITH(NOLOCK) LEFT JOIN TFNMCardType T ON (C.FTCtyCode=T.FTCtyCode) ");
                                oSql.AppendLine("WHERE FTCrdCode='" + poPara[nRow].ptCrdCode + "'");

                                oCard = new cmlTFNMCard();
                                oCard = oDatabase.C_DAToSqlQuery<cmlTFNMCard>(oSql.ToString(), nCmdTme);

                                if (oCard == null)
                                {
                                    // ไม่พบข้อมุลบัตร
                                    oApvOpenCard.rtBchCode = poPara[nRow].ptBchCode;
                                    oApvOpenCard.rtCrdCode = poPara[nRow].ptCrdCode;
                                    oApvOpenCard.rtStatus = "4";
                                    aoApvOpenCard.Add(oApvOpenCard);
                                    continue;
                                }

                                if (oCard.FTCrdStaType != "1") {
                                    // ประเภทบัตรไม่ใช่แบบปกติ
                                    oApvOpenCard.rtBchCode = poPara[nRow].ptBchCode;
                                    oApvOpenCard.rtCrdCode = poPara[nRow].ptCrdCode;
                                    oApvOpenCard.rtStatus = "5";
                                    aoApvOpenCard.Add(oApvOpenCard);
                                    continue;
                                }

                                if (oCard.FTCrdStaLocate == "1") {
                                    oApvOpenCard.rtBchCode = poPara[nRow].ptBchCode;
                                    oApvOpenCard.rtCrdCode = poPara[nRow].ptCrdCode;
                                    oApvOpenCard.rtStatus = "3";
                                    aoApvOpenCard.Add(oApvOpenCard);
                                    continue;
                                }

                                //Update สถานะบัตร
                                oSql.Clear();
                                oSql.AppendLine("UPDATE TFNMCard WITH (ROWLOCK) SET FTCrdStaLocate='1' ");
                                oSql.AppendLine("WHERE FTCrdCode='" + poPara[nRow].ptCrdCode + "'");
                                try
                                {
                                    // Confuguration database.
                                    nConTme = 0;
                                    oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                                    nCmdTme = 0;
                                    oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                                    oDatabase = new cDatabase(nConTme);
                                    nRowEff = oDatabase.C_DATnExecuteSql(oSql.ToString(), nCmdTme);

                                    if (nRowEff == 0)
                                    {
                                        oApvOpenCard.rtBchCode = poPara[nRow].ptBchCode;
                                        oApvOpenCard.rtCrdCode = poPara[nRow].ptCrdCode;
                                        oApvOpenCard.rtStatus = "2";
                                        aoApvOpenCard.Add(oApvOpenCard);
                                    }
                                    else
                                    { 
                                        oApvOpenCard.rtBchCode = poPara[nRow].ptBchCode;
                                        oApvOpenCard.rtCrdCode = poPara[nRow].ptCrdCode;
                                        oApvOpenCard.rtStatus = "1";
                                        aoApvOpenCard.Add(oApvOpenCard);
                                    }
                                }
                                catch (EntityException oEtyExn)
                                {
                                    switch (oEtyExn.HResult)
                                    {
                                        case -2146232060:
                                            // Cannot connect database..
                                            oResult = new cmlResaoApvOpenCard();
                                            oResult.rtCode = oMsg.tMS_RespCode905;
                                            oResult.rtDesc = oMsg.tMS_RespDesc905;
                                            return oResult;
                                    }
                                }
                            }

                            // จบการทำงาน
                            oResult = new cmlResaoApvOpenCard();
                            oResult.rtCode = oMsg.tMS_RespCode1;
                            oResult.rtDesc = oMsg.tMS_RespDesc1;
                            oResult.roApvOpenCard = aoApvOpenCard;
                            return oResult;

                        }
                        else
                        {
                            // Key not allowed to use method.
                            oResult = new cmlResaoApvOpenCard();
                            oResult.rtCode = oMsg.tMS_RespCode904;
                            oResult.rtDesc = oMsg.tMS_RespDesc904;
                            return oResult;
                        }
                    }
                    else
                    {
                        // This time not allowed to use method.
                        oResult = new cmlResaoApvOpenCard();
                        oResult.rtCode = oMsg.tMS_RespCode906;
                        oResult.rtDesc = oMsg.tMS_RespDesc906;
                        return oResult;
                    }
                }
                else
                {
                    // Validate parameter model false.
                    oResult = new cmlResaoApvOpenCard();
                    oResult.rtCode = oMsg.tMS_RespCode701;
                    oResult.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                    return oResult;
                }
            }
            catch (Exception oEx)
            {
                // Return error.
                oResult = new cmlResaoApvOpenCard();
                oResult.rtCode = new cMS().tMS_RespCode900;
                oResult.rtDesc = new cMS().tMS_RespDesc900;
                return oResult;
            }
            finally
            {
                oFunc = null;
                oCons = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
                aoSysConfig = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                oResult = null;
                oComp = null;
                oCard = null;
                oApvOpenCard = null;
                aoApvOpenCard = null;
            }
        }

        /// <summary>
        /// คืนบัตร//ApvReturnCard
        /// </summary>
        /// <param name="poPara"></param>
        /// <returns>
        ///     System process status.<br/>
        ///     1   : success.<br/>
        ///     701 : validate parameter model false.
        ///     802 : formate data incorrect..<br/>
        ///     900 : service process false.<br/>
        ///     904 : key not allowed to use method.<br/>
        ///     905 : cannot connect database.<br/>
        ///     906 : this time not allowed to use method.<br/>
        /// </returns>
        [Route("ApvReturnCard")]
        [HttpPost]
        public cmlResaoApvReturnCard POST_PUNoApvReturnCard([FromBody] List<cmlReqApvReturnCard> poPara)
        {
            cSP oFunc;
            cCS oCons;
            cMS oMsg;
            cDatabase oDatabase;
            StringBuilder oSql;
            List<cmlTSysConfig> aoSysConfig;
            cmlResaoApvReturnCard oResult;
            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi;
            cmlTCNMComp oComp = new cmlTCNMComp();
            cmlTFNMCard oCard = new cmlTFNMCard();
            cmlResApvReturnCard oApvOpenCard = new cmlResApvReturnCard();
            List<cmlResApvReturnCard> aoApvOpenCard = new List<cmlResApvReturnCard>();
            bool bResetExp = true;
            cResetExpired oResetExpired = new cResetExpired();
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

                oFunc = new cSP();
                oCons = new cCS();
                oMsg = new cMS();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                // Validate parameter.
                tModelErr = "";
                if (oFunc.SP_CHKbParaModel(ref tModelErr, ModelState))
                {
                    // Load configuration.
                    aoSysConfig = oFunc.SP_SYSaLoadConfiguration();

                    // Check range time use function.
                    if (oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        // Check KeyApi.
                        if (oFunc.SP_CHKbKeyApi(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            oResult = new cmlResaoApvReturnCard();
                            nConTme = 0;
                            oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                            nCmdTme = 0;
                            oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                            oDatabase = new cDatabase(nConTme);
                            oSql = new StringBuilder();

                            for (int nRow = 0; nRow < poPara.Count; nRow++)
                            {
                                oApvOpenCard = new cmlResApvReturnCard();
                                oSql.Clear();
                                oSql.AppendLine("SELECT ISNULL(C.FTCrdStaLocate,2) AS FTCrdStaLocate,C.FTCrdStaType,ISNULL(C.FCCrdValue,0) AS FCCrdValue");
                                oSql.AppendLine("FROM TFNMCard C WITH(NOLOCK) LEFT JOIN TFNMCardType T ON (C.FTCtyCode=T.FTCtyCode) ");
                                oSql.AppendLine("WHERE FTCrdCode='" + poPara[nRow].ptCrdCode + "'");

                                oCard = new cmlTFNMCard();
                                oCard = oDatabase.C_DAToSqlQuery<cmlTFNMCard>(oSql.ToString(), nCmdTme);

                                if (oCard == null)
                                {
                                    // ไม่พบข้อมุลบัตร
                                    oApvOpenCard.rtBchCode = poPara[nRow].ptBchCode;
                                    oApvOpenCard.rtCrdCode = poPara[nRow].ptCrdCode;
                                    oApvOpenCard.rtStatus = "4";
                                    aoApvOpenCard.Add(oApvOpenCard);
                                    continue;
                                }

                                if (oCard.FTCrdStaType != "1")
                                {
                                    // ประเภทบัตรไม่ใช่แบบปกติ
                                    oApvOpenCard.rtBchCode = poPara[nRow].ptBchCode;
                                    oApvOpenCard.rtCrdCode = poPara[nRow].ptCrdCode;
                                    oApvOpenCard.rtStatus = "5";
                                    aoApvOpenCard.Add(oApvOpenCard);
                                    continue;
                                }

                                // ยอดเงินคงเหลือมากกว่า 0
                                if (oCard.FCCrdValue > 0)
                                {
                                    bResetExp = true;  // Reset 
                                    bResetExp = oResetExpired.C_SETbResetExpired(poPara[nRow].ptCrdCode, poPara[nRow].ptBchCode, aoSysConfig);
                                    if (bResetExp == false) {
                                        //ไม่สามารถ ResetExpired ได้
                                        oApvOpenCard.rtBchCode = poPara[nRow].ptBchCode;
                                        oApvOpenCard.rtCrdCode = poPara[nRow].ptCrdCode;
                                        oApvOpenCard.rtStatus = "3";
                                        aoApvOpenCard.Add(oApvOpenCard);
                                        continue;
                                    }
                                }

                                //Update สถานะบัตร
                                oSql.Clear();
                                oSql.AppendLine("UPDATE TFNMCard WITH (ROWLOCK) SET FTCrdStaLocate='1' ");
                                oSql.AppendLine("WHERE FTCrdCode='" + poPara[nRow].ptCrdCode + "'");
                                try
                                {
                                    // Confuguration database.
                                    nConTme = 0;
                                    oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                                    nCmdTme = 0;
                                    oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                                    oDatabase = new cDatabase(nConTme);
                                    nRowEff = oDatabase.C_DATnExecuteSql(oSql.ToString(), nCmdTme);

                                    if (nRowEff == 0)
                                    {
                                        oApvOpenCard.rtBchCode = poPara[nRow].ptBchCode;
                                        oApvOpenCard.rtCrdCode = poPara[nRow].ptCrdCode;
                                        oApvOpenCard.rtStatus = "2";
                                        aoApvOpenCard.Add(oApvOpenCard);
                                    }
                                    else
                                    {
                                        oApvOpenCard.rtBchCode = poPara[nRow].ptBchCode;
                                        oApvOpenCard.rtCrdCode = poPara[nRow].ptCrdCode;
                                        oApvOpenCard.rtStatus = "1";
                                        aoApvOpenCard.Add(oApvOpenCard);
                                    }
                                }
                                catch (EntityException oEtyExn)
                                {
                                    switch (oEtyExn.HResult)
                                    {
                                        case -2146232060:
                                            // Cannot connect database..
                                            oResult = new cmlResaoApvReturnCard();
                                            oResult.rtCode = oMsg.tMS_RespCode905;
                                            oResult.rtDesc = oMsg.tMS_RespDesc905;
                                            return oResult;
                                    }
                                }
                            }

                            // จบการทำงาน
                            oResult = new cmlResaoApvReturnCard();
                            oResult.rtCode = oMsg.tMS_RespCode1;
                            oResult.rtDesc = oMsg.tMS_RespDesc1;
                            oResult.roApvOpenCard = aoApvOpenCard;
                            return oResult;

                        }
                        else
                        {
                            // Key not allowed to use method.
                            oResult = new cmlResaoApvReturnCard();
                            oResult.rtCode = oMsg.tMS_RespCode904;
                            oResult.rtDesc = oMsg.tMS_RespDesc904;
                            return oResult;
                        }
                    }
                    else
                    {
                        // This time not allowed to use method.
                        oResult = new cmlResaoApvReturnCard();
                        oResult.rtCode = oMsg.tMS_RespCode906;
                        oResult.rtDesc = oMsg.tMS_RespDesc906;
                        return oResult;
                    }
                }
                else
                {
                    // Validate parameter model false.
                    oResult = new cmlResaoApvReturnCard();
                    oResult.rtCode = oMsg.tMS_RespCode701;
                    oResult.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                    return oResult;
                }
            }
            catch (Exception oEx)
            {
                // Return error.
                oResult = new cmlResaoApvReturnCard();
                oResult.rtCode = new cMS().tMS_RespCode900;
                oResult.rtDesc = new cMS().tMS_RespDesc900;
                return oResult;
            }
            finally
            {
                oFunc = null;
                oCons = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
                aoSysConfig = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                oResult = null;
                oComp = null;
                oCard = null;
                oApvOpenCard = null;
                aoApvOpenCard = null;
            }
        }

        /// <summary>
        /// ใบคืน//ReturnTopupList
        /// </summary>
        /// <param name="poPara"></param>
        /// <returns>
        ///     System process status.<br/>
        ///     1   : success.<br/>
        ///     701 : validate parameter model false.
        ///     802 : formate data incorrect..<br/>
        ///     900 : service process false.<br/>
        ///     904 : key not allowed to use method.<br/>
        ///     905 : cannot connect database.<br/>
        ///     906 : this time not allowed to use method.<br/>
        /// </returns>
        [Route("ReturnTopupList")]
        [HttpPost]
        public cmlResaoReturnTopupList POST_PUNoReturnTopupList([FromBody] List<cmlReqReturnTopupList> poPara)
        {
            cSP oFunc;
            cCS oCons;
            cMS oMsg;
            cDatabase oDatabase;
            StringBuilder oSql;
            List<cmlTSysConfig> aoSysConfig;
            cmlResaoReturnTopupList oResult;
            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi;
            cmlTCNMComp oComp = new cmlTCNMComp();
            cmlTFNMCard oCard = new cmlTFNMCard();
            cmlResReturnTopupList oReturnTopupList = new cmlResReturnTopupList();
            List<cmlResReturnTopupList> aoReturnTopupList = new List<cmlResReturnTopupList>();
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

                oFunc = new cSP();
                oCons = new cCS();
                oMsg = new cMS();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                // Validate parameter.
                tModelErr = "";
                if (oFunc.SP_CHKbParaModel(ref tModelErr, ModelState))
                {
                    // Load configuration.
                    aoSysConfig = oFunc.SP_SYSaLoadConfiguration();

                    // Check range time use function.
                    if (oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        // Check KeyApi.
                        if (oFunc.SP_CHKbKeyApi(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            oResult = new cmlResaoReturnTopupList();
                            nConTme = 0;
                            oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                            nCmdTme = 0;
                            oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                            oDatabase = new cDatabase(nConTme);
                            oSql = new StringBuilder();

                            for (int nRow = 0; nRow < poPara.Count; nRow++)
                            {
                                oReturnTopupList= new cmlResReturnTopupList();
                                oSql.Clear();
                                oSql.AppendLine("SELECT ISNULL(C.FTCrdStaLocate,2) AS FTCrdStaLocate,C.FTCrdStaType ");
                                oSql.AppendLine("FROM TFNMCard C WITH(NOLOCK) LEFT JOIN TFNMCardType T ON (C.FTCtyCode=T.FTCtyCode) ");
                                oSql.AppendLine("WHERE FTCrdCode='" + poPara[nRow].ptCrdCode + "'");

                                oCard = new cmlTFNMCard();
                                oCard = oDatabase.C_DAToSqlQuery<cmlTFNMCard>(oSql.ToString(), nCmdTme);

                                if (oCard == null)
                                {
                                    // ไม่พบข้อมุลบัตร
                                    oReturnTopupList.rtBchCode = poPara[nRow].ptBchCode;
                                    oReturnTopupList.rtCrdCode = poPara[nRow].ptCrdCode;
                                    oReturnTopupList.rtStatus = "4";
                                    aoReturnTopupList.Add(oReturnTopupList);
                                    continue;
                                }

                                //if (oCard.FTCrdStaType != "1")
                                //{
                                //    // ประเภทบัตรไม่ใช่แบบปกติ
                                //    oReturnTopupList.rtBchCode = poPara[nRow].ptBchCode;
                                //    oReturnTopupList.rtCrdCode = poPara[nRow].ptCrdCode;
                                //    oReturnTopupList.rtStatus = "5";
                                //    aoReturnTopupList.Add(oReturnTopupList);
                                //    continue;
                                //}

                                if (oCard.FTCrdStaLocate != "1")
                                {
                                    oReturnTopupList.rtBchCode = poPara[nRow].ptBchCode;
                                    oReturnTopupList.rtCrdCode = poPara[nRow].ptCrdCode;
                                    oReturnTopupList.rtStatus = "3";
                                    aoReturnTopupList.Add(oReturnTopupList);
                                    continue;
                                }

                                //Update สถานะบัตร
                                oSql.Clear();
                                oSql.AppendLine("UPDATE TFNMCard WITH (ROWLOCK) SET FTCrdStaLocate='2' ");
                                oSql.AppendLine("WHERE FTCrdCode='" + poPara[nRow].ptCrdCode + "'");
                                try
                                {
                                    // Confuguration database.
                                    nConTme = 0;
                                    oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                                    nCmdTme = 0;
                                    oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                                    oDatabase = new cDatabase(nConTme);
                                    nRowEff = oDatabase.C_DATnExecuteSql(oSql.ToString(), nCmdTme);

                                    if (nRowEff == 0)
                                    {
                                        oReturnTopupList.rtBchCode = poPara[nRow].ptBchCode;
                                        oReturnTopupList.rtCrdCode = poPara[nRow].ptCrdCode;
                                        oReturnTopupList.rtStatus = "2";
                                        aoReturnTopupList.Add(oReturnTopupList);
                                    }
                                    else
                                    {
                                        oReturnTopupList.rtBchCode = poPara[nRow].ptBchCode;
                                        oReturnTopupList.rtCrdCode = poPara[nRow].ptCrdCode;
                                        oReturnTopupList.rtStatus = "1";
                                        aoReturnTopupList.Add(oReturnTopupList);
                                    }
                                }
                                catch (EntityException oEtyExn)
                                {
                                    switch (oEtyExn.HResult)
                                    {
                                        case -2146232060:
                                            // Cannot connect database..
                                            oResult = new cmlResaoReturnTopupList();
                                            oResult.rtCode = oMsg.tMS_RespCode905;
                                            oResult.rtDesc = oMsg.tMS_RespDesc905;
                                            return oResult;
                                    }
                                }
                            }

                            // จบการทำงาน
                            oResult = new cmlResaoReturnTopupList();
                            oResult.rtCode = oMsg.tMS_RespCode1;
                            oResult.rtDesc = oMsg.tMS_RespDesc1;
                            oResult.roReturnTopupList = aoReturnTopupList;
                            return oResult;

                        }
                        else
                        {
                            // Key not allowed to use method.
                            oResult = new cmlResaoReturnTopupList();
                            oResult.rtCode = oMsg.tMS_RespCode904;
                            oResult.rtDesc = oMsg.tMS_RespDesc904;
                            return oResult;
                        }
                    }
                    else
                    {
                        // This time not allowed to use method.
                        oResult = new cmlResaoReturnTopupList();
                        oResult.rtCode = oMsg.tMS_RespCode906;
                        oResult.rtDesc = oMsg.tMS_RespDesc906;
                        return oResult;
                    }
                }
                else
                {
                    // Validate parameter model false.
                    oResult = new cmlResaoReturnTopupList();
                    oResult.rtCode = oMsg.tMS_RespCode701;
                    oResult.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                    return oResult;
                }
            }
            catch (Exception oEx)
            {
                // Return error.
                oResult = new cmlResaoReturnTopupList();
                oResult.rtCode = new cMS().tMS_RespCode900;
                oResult.rtDesc = new cMS().tMS_RespDesc900;
                return oResult;
            }
            finally
            {
                oFunc = null;
                oCons = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
                aoSysConfig = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                oResult = null;
                oComp = null;
                oCard = null;
                oReturnTopupList = null;
                aoReturnTopupList = null;
            }
        }

        /// <summary>
        ///  เติมเงินเป็นชุด // Topup List
        /// </summary>
        /// <param name="poPara"></param>
        /// <returns>
        ///     System process status.<br/>
        ///     1   : success.<br/>
        ///     701 : validate parameter model false.
        ///     802 : formate data incorrect..<br/>
        ///     900 : service process false.<br/>
        ///     904 : key not allowed to use method.<br/>
        ///     905 : cannot connect database.<br/>
        ///     906 : this time not allowed to use method.<br/>
        /// </returns>
        [Route("TopupList")]
        [HttpPost]
        public cmlResaoTopupList POST_PUNoTopupList([FromBody] List<cmlReqTopupList> poPara)
        {
            cSP oFunc;
            cCS oCons;
            cMS oMsg;
            cDatabase oDatabase;
            StringBuilder oSql;
            List<cmlTSysConfig> aoSysConfig;
            cmlResaoTopupList oResult;
            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi;
            cmlTCNMComp oComp = new cmlTCNMComp();
            cmlTFNMCard oCard = new cmlTFNMCard();
            cmlResTopupList oReturnTopupList = new cmlResTopupList();
            List<cmlResTopupList> aoReturnTopupList = new List<cmlResTopupList>();
            string tNow;
            string tCrdExpireDate;
            cResetExpired oResetExpired = new cResetExpired();
            DateTime dExpireTopup = DateTime.Now;
            string tExpireTopup;
            bool bResultReset = true;
            cResetExpired oResetExpire = new cResetExpired();
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

                oFunc = new cSP();
                oCons = new cCS();
                oMsg = new cMS();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                // Validate parameter.
                tModelErr = "";
                if (oFunc.SP_CHKbParaModel(ref tModelErr, ModelState))
                {
                    // Load configuration.
                    aoSysConfig = oFunc.SP_SYSaLoadConfiguration();

                    // Check range time use function.
                    if (oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        // Check KeyApi.
                        if (oFunc.SP_CHKbKeyApi(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            oResult = new cmlResaoTopupList();
                            nConTme = 0;
                            oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                            nCmdTme = 0;
                            oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                            oDatabase = new cDatabase(nConTme);
                            oSql = new StringBuilder();

                            for (int nRow = 0; nRow < poPara.Count; nRow++)
                            {
                                oReturnTopupList = new cmlResTopupList();
                                oSql.Clear();
                                oSql.AppendLine("SELECT ISNULL(C.FTCrdStaLocate,2) AS FTCrdStaLocate,C.FTCrdStaType");
                                oSql.AppendLine(",ISNULL(T.FCCtyTopupAuto,0) AS FCCtyTopupAuto,C.FDCrdExpireDate ");
                                oSql.AppendLine(",ISNULL(T.FNCtyExpirePeriod,0) AS FNCtyExpirePeriod ");
                                oSql.AppendLine(",ISNULL(T.FNCtyExpirePeriod,0) AS FNCtyExpirePeriod ");
                                oSql.AppendLine("FROM TFNMCard C WITH(NOLOCK) LEFT JOIN TFNMCardType T ON (C.FTCtyCode=T.FTCtyCode) ");
                                oSql.AppendLine("WHERE FTCrdCode='" + poPara[nRow].ptCrdCode + "'");

                                oCard = new cmlTFNMCard();
                                oCard = oDatabase.C_DAToSqlQuery<cmlTFNMCard>(oSql.ToString(), nCmdTme);

                                if (oCard == null)
                                {
                                    // ไม่พบข้อมุลบัตร
                                    oReturnTopupList.rtBchCode = poPara[nRow].ptBchCode;
                                    oReturnTopupList.rtCrdCode = poPara[nRow].ptCrdCode;
                                    oReturnTopupList.rtStatus = "4";
                                    aoReturnTopupList.Add(oReturnTopupList);
                                    continue;
                                }

                                if (oCard.FTCrdStaLocate != "1")
                                {
                                    // สถานะบัตรยังไม่ถูกเปิดใช้งาน
                                    oReturnTopupList.rtBchCode = poPara[nRow].ptBchCode;
                                    oReturnTopupList.rtCrdCode = poPara[nRow].ptCrdCode;
                                    oReturnTopupList.rtStatus = "6";
                                    aoReturnTopupList.Add(oReturnTopupList);
                                    continue;
                                }

                                // ตรวจสอบวันหมดอายุรหัสบัตร
                                tNow = DateTime.Now.ToString(@"yyyy-MM-dd HH:mm:ss", new CultureInfo("en-US"));
                                tCrdExpireDate = oCard.FDCrdExpireDate.ToString("yyyy-MM-dd HH:mm:ss");
                                if ((DateTime.Parse(tNow) > DateTime.Parse(tCrdExpireDate))) {
                                    oReturnTopupList.rtBchCode = poPara[nRow].ptBchCode;
                                    oReturnTopupList.rtCrdCode = poPara[nRow].ptCrdCode;
                                    oReturnTopupList.rtStatus = "5";
                                    aoReturnTopupList.Add(oReturnTopupList);
                                    continue; 
                                }

                                // ตรวจสอบอายุเงิน ถ้า FNCtyExpirePeriod = 0 ไม่ต้องตรวจสอบ
                                if (oCard.FNCtyExpirePeriod != 0)
                                {

                                    // ตรวจสอบกรณี ยังไม่เคยเติมเงิน ไม่ต้องตรวจสอบวัน Expire ของยอดเงิน
                                    if (oCard.FDCrdLastTopup != null)
                                    {
                                        switch (oCard.FNCtyExpiredType)
                                        {
                                            case 1:
                                                dExpireTopup = Convert.ToDateTime(oCard.FDCrdLastTopup).AddHours(Convert.ToInt32(oCard.FNCtyExpirePeriod));
                                                break;
                                            case 2:
                                                dExpireTopup = Convert.ToDateTime(oCard.FDCrdLastTopup).AddDays(Convert.ToInt32(oCard.FNCtyExpirePeriod));
                                                break;
                                            case 3:
                                                dExpireTopup = Convert.ToDateTime(oCard.FDCrdLastTopup).AddMonths(Convert.ToInt32(oCard.FNCtyExpirePeriod));
                                                break;
                                            case 4:
                                                dExpireTopup = Convert.ToDateTime(oCard.FDCrdLastTopup).AddYears(Convert.ToInt32(oCard.FNCtyExpirePeriod));
                                                break;
                                        }

                                        tExpireTopup = dExpireTopup.ToString(@"yyyy-MM-dd HH:mm:ss", new CultureInfo("en-US"));

                                        if (DateTime.Parse(tNow) > DateTime.Parse(tExpireTopup))
                                        {
                                            // ยอดเงินหมดอายุ ResetExpire
                                            bResultReset = oResetExpire.C_SETbResetExpired(poPara[nRow].ptCrdCode, poPara[nRow].ptBchCode, aoSysConfig);
                                            if (bResultReset == false)
                                            {
                                                oReturnTopupList.rtBchCode = poPara[nRow].ptBchCode;
                                                oReturnTopupList.rtCrdCode = poPara[nRow].ptCrdCode;
                                                oReturnTopupList.rtStatus = "3";
                                                aoReturnTopupList.Add(oReturnTopupList);
                                                continue;
                                            }
                                        }
                                    }

                                }

                                //////
                                ////Update สถานะบัตร
                                ////oSql.Clear();
                                ////oSql.AppendLine("UPDATE TFNMCard WITH (ROWLOCK) SET FTCrdStaLocate='2' ");
                                ////oSql.AppendLine("WHERE FTCrdCode='" + poPara[nRow].ptCrdCode + "'");

                                //========================================
                                oSql.Clear();
                                oSql.AppendLine("BEGIN TRANSACTION ");
                                oSql.AppendLine("  SAVE TRANSACTION Topup ");
                                oSql.AppendLine("  BEGIN TRY ");

                                if (poPara[nRow].ptAuto == "0")
                                {
                                    // เติมเงินแบบ Manual
                                    // Insert transection Sale
                                    oSql.AppendLine("     INSERT INTO TFNTCrdTopUp WITH(ROWLOCK)");
                                    oSql.AppendLine("     (");
                                    oSql.AppendLine("	  FTBchCode,FTTxnDocType,FTCrdCode,");
                                    oSql.AppendLine("	  FDTxnDocDate,FTBchCodeRef,FCTxnValue,");
                                    oSql.AppendLine("	  FTTxnStaPrc");
                                    oSql.AppendLine("     )");
                                    oSql.AppendLine("     VALUES");
                                    oSql.AppendLine("     (");
                                    oSql.AppendLine("	  '" + oComp.FTBchcode + "','1','" + poPara[nRow].ptCrdCode + "',");
                                    oSql.AppendLine("	  GETDATE(),'" + poPara[nRow].ptBchCode + "','" + poPara[nRow].pcTxnValue + "',");
                                    oSql.AppendLine("	  '1'");
                                    oSql.AppendLine("     )");

                                    // Update Master Card
                                    oSql.AppendLine("     UPDATE TFNMCard WITH (ROWLOCK) SET ");
                                    oSql.AppendLine("     FCCrdValue=(FCCrdValue + " + poPara[nRow].pcTxnValue + "),");
                                    oSql.AppendLine("     FDCrdLastTopup=GETDATE() ");
                                    oSql.AppendLine("     WHERE FTCrdCode='" + poPara[nRow].ptCrdCode + "'");
                                    oSql.AppendLine("     COMMIT TRANSACTION Topup");
                                    oSql.AppendLine("  END TRY");

                                }
                                else if(poPara[nRow].ptAuto == "1")
                                {
                                    // เติมเงินแบบ Auto
                                    // Insert transection Sale
                                    oSql.AppendLine("     INSERT INTO TFNTCrdTopUp WITH(ROWLOCK)");
                                    oSql.AppendLine("     (");
                                    oSql.AppendLine("	  FTBchCode,FTTxnDocType,FTCrdCode,");
                                    oSql.AppendLine("	  FDTxnDocDate,FTBchCodeRef,FCTxnValue,");
                                    oSql.AppendLine("	  FTTxnStaPrc");
                                    oSql.AppendLine("     )");
                                    oSql.AppendLine("     VALUES");
                                    oSql.AppendLine("     (");
                                    oSql.AppendLine("	  '" + oComp.FTBchcode + "','1','" + poPara[nRow].ptCrdCode + "',");
                                    oSql.AppendLine("	  GETDATE(),'" + poPara[nRow].ptBchCode + "','" + oCard.FCCtyTopupAuto + "',");
                                    oSql.AppendLine("	  '1'");
                                    oSql.AppendLine("     )");

                                    // Update Master Card
                                    oSql.AppendLine("     UPDATE TFNMCard WITH (ROWLOCK) SET ");
                                    oSql.AppendLine("     FCCrdValue=(FCCrdValue + " + oCard.FCCtyTopupAuto + "),");
                                    oSql.AppendLine("     FDCrdLastTopup=GETDATE() ");
                                    oSql.AppendLine("     WHERE FTCrdCode='" + poPara[nRow].ptCrdCode + "'");
                                    oSql.AppendLine("     COMMIT TRANSACTION Topup");
                                    oSql.AppendLine("  END TRY");

                                }

                                oSql.AppendLine("  BEGIN CATCH");
                                oSql.AppendLine("   ROLLBACK TRANSACTION Topup");
                                oSql.AppendLine("  END CATCH");
                                try
                                {
                                    // Confuguration database.
                                    nConTme = 0;
                                    oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                                    nCmdTme = 0;
                                    oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                                    oDatabase = new cDatabase(nConTme);
                                    nRowEff = oDatabase.C_DATnExecuteSql(oSql.ToString(), nCmdTme);

                                    if (nRowEff == 0)
                                    {
                                        oReturnTopupList.rtBchCode = poPara[nRow].ptBchCode;
                                        oReturnTopupList.rtCrdCode = poPara[nRow].ptCrdCode;
                                        oReturnTopupList.rtStatus = "2";
                                        aoReturnTopupList.Add(oReturnTopupList);
                                    }
                                    else
                                    {
                                        oReturnTopupList.rtBchCode = poPara[nRow].ptBchCode;
                                        oReturnTopupList.rtCrdCode = poPara[nRow].ptCrdCode;
                                        oReturnTopupList.rtStatus = "1";
                                        aoReturnTopupList.Add(oReturnTopupList);
                                    }
                                }
                                catch (EntityException oEtyExn)
                                {
                                    switch (oEtyExn.HResult)
                                    {
                                        case -2146232060:
                                            // Cannot connect database..
                                            oResult = new cmlResaoTopupList();
                                            oResult.rtCode = oMsg.tMS_RespCode905;
                                            oResult.rtDesc = oMsg.tMS_RespDesc905;
                                            return oResult;
                                    }
                                }
                            }

                            // จบการทำงาน
                            oResult = new cmlResaoTopupList();
                            oResult.rtCode = oMsg.tMS_RespCode1;
                            oResult.rtDesc = oMsg.tMS_RespDesc1;
                            oResult.roTopupList = aoReturnTopupList;
                            return oResult;

                        }
                        else
                        {
                            // Key not allowed to use method.
                            oResult = new cmlResaoTopupList();
                            oResult.rtCode = oMsg.tMS_RespCode904;
                            oResult.rtDesc = oMsg.tMS_RespDesc904;
                            return oResult;
                        }
                    }
                    else
                    {
                        // This time not allowed to use method.
                        oResult = new cmlResaoTopupList();
                        oResult.rtCode = oMsg.tMS_RespCode906;
                        oResult.rtDesc = oMsg.tMS_RespDesc906;
                        return oResult;
                    }
                }
                else
                {
                    // Validate parameter model false.
                    oResult = new cmlResaoTopupList();
                    oResult.rtCode = oMsg.tMS_RespCode701;
                    oResult.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                    return oResult;
                }
            }
            catch (Exception oEx)
            {
                // Return error.
                oResult = new cmlResaoTopupList();
                oResult.rtCode = new cMS().tMS_RespCode900;
                oResult.rtDesc = new cMS().tMS_RespDesc900;
                return oResult;
            }
            finally
            {
                oFunc = null;
                oCons = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
                aoSysConfig = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                oResult = null;
                oComp = null;
                oCard = null;
                oReturnTopupList = null;
                aoReturnTopupList = null;
            }
        }

    }
}
