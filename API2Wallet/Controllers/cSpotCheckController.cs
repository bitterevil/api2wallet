﻿using API2Wallet.Class;
using API2Wallet.Class.SpotCheck;
using API2Wallet.Class.Standard;
using API2Wallet.Models;
using API2Wallet.Models.WebService.Request.SpotCheck;
using API2Wallet.Models.WebService.Response.SpotCheck;
using API2Wallet.Models.WebService.Request.CardHistory;
using API2Wallet.Models.WebService.Response.CardHistory;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Data;

namespace API2Wallet.Controllers
{
    /// <summary>
    /// SpotCheck information.
    /// </summary>
    [RoutePrefix(cCS.tCS_APIVer + "/SpotCheck")]
    public class cSpotCheckController : ApiController
    {

        /// <summary>
        /// ตรวจสอบยอดคงเหลือ
        /// </summary>
        /// <param name="poPara"></param>
        /// <returns>
        ///     System process status.<br/>
        ///     1   : success.<br/>
        ///     701 : validate parameter model false.
        ///     712 : Not found Card.
        ///     802 : formate data incorrect..<br/>
        ///     900 : service process false.<br/>
        ///     904 : key not allowed to use method.<br/>
        ///     905 : cannot connect database.<br/>
        ///     906 : this time not allowed to use method.<br/>
        /// </returns>
        [Route("Check")]
        [HttpPost]
        public cmlResSpotChk POST_PUNoInsTopup([FromBody] cmlReqSpotChk poPara)
        {
            cSP oFunc;
            cCS oCons;
            cMS oMsg;
            cDatabase oDatabase;
            StringBuilder oSql;
            cSpotCheck oSpotCheck = new cSpotCheck();
            List<cmlTSysConfig> aoSysConfig;
            cmlResSpotChk oResSpotCheckErr;
            cmlResSpotChk oResult;
            cmlResSpotChk oResTopup = new cmlResSpotChk();
            int nConTme, nCmdTme; 
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc;
            bool bVerifyPara;
            cmlTCNMComp oComp = new cmlTCNMComp();
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

                oResult = new cmlResSpotChk();
                oFunc = new cSP();
                oCons = new cCS();
                oMsg = new cMS();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                // Validate parameter.
                tModelErr = "";
                if (oFunc.SP_CHKbParaModel(ref tModelErr, ModelState))
                {
                    // Load configuration.
                    aoSysConfig = oFunc.SP_SYSaLoadConfiguration();

                    // Check range time use function.
                    if (oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        // Check KeyApi.
                        if (oFunc.SP_CHKbKeyApi(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            // Varify parameter value.
                            oSpotCheck = new cSpotCheck();
                            bVerifyPara = oSpotCheck.C_DATbVerifySpotCheck(poPara, aoSysConfig, out tErrCode, out tErrDesc, out oResSpotCheckErr);
                            if (bVerifyPara == true)
                            {

                                try
                                {

                                }
                                catch (EntityException oEtyExn)
                                {
                                    switch (oEtyExn.HResult)
                                    {
                                        case -2146232060:
                                            // Cannot connect database..
                                            oResult.rtCode = oMsg.tMS_RespCode905;
                                            oResult.rtDesc = oMsg.tMS_RespDesc905;
                                            return oResult;
                                    }
                                }

                                oSql = new StringBuilder();
                                oSql.Clear();
                                oSql.AppendLine("SELECT TOP 1 ISNULL(C.FCCrdValue,0) AS 'rcTxnValue'");
                                oSql.AppendLine(",T.FCCtyDeposit AS 'rcCtyDeposit'");
                                oSql.AppendLine(",ISNULL((C.FCCrdValue-T.FCCtyDeposit),0) AS 'rcTxnValueAvb'");
                                oSql.AppendLine(",C.FTCtyCode AS 'rtCtyCode'");
                                oSql.AppendLine(",FDCrdExpireDate AS 'rdCrdExpireDate'");
                                oSql.AppendLine(",L.FTCrdName AS 'rtCrdName'");
                                oSql.AppendLine("FROM TFNMCard C WITH(NOLOCK) ");
                                oSql.AppendLine("LEFT JOIN TFNMCardType T ON (C.FTCtyCode=T.FTCtyCode)");
                                oSql.AppendLine("LEFT JOIN TFNMCard_L L ON (C.FTCrdCode=L.FTCrdCode)");
                                oSql.AppendLine("WHERE  C.FTCrdCode='" + poPara.ptCrdCode + "'");

                                nConTme = 0;
                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                                nCmdTme = 0;
                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                                oDatabase = new cDatabase(nConTme);
                                oResTopup = oDatabase.C_DAToSqlQuery<cmlResSpotChk>(oSql.ToString(), nCmdTme);
                                oResult.rtCode = oMsg.tMS_RespCode1;
                                oResult.rtDesc = oMsg.tMS_RespDesc1;
                                oResult.rcTxnValue = oResTopup.rcTxnValue;
                                oResult.rcCtyDeposit = oResTopup.rcCtyDeposit;
                                oResult.rcTxnValueAvb = oResTopup.rcTxnValueAvb;
                                oResult.rtCtyCode = oResTopup.rtCtyCode;
                                oResult.rtCrdName = oResTopup.rtCrdName;
                                oResult.rdCrdExpireDate = oResTopup.rdCrdExpireDate;
                                return oResult;
                            }
                            else
                            {
                                // Varify parameter value false.
                                oResult.rtCode = tErrCode;
                                oResult.rtDesc = tErrDesc;
                                return oResult;
                            }

                        }
                        else
                        {
                            // Key not allowed to use method.
                            oResult.rtCode = oMsg.tMS_RespCode904;
                            oResult.rtDesc = oMsg.tMS_RespDesc904;
                            return oResult;
                        }
                    }
                    else
                    {
                        // This time not allowed to use method.
                        oResult.rtCode = oMsg.tMS_RespCode906;
                        oResult.rtDesc = oMsg.tMS_RespDesc906;
                        return oResult;
                    }
                }
                else
                {
                    // Validate parameter model false.
                    oResult.rtCode = oMsg.tMS_RespCode701;
                    oResult.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                    return oResult;
                }
            }
            catch (Exception oEx)
            {
                // Return error.
                oResult = new cmlResSpotChk();
                oResult.rtCode = new cMS().tMS_RespCode900;
                oResult.rtDesc = new cMS().tMS_RespDesc900;
                return oResult;
            }
            finally
            {
                oFunc = null;
                oCons = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
                aoSysConfig = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                oResult = null;
                oResTopup = null;
                oComp = null;
            }
        }

       // [Route("CrdHistory")]
       // [HttpPost]
       //public cmlResCardHistory POST_PUNoCrdHistory([FromBody] cmlReqCardHistory poPrm)
       // {
       //     cSP oFunc;
       //     cCS oCons;
       //     cMS oMsg;
       //     cDatabase oDatabase;
       //     StringBuilder oSql;
       //     List<cmlTSysConfig> aoSysConfig;
       //     cmlResCardHistory oResResult;

       //     int nConTme, nCmdTme;
       //     string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc;
       //     bool bVerifyPara;
       //     cmlTCNMComp oComp = new cmlTCNMComp();

       //     try
       //     {
       //         Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
       //         Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
       //         oFunc = new cSP();
       //         oCons = new cCS();
       //         oMsg = new cMS();
       //         oResResult = new cmlResCardHistory();

       //         // Get method name.
       //         tFuncName = MethodBase.GetCurrentMethod().Name;

       //         // Validate parameter.
       //         tModelErr = "";

       //         if (oFunc.SP_CHKbParaModel(ref tModelErr, ModelState))
       //         {
       //             // Load configuration.
       //             aoSysConfig = oFunc.SP_SYSaLoadConfiguration();

       //             // Check range time use function.
       //             if (oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
       //             {
                     
       //                 tKeyApi = "";
       //                 // Check KeyApi.
       //                 if (oFunc.SP_CHKbKeyApi(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
       //                 {
       //                     oDatabase = new cDatabase();
       //                     oSql = new StringBuilder();

       //                     oSql.Clear();
       //                     oSql.AppendLine(" DECLARE @FNQty AS Integer");
       //                     oSql.AppendLine(" DECLARE @FTCrdCode AS VARCHAR(50) ");
       //                     oSql.AppendLine(" DECLARE @FTBchCode AS VARCHAR(50) ");
       //                     oSql.AppendLine(" SET @FNQty = '" + poPrm.pnQty + "' ");
       //                     oSql.AppendLine(" SET @FTCrdCode = '" + poPrm.ptCrdNo +"' ");
       //                     oSql.AppendLine(" SET @FTBchCode = '" + poPrm.ptBchCode +"' ");
       //                     oSql.AppendLine(" SELECT TOP(@FNQty)  FTTxnDocType AS ptType,FDTxnDocDate AS ptDocDate ");
       //                     oSql.AppendLine(" ,FTBchCodeRef AS ptBchRef,FTTxnDocNoRef AS ptDocRef ,FTTxnPosCode AS ptPos ");
       //                     oSql.AppendLine(" FROM ");
       //                     oSql.AppendLine(" (SELECT FTCrdCode,FTBchCode,FTTxnDocType,FDTxnDocDate,FTBchCodeRef,FTTxnDocNoRef,FTTxnPosCode");
       //                     oSql.AppendLine(" FROM TFNTCrdTopup ");
       //                     oSql.AppendLine(" UNION ");
       //                     oSql.AppendLine(" SELECT FTCrdCode,FTBchCode,FTTxnDocType,FDTxnDocDate,FTBchCodeRef,FTTxnDocNoRef,FTTxnPosCode ");
       //                     oSql.AppendLine(" FROM TFNTCrdSale) AS CrdHistory");
       //                     oSql.AppendLine(" WHERE FTCrdCode = @FTCrdCode AND FTBchCode = @FTBchCode ");

       //                     if (poPrm.ptSort == "0")
       //                     {
       //                         oSql.AppendLine("ORDER BY FDTxnDocDate ASC");
                               
       //                     }
       //                     else
       //                     {
       //                         oSql.AppendLine("ORDER BY FDTxnDocDate DESC");
       //                     }


       //                     nConTme = 0;
       //                     oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
       //                     nCmdTme = 0;
       //                     oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                          
       //                     //Execute Add to list
       //                     oDatabase = new cDatabase(nConTme);
       //                     oResResult.roCrdHistory = oDatabase.C_DATaSqlQuery<cmlResCardHistoryList>(oSql.ToString());
       //                     oResResult.rtCode = oMsg.tMS_RespCode1;
       //                     oResResult.rtDesc = oMsg.tMS_RespDesc1;
       //                     return oResResult;
       //                 }
       //                 else
       //                 {
       //                     // Key not allowed to use method.
       //                     oResResult.rtCode = oMsg.tMS_RespCode904;
       //                     oResResult.rtDesc = oMsg.tMS_RespDesc904;
       //                     return oResResult;
       //                 }
       //             }
       //             else
       //             {
       //                 // This time not allowed to use method.
       //                 oResResult.rtCode = oMsg.tMS_RespCode906;
       //                 oResResult.rtDesc = oMsg.tMS_RespDesc906;
       //                 return oResResult;
       //             }
       //         }
       //         else
       //         {
       //             // Validate parameter model false.
       //             oResResult.rtCode = oMsg.tMS_RespCode701;
       //             oResResult.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
       //             return oResResult;
       //         }
       //     }
       //     catch(Exception oEx)
       //     {
       //         oResResult = new cmlResCardHistory();
       //         oResResult.rtCode = new cMS().tMS_RespCode900;
       //         oResResult.rtDesc = new cMS().tMS_RespDesc900;
       //         return oResResult;
       //     }
       //     finally
       //     {
       //         oFunc = null;
       //         oCons = null;
       //         oMsg = null;
       //         oDatabase = null;
       //         oSql = null;
       //         aoSysConfig = null;
       //         GC.Collect();
       //         GC.WaitForPendingFinalizers();
       //         GC.Collect();
       //         oResResult = null;
       //         oComp = null;
       //     }
          
       // }


    }
}

