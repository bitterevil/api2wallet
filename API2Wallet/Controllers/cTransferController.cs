﻿using API2Wallet.Class;
using API2Wallet.Class.Standard;
using API2Wallet.Class.Tranfer;
using API2Wallet.Models;
using API2Wallet.Models.WebService.Request.Tranfer;
using API2Wallet.Models.WebService.Response.Tranfer;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http;

namespace API2Wallet.Controllers
{

    ///// <summary>
    ///// SpotCheck information.
    ///// </summary>
    //[RoutePrefix(cCS.tCS_APIVer + "/Tranfer")]
    //public class cTransferController : ApiController
    //{
    //    /// <summary>
    //    /// เติมเงิน Topup
    //    /// </summary>
    //    /// <param name="poPara"></param>
    //    /// <returns>
    //    ///     System process status.<br/>
    //    ///     1   : success.<br/>
    //    ///     701 : validate parameter model false.
    //    ///     712 : Not found Card.
    //    ///     713 : Card Date expired.
    //    ///     716 : ResetExpire card unsuccess.".
    //    ///     718 : Card Topup expired
    //    ///     719 : Not enough money for transfer.
    //    ///     802 : formate data incorrect..<br/>
    //    ///     900 : service process false.<br/>
    //    ///     904 : key not allowed to use method.<br/>
    //    ///     905 : cannot connect database.<br/>
    //    ///     906 : this time not allowed to use method.<br/>
    //    /// </returns>
    //    [Route("OUT")]
    //    [HttpPost]
    //    public cmlResTnfOut POST_PUNoTnfOut([FromBody] cmlReqTnfOut poPara)
    //    {
    //        cSP oFunc;
    //        cCS oCons;
    //        cMS oMsg;
    //        cDatabase oDatabase;
    //        StringBuilder oSql;
    //        cTranfer oTranfer;
    //        List<cmlTSysConfig> aoSysConfig;
    //        cmlResTnfOut oResTnfOutErr;
    //        cmlResTnfOut oResult;
    //        cmlResTnfOut oResTnfOut = new cmlResTnfOut();
    //        int nRowEff, nConTme, nCmdTme;
    //        string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc;
    //        bool bVerifyPara;
    //        cmlTCNMComp oComp = new cmlTCNMComp();
    //        try
    //        {
    //            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
    //            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

    //            oResult = new cmlResTnfOut();
    //            oFunc = new cSP();
    //            oCons = new cCS();
    //            oMsg = new cMS();

    //            // Get method name.
    //            tFuncName = MethodBase.GetCurrentMethod().Name;

    //            // Validate parameter.
    //            tModelErr = "";
    //            if (oFunc.SP_CHKbParaModel(ref tModelErr, ModelState))
    //            {
    //                // Load configuration.
    //                aoSysConfig = oFunc.SP_SYSaLoadConfiguration();

    //                // Check range time use function.
    //                if (oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
    //                {
    //                    tKeyApi = "";
    //                    // Check KeyApi.
    //                    if (oFunc.SP_CHKbKeyApi(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
    //                    {
    //                        // Varify parameter value.
    //                        oTranfer = new cTranfer();
    //                        bVerifyPara = oTranfer.C_DATbVerifyTnfOut(poPara, aoSysConfig, out tErrCode, out tErrDesc, out oResTnfOutErr);
    //                        if (bVerifyPara == true)
    //                        {
    //                            nConTme = 0;
    //                            oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
    //                            nCmdTme = 0;
    //                            oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
    //                            oDatabase = new cDatabase(nConTme);
    //                            oSql = new StringBuilder();

    //                            // Get Company Code
    //                            oSql.Clear();
    //                            oSql.AppendLine("SELECT TOP 1 [FTCmpCode],[FTCmpTel],[FTCmpFax]");
    //                            oSql.AppendLine(",[FTBchcode],[FTCmpWhsInOrEx],[FTCmpRetInOrEx]");
    //                            oSql.AppendLine(",[FTCmpEmail],[FTRteCode],[FTVatCode]");
    //                            oSql.AppendLine(" FROM TCNMComp WITH (NOLOCK) ");
    //                            oComp = oDatabase.C_DAToSqlQuery<cmlTCNMComp>(oSql.ToString(), nCmdTme);

    //                            oSql.Clear();
    //                            oSql.AppendLine("BEGIN TRANSACTION ");
    //                            oSql.AppendLine("  SAVE TRANSACTION TnfOut ");
    //                            oSql.AppendLine("  BEGIN TRY ");

    //                            // Insert transection Sale
    //                            oSql.AppendLine("     INSERT INTO TFNTCrdTopUp WITH(ROWLOCK)");
    //                            oSql.AppendLine("     (");
    //                            oSql.AppendLine("	  FTBchCode,FTTxnDocType,FTCrdCode,");
    //                            oSql.AppendLine("	  FDTxnDocDate,FTBchCodeRef,FCTxnValue,");
    //                            oSql.AppendLine("	  FTTxnStaPrc");
    //                            oSql.AppendLine("     )");
    //                            oSql.AppendLine("     VALUES");
    //                            oSql.AppendLine("     (");
    //                            oSql.AppendLine("	  '" + oComp.FTBchcode + "','8','" + poPara.ptCrdCode + "',");
    //                            oSql.AppendLine("	  GETDATE(),'" + poPara.ptBchCode + "','" + poPara.pcTxnValue + "',");
    //                            oSql.AppendLine("	  '1'");
    //                            oSql.AppendLine("     )");

    //                            // Update Master Card
    //                            oSql.AppendLine("     UPDATE TFNMCard WITH (ROWLOCK) SET ");
    //                            oSql.AppendLine("     FCCrdValue=(FCCrdValue - " + poPara.pcTxnValue + ")");
    //                            //oSql.AppendLine("     ,FDCrdLastTopup=GETDATE() ");
    //                            oSql.AppendLine("     WHERE FTCrdCode='" + poPara.ptCrdCode + "'");
    //                            oSql.AppendLine("     COMMIT TRANSACTION TnfOut");
    //                            oSql.AppendLine("  END TRY");

    //                            oSql.AppendLine("  BEGIN CATCH");
    //                            oSql.AppendLine("   ROLLBACK TRANSACTION TnfOut");
    //                            oSql.AppendLine("  END CATCH");

    //                            try
    //                            {
    //                                // Confuguration database.
    //                                nConTme = 0;
    //                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
    //                                nCmdTme = 0;
    //                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
    //                                oDatabase = new cDatabase(nConTme);
    //                                nRowEff = oDatabase.C_DATnExecuteSql(oSql.ToString(), nCmdTme);

    //                                if (nRowEff == 0)
    //                                {
    //                                    oResult.rtCode = oMsg.tMS_RespCode900;
    //                                    oResult.rtDesc = oMsg.tMS_RespDesc900;
    //                                    return oResult;
    //                                }
    //                            }
    //                            catch (EntityException oEtyExn)
    //                            {
    //                                switch (oEtyExn.HResult)
    //                                {
    //                                    case -2146232060:
    //                                        // Cannot connect database..
    //                                        oResult.rtCode = oMsg.tMS_RespCode905;
    //                                        oResult.rtDesc = oMsg.tMS_RespDesc905;
    //                                        return oResult;
    //                                }
    //                            }

    //                            oSql = new StringBuilder();
    //                            oSql.Clear();
    //                            oSql.AppendLine("SELECT TOP 1 ISNULL(C.FCCrdValue,0) AS 'rcTxnValue'");
    //                            oSql.AppendLine(",T.FCCtyDeposit AS 'rcCtyDeposit'");
    //                            oSql.AppendLine(",ISNULL((C.FCCrdValue-T.FCCtyDeposit),0) AS 'rcTxnValueAvb'");
    //                            oSql.AppendLine(",C.FTCtyCode AS 'rtCtyCode'");
    //                            oSql.AppendLine(",FDCrdExpireDate AS 'rdCrdExpireDate'");
    //                            oSql.AppendLine(",L.FTCrdName AS 'rtCrdName'");
    //                            oSql.AppendLine("FROM TFNMCard C");
    //                            oSql.AppendLine("LEFT JOIN TFNMCardType T ON (C.FTCtyCode=T.FTCtyCode)");
    //                            oSql.AppendLine("LEFT JOIN TFNMCard_L L ON (C.FTCrdCode=L.FTCrdCode)");
    //                            oSql.AppendLine("WHERE  C.FTCrdCode='" + poPara.ptCrdCode + "'");

    //                            nConTme = 0;
    //                            oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
    //                            nCmdTme = 0;
    //                            oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
    //                            oDatabase = new cDatabase(nConTme);
    //                            oResTnfOut = oDatabase.C_DAToSqlQuery<cmlResTnfOut>(oSql.ToString(), nCmdTme);
    //                            oResult.rtCode = oMsg.tMS_RespCode1;
    //                            oResult.rtDesc = oMsg.tMS_RespDesc1;
    //                            oResult.rcTxnValue = oResTnfOut.rcTxnValue;
    //                            oResult.rcCtyDeposit = oResTnfOut.rcCtyDeposit;
    //                            oResult.rcTxnValueAvb = oResTnfOut.rcTxnValueAvb;
    //                            oResult.rtCtyCode = oResTnfOut.rtCtyCode;
    //                            oResult.rtCrdName = oResTnfOut.rtCrdName;
    //                            oResult.rdCrdExpireDate = oResTnfOut.rdCrdExpireDate;
    //                            return oResult;
    //                        }
    //                        else
    //                        {
    //                            // Varify parameter value false.
    //                            oResult.rtCode = tErrCode;
    //                            oResult.rtDesc = tErrDesc;
    //                            return oResult;
    //                        }

    //                    }
    //                    else
    //                    {
    //                        // Key not allowed to use method.
    //                        oResult.rtCode = oMsg.tMS_RespCode904;
    //                        oResult.rtDesc = oMsg.tMS_RespDesc904;
    //                        return oResult;
    //                    }
    //                }
    //                else
    //                {
    //                    // This time not allowed to use method.
    //                    oResult.rtCode = oMsg.tMS_RespCode906;
    //                    oResult.rtDesc = oMsg.tMS_RespDesc906;
    //                    return oResult;
    //                }
    //            }
    //            else
    //            {
    //                // Validate parameter model false.
    //                oResult.rtCode = oMsg.tMS_RespCode701;
    //                oResult.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
    //                return oResult;
    //            }
    //        }
    //        catch (Exception oEx)
    //        {
    //            // Return error.
    //            oResult = new cmlResTnfOut();
    //            oResult.rtCode = new cMS().tMS_RespCode900;
    //            oResult.rtDesc = new cMS().tMS_RespDesc900;
    //            return oResult;
    //        }
    //        finally
    //        {
    //            oFunc = null;
    //            oCons = null;
    //            oMsg = null;
    //            oDatabase = null;
    //            oSql = null;
    //            aoSysConfig = null;
    //            oResTnfOutErr = null;
    //            GC.Collect();
    //            GC.WaitForPendingFinalizers();
    //            GC.Collect();
    //            oTranfer = null;
    //            oResult = null;
    //            oResTnfOut = null;
    //            oComp = null;
    //        }
    //    }

    //    /// <summary>
    //    /// เติมเงิน Topup
    //    /// </summary>
    //    /// <param name="poPara"></param>
    //    /// <returns>
    //    ///     System process status.<br/>
    //    ///     1   : success.<br/>
    //    ///     701 : validate parameter model false.
    //    ///     712 : Not found Card.
    //    ///     713 : Card Date expired.
    //    ///     716 : ResetExpire card unsuccess.".
    //    ///     718 : Card Topup expired
    //    ///     719 : Not enough money for transfer.
    //    ///     802 : formate data incorrect..<br/>
    //    ///     900 : service process false.<br/>
    //    ///     904 : key not allowed to use method.<br/>
    //    ///     905 : cannot connect database.<br/>
    //    ///     906 : this time not allowed to use method.<br/>
    //    /// </returns>
    //    [Route("IN")]
    //    [HttpPost]
    //    public cmlResTnfIn POST_PUNoTnfIn([FromBody] cmlReqTnfIn poPara)
    //    {
    //        cSP oFunc;
    //        cCS oCons;
    //        cMS oMsg;
    //        cDatabase oDatabase;
    //        StringBuilder oSql;
    //        cTranfer oTranfer;
    //        List<cmlTSysConfig> aoSysConfig;
    //        cmlResTnfIn oResTnfInErr;
    //        cmlResTnfIn oResult;
    //        cmlResTnfIn oResTopup = new cmlResTnfIn();
    //        int nRowEff, nConTme, nCmdTme;
    //        string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc;
    //        bool bVerifyPara;
    //        cmlTCNMComp oComp = new cmlTCNMComp();
    //        try
    //        {
    //            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
    //            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

    //            oResult = new cmlResTnfIn();
    //            oFunc = new cSP();
    //            oCons = new cCS();
    //            oMsg = new cMS();

    //            // Get method name.
    //            tFuncName = MethodBase.GetCurrentMethod().Name;

    //            // Validate parameter.
    //            tModelErr = "";
    //            if (oFunc.SP_CHKbParaModel(ref tModelErr, ModelState))
    //            {
    //                // Load configuration.
    //                aoSysConfig = oFunc.SP_SYSaLoadConfiguration();

    //                // Check range time use function.
    //                if (oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
    //                {
    //                    tKeyApi = "";
    //                    // Check KeyApi.
    //                    if (oFunc.SP_CHKbKeyApi(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
    //                    {
    //                        // Varify parameter value.
    //                        oTranfer = new cTranfer();
    //                        bVerifyPara = oTranfer.C_DATbVerifyTnfIn(poPara, aoSysConfig, out tErrCode, out tErrDesc, out oResTnfInErr);
    //                        if (bVerifyPara == true)
    //                        {
    //                            nConTme = 0;
    //                            oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
    //                            nCmdTme = 0;
    //                            oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
    //                            oDatabase = new cDatabase(nConTme);
    //                            oSql = new StringBuilder();

    //                            // Get Company Code
    //                            oSql.Clear();
    //                            oSql.AppendLine("SELECT TOP 1 [FTCmpCode],[FTCmpTel],[FTCmpFax]");
    //                            oSql.AppendLine(",[FTBchcode],[FTCmpWhsInOrEx],[FTCmpRetInOrEx]");
    //                            oSql.AppendLine(",[FTCmpEmail],[FTRteCode],[FTVatCode]");
    //                            oSql.AppendLine(" FROM TCNMComp WITH (NOLOCK) ");
    //                            oComp = oDatabase.C_DAToSqlQuery<cmlTCNMComp>(oSql.ToString(), nCmdTme);

    //                            oSql.Clear();
    //                            oSql.AppendLine("BEGIN TRANSACTION ");
    //                            oSql.AppendLine("  SAVE TRANSACTION TnfOut ");
    //                            oSql.AppendLine("  BEGIN TRY ");

    //                            // Insert transection Sale
    //                            oSql.AppendLine("     INSERT INTO TFNTCrdTopUp WITH(ROWLOCK)");
    //                            oSql.AppendLine("     (");
    //                            oSql.AppendLine("	  FTBchCode,FTTxnDocType,FTCrdCode,");
    //                            oSql.AppendLine("	  FDTxnDocDate,FTBchCodeRef,FCTxnValue,");
    //                            oSql.AppendLine("	  FTTxnStaPrc");
    //                            oSql.AppendLine("     )");
    //                            oSql.AppendLine("     VALUES");
    //                            oSql.AppendLine("     (");
    //                            oSql.AppendLine("	  '" + oComp.FTBchcode + "','9','" + poPara.ptCrdCode + "',");
    //                            oSql.AppendLine("	  GETDATE(),'" + poPara.ptBchCode + "','" + poPara.pcTxnValue + "',");
    //                            oSql.AppendLine("	  '1'");
    //                            oSql.AppendLine("     )");

    //                            // Update Master Card
    //                            oSql.AppendLine("     UPDATE TFNMCard WITH (ROWLOCK) SET ");
    //                            oSql.AppendLine("     FCCrdValue=(FCCrdValue + " + poPara.pcTxnValue + ")");
    //                            //oSql.AppendLine("     ,FDCrdLastTopup=GETDATE() ");
    //                            oSql.AppendLine("     WHERE FTCrdCode='" + poPara.ptCrdCode + "'");
    //                            oSql.AppendLine("     COMMIT TRANSACTION TnfOut");
    //                            oSql.AppendLine("  END TRY");

    //                            oSql.AppendLine("  BEGIN CATCH");
    //                            oSql.AppendLine("   ROLLBACK TRANSACTION TnfOut");
    //                            oSql.AppendLine("  END CATCH");

    //                            try
    //                            {
    //                                // Confuguration database.
    //                                nConTme = 0;
    //                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
    //                                nCmdTme = 0;
    //                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
    //                                oDatabase = new cDatabase(nConTme);
    //                                nRowEff = oDatabase.C_DATnExecuteSql(oSql.ToString(), nCmdTme);

    //                                if (nRowEff == 0)
    //                                {
    //                                    oResult.rtCode = oMsg.tMS_RespCode900;
    //                                    oResult.rtDesc = oMsg.tMS_RespDesc900;
    //                                    return oResult;
    //                                }
    //                            }
    //                            catch (EntityException oEtyExn)
    //                            {
    //                                switch (oEtyExn.HResult)
    //                                {
    //                                    case -2146232060:
    //                                        // Cannot connect database..
    //                                        oResult.rtCode = oMsg.tMS_RespCode905;
    //                                        oResult.rtDesc = oMsg.tMS_RespDesc905;
    //                                        return oResult;
    //                                }
    //                            }

    //                            oSql = new StringBuilder();
    //                            oSql.Clear();
    //                            oSql.AppendLine("SELECT TOP 1 ISNULL(C.FCCrdValue,0) AS 'rcTxnValue'");
    //                            oSql.AppendLine(",T.FCCtyDeposit AS 'rcCtyDeposit'");
    //                            oSql.AppendLine(",ISNULL((C.FCCrdValue-T.FCCtyDeposit),0) AS 'rcTxnValueAvb'");
    //                            oSql.AppendLine(",C.FTCtyCode AS 'rtCtyCode'");
    //                            oSql.AppendLine(",FDCrdExpireDate AS 'rdCrdExpireDate'");
    //                            oSql.AppendLine(",L.FTCrdName AS 'rtCrdName'");
    //                            oSql.AppendLine("FROM TFNMCard C");
    //                            oSql.AppendLine("LEFT JOIN TFNMCardType T ON (C.FTCtyCode=T.FTCtyCode)");
    //                            oSql.AppendLine("LEFT JOIN TFNMCard_L L ON (C.FTCrdCode=L.FTCrdCode)");
    //                            oSql.AppendLine("WHERE  C.FTCrdCode='" + poPara.ptCrdCode + "'");

    //                            nConTme = 0;
    //                            oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
    //                            nCmdTme = 0;
    //                            oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
    //                            oDatabase = new cDatabase(nConTme);
    //                            oResTopup = oDatabase.C_DAToSqlQuery<cmlResTnfIn>(oSql.ToString(), nCmdTme);
    //                            oResult.rtCode = oMsg.tMS_RespCode1;
    //                            oResult.rtDesc = oMsg.tMS_RespDesc1;
    //                            oResult.rcTxnValue = oResTopup.rcTxnValue;
    //                            oResult.rcCtyDeposit = oResTopup.rcCtyDeposit;
    //                            oResult.rcTxnValueAvb = oResTopup.rcTxnValueAvb;
    //                            oResult.rtCtyCode = oResTopup.rtCtyCode;
    //                            oResult.rtCrdName = oResTopup.rtCrdName;
    //                            oResult.rdCrdExpireDate = oResTopup.rdCrdExpireDate;
    //                            return oResult;
    //                        }
    //                        else
    //                        {
    //                            // Varify parameter value false.
    //                            oResult.rtCode = tErrCode;
    //                            oResult.rtDesc = tErrDesc;
    //                            return oResult;
    //                        }

    //                    }
    //                    else
    //                    {
    //                        // Key not allowed to use method.
    //                        oResult.rtCode = oMsg.tMS_RespCode904;
    //                        oResult.rtDesc = oMsg.tMS_RespDesc904;
    //                        return oResult;
    //                    }
    //                }
    //                else
    //                {
    //                    // This time not allowed to use method.
    //                    oResult.rtCode = oMsg.tMS_RespCode906;
    //                    oResult.rtDesc = oMsg.tMS_RespDesc906;
    //                    return oResult;
    //                }
    //            }
    //            else
    //            {
    //                // Validate parameter model false.
    //                oResult.rtCode = oMsg.tMS_RespCode701;
    //                oResult.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
    //                return oResult;
    //            }
    //        }
    //        catch (Exception oEx)
    //        {
    //            // Return error.
    //            oResult = new cmlResTnfIn();
    //            oResult.rtCode = new cMS().tMS_RespCode900;
    //            oResult.rtDesc = new cMS().tMS_RespDesc900;
    //            return oResult;
    //        }
    //        finally
    //        {
    //            oFunc = null;
    //            oCons = null;
    //            oMsg = null;
    //            oDatabase = null;
    //            oSql = null;
    //            aoSysConfig = null;
    //            oResTnfInErr = null;
    //            GC.Collect();
    //            GC.WaitForPendingFinalizers();
    //            GC.Collect();
    //            oTranfer = null;
    //            oResult = null;
    //            oResTopup = null;
    //            oComp = null;
    //        }
    //    }
    //}
}
