﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http;
using API2Wallet.Class;
using API2Wallet.Class.Standard;
using API2Wallet.Models;
using API2Wallet.Models.WebService.Request.CardHistory;
using API2Wallet.Models.WebService.Response.CardHistory;

namespace API2Wallet.Controllers
{
    [RoutePrefix(cCS.tCS_APIVer + "/CrdHistoryCheck")]
    public class cCheckCrdHistoryController : ApiController
    {
        [Route("CrdHistory")]
        [HttpPost]
        public cmlResCardHistory POST_PUNoCrdHistory([FromBody] cmlReqCardHistory poPrm)
        {
            cSP oFunc;
            cCS oCons;
            cMS oMsg;
            cDatabase oDatabase;
            StringBuilder oSql;
            List<cmlTSysConfig> aoSysConfig;
            cmlResCardHistory oResResult;

            int nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc;
            bool bVerifyPara;
            cmlTCNMComp oComp = new cmlTCNMComp();

            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
                oFunc = new cSP();
                oCons = new cCS();
                oMsg = new cMS();
                oResResult = new cmlResCardHistory();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                // Validate parameter.
                tModelErr = "";

                if (oFunc.SP_CHKbParaModel(ref tModelErr, ModelState))
                {
                    // Load configuration.
                    aoSysConfig = oFunc.SP_SYSaLoadConfiguration();

                    // Check range time use function.
                    if (oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {

                        tKeyApi = "";
                        // Check KeyApi.
                        if (oFunc.SP_CHKbKeyApi(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            oDatabase = new cDatabase();
                            oSql = new StringBuilder();

                            oSql.Clear();
                            oSql.AppendLine(" DECLARE @FNQty AS Integer");
                            oSql.AppendLine(" DECLARE @FTCrdCode AS VARCHAR(50) ");
                            oSql.AppendLine(" DECLARE @FTBchCode AS VARCHAR(50) ");
                            oSql.AppendLine(" SET @FNQty = '" + poPrm.pnQty + "' ");
                            oSql.AppendLine(" SET @FTCrdCode = '" + poPrm.ptCrdNo + "' ");
                            oSql.AppendLine(" SET @FTBchCode = '" + poPrm.ptBchCode + "' ");
                            oSql.AppendLine(" SELECT TOP(@FNQty)  FTTxnDocType AS ptType,FDTxnDocDate AS ptDocDate ");
                            oSql.AppendLine(" ,FTBchCodeRef AS ptBchRef,FTTxnDocNoRef AS ptDocRef ,FTTxnPosCode AS ptPos ");
                            oSql.AppendLine(" FROM ");
                            oSql.AppendLine(" (SELECT FTCrdCode,FTBchCode,FTTxnDocType,FDTxnDocDate,FTBchCodeRef,FTTxnDocNoRef,FTTxnPosCode");
                            oSql.AppendLine(" FROM TFNTCrdTopup ");
                            oSql.AppendLine(" UNION ");
                            oSql.AppendLine(" SELECT FTCrdCode,FTBchCode,FTTxnDocType,FDTxnDocDate,FTBchCodeRef,FTTxnDocNoRef,FTTxnPosCode ");
                            oSql.AppendLine(" FROM TFNTCrdSale) AS CrdHistory");
                            oSql.AppendLine(" WHERE FTCrdCode = @FTCrdCode AND FTBchCode = @FTBchCode ");

                            if (poPrm.ptSort == "0")
                            {
                                oSql.AppendLine("ORDER BY FDTxnDocDate ASC");

                            }
                            else
                            {
                                oSql.AppendLine("ORDER BY FDTxnDocDate DESC");
                            }

                            nConTme = 0;
                            oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                            nCmdTme = 0;
                            oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");

                            //Execute Add to list
                            oDatabase = new cDatabase(nConTme);
                            oResResult.roCrdHistory = oDatabase.C_DATaSqlQuery<cmlResCardHistoryList>(oSql.ToString());
                            oResResult.rtCode = oMsg.tMS_RespCode1;
                            oResResult.rtDesc = oMsg.tMS_RespDesc1;
                            return oResResult;
                        }
                        else
                        {
                            // Key not allowed to use method.
                            oResResult.rtCode = oMsg.tMS_RespCode904;
                            oResResult.rtDesc = oMsg.tMS_RespDesc904;
                            return oResResult;
                        }
                    }
                    else
                    {
                        // This time not allowed to use method.
                        oResResult.rtCode = oMsg.tMS_RespCode906;
                        oResResult.rtDesc = oMsg.tMS_RespDesc906;
                        return oResResult;
                    }
                }
                else
                {
                    // Validate parameter model false.
                    oResResult.rtCode = oMsg.tMS_RespCode701;
                    oResResult.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                    return oResResult;
                }
            }
            catch (Exception oEx)
            {
                oResResult = new cmlResCardHistory();
                oResResult.rtCode = new cMS().tMS_RespCode900;
                oResResult.rtDesc = new cMS().tMS_RespDesc900;
                return oResResult;
            }
            finally
            {
                oFunc = null;
                oCons = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
                aoSysConfig = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                oResResult = null;
                oComp = null;
            }

        }
    }
}
