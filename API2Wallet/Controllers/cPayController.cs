﻿using API2Wallet.Class;
using API2Wallet.Class.Pay;
using API2Wallet.Class.Standard;
using API2Wallet.Models;
using API2Wallet.Models.WebService.Request.Pay;
using API2Wallet.Models.WebService.Response.Pay;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http;

namespace API2Wallet.Controllers
{
    /// <summary>
    /// Pay information.
    /// </summary>
    [RoutePrefix(cCS.tCS_APIVer + "/Pay")]
    public class cPayController : ApiController
    {

        /// <summary>
        /// ตัดจ่าย//PayTxn
        /// </summary>
        /// <param name="poPara"></param>
        /// <returns>
        ///     System process status.<br/>
        ///     1   : success.<br/>
        ///     701 : validate parameter model false.
        ///     712 : Not found Card.
        ///     713 : Card Date expired.
        ///     714 : The amount of money in the card is not enough.".
        ///     802 : formate data incorrect..<br/>
        ///     900 : service process false.<br/>
        ///     904 : key not allowed to use method.<br/>
        ///     905 : cannot connect database.<br/>
        ///     906 : this time not allowed to use method.<br/>
        /// </returns>
        [Route("PayTxn")]
        [HttpPost]
        public cmlResPayTxn POST_PUNoInsPayTxn([FromBody] cmlReqPayTxn poPara)
        {
            cSP oFunc;
            cCS oCons;
            cMS oMsg;
            cDatabase oDatabase;
            StringBuilder oSql;
            cPay oPay;
            List<cmlTSysConfig> aoSysConfig;
            cmlResPayTxn oResPayErr;
            cmlResPayTxn oResult;
            cmlResPayTxn oResPay = new cmlResPayTxn(); 
            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc;
            bool bVerifyPara;
            cmlTCNMComp oComp = new cmlTCNMComp();
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

                oResult = new cmlResPayTxn();
                oFunc = new cSP();
                oCons = new cCS();
                oMsg = new cMS();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                // Validate parameter.
                tModelErr = "";
                if (oFunc.SP_CHKbParaModel(ref tModelErr, ModelState))
                {
                    // Load configuration.
                    aoSysConfig = oFunc.SP_SYSaLoadConfiguration();

                    // Check range time use function.
                    if (oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        // Check KeyApi.
                        if (oFunc.SP_CHKbKeyApi(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            // Varify parameter value.
                            oPay = new cPay();
                            bVerifyPara = oPay.C_DATbVerifyInsItemPaytxn(poPara, aoSysConfig, out tErrCode, out tErrDesc, out oResPayErr);
                            if (bVerifyPara == true)
                            {
                                nConTme = 0;
                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                                nCmdTme = 0;
                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                                oDatabase = new cDatabase(nConTme);
                                oSql = new StringBuilder();

                                // Get Company Code
                                oSql.Clear();
                                oSql.AppendLine("SELECT TOP 1 [FTCmpCode],[FTCmpTel],[FTCmpFax]");
                                oSql.AppendLine(",[FTBchcode],[FTCmpWhsInOrEx],[FTCmpRetInOrEx]");
                                oSql.AppendLine(",[FTCmpEmail],[FTRteCode],[FTVatCode]");
                                oSql.AppendLine(" FROM TCNMComp WITH (NOLOCK) ");
                                oComp = oDatabase.C_DAToSqlQuery<cmlTCNMComp>(oSql.ToString(), nCmdTme);

                                oSql.Clear();
                                oSql.AppendLine("BEGIN TRANSACTION ");
                                oSql.AppendLine("  SAVE TRANSACTION SavePay ");
                                oSql.AppendLine("  BEGIN TRY ");

                                // Insert transection Sale
                                oSql.AppendLine("     INSERT INTO TFNTCrdSale WITH(ROWLOCK)");
                                oSql.AppendLine("     (");
                                oSql.AppendLine("	  FTBchCode,FTCrdCode,FTTxnDocType,");
                                oSql.AppendLine("	  FTTxnPosCode,FTTxnDocNoRef,FCTxnValue,");
                                oSql.AppendLine("	  FTTxnstaPrc,FDTxnDocDate,FTBchCodeRef");
                                oSql.AppendLine("     )");
                                oSql.AppendLine("     VALUES");
                                oSql.AppendLine("     (");
                                oSql.AppendLine("	  '" + oComp.FTBchcode + "','" + poPara.ptCrdCode + "','3',");
                                oSql.AppendLine("	  '" + poPara.ptTxnPosCode + "','" + poPara.ptTxnDocNoRef + "','" + poPara.pcTxnValue + "',");
                                oSql.AppendLine("	  '1',GETDATE(),'" + poPara.ptBchCode + "'");
                                oSql.AppendLine("     )");

                                // Update Master Card
                                oSql.AppendLine("     UPDATE TFNMCard WITH (ROWLOCK) SET ");
                                oSql.AppendLine("     FCCrdValue=FCCrdValue- " + poPara.pcTxnValue + " ");
                                oSql.AppendLine("     WHERE FTCrdCode='" + poPara.ptCrdCode + "'");
                                oSql.AppendLine("     COMMIT TRANSACTION SavePay");
                                oSql.AppendLine("  END TRY");

                                oSql.AppendLine("  BEGIN CATCH");
                                oSql.AppendLine("   ROLLBACK TRANSACTION SavePay");
                                oSql.AppendLine("  END CATCH");

                                try
                                {
                                    // Confuguration database.
                                    nConTme = 0;
                                    oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                                    nCmdTme = 0;
                                    oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                                    oDatabase = new cDatabase(nConTme);
                                    nRowEff = oDatabase.C_DATnExecuteSql(oSql.ToString(), nCmdTme);
                                 
                                    if (nRowEff == 0) {
                                        oResult.rtCode = oMsg.tMS_RespCode900;
                                        oResult.rtDesc = oMsg.tMS_RespDesc900;
                                        return oResult;
                                    }
                                }
                                catch (EntityException oEtyExn)
                                {
                                    switch (oEtyExn.HResult)
                                    {
                                        case -2146232060:
                                            // Cannot connect database..
                                            oResult.rtCode = oMsg.tMS_RespCode905;
                                            oResult.rtDesc = oMsg.tMS_RespDesc905;
                                            return oResult;
                                    }
                                }

                                oSql = new StringBuilder();
                                oSql.Clear();
                                oSql.AppendLine("SELECT TOP 1 C.FCCrdValue AS 'rcTxnValue'");
                                oSql.AppendLine(",T.FCCtyDeposit AS 'rcCtyDeposit'");
                                oSql.AppendLine(",(C.FCCrdValue-T.FCCtyDeposit) AS 'rcTxnValueAvb'");
                                oSql.AppendLine(",C.FTCtyCode AS 'rtCtyCode'");
                                oSql.AppendLine(",FDCrdExpireDate AS 'rdCrdExpireDate'");
                                oSql.AppendLine(",L.FTCrdName AS 'rtCrdName'");
                                oSql.AppendLine("FROM TFNMCard C");
                                oSql.AppendLine("LEFT JOIN TFNMCardType T ON (C.FTCtyCode=T.FTCtyCode)");
                                oSql.AppendLine("LEFT JOIN TFNMCard_L L ON (C.FTCrdCode=L.FTCrdCode)");
                                oSql.AppendLine("WHERE  C.FTCrdCode='" + poPara.ptCrdCode + "'");

                                nConTme = 0;
                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                                nCmdTme = 0;
                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                                oDatabase = new cDatabase(nConTme);
                                oResPay = oDatabase.C_DAToSqlQuery<cmlResPayTxn>(oSql.ToString(), nCmdTme);
                                oResult.rtCode = oMsg.tMS_RespCode1;
                                oResult.rtDesc = oMsg.tMS_RespDesc1;
                                oResult.rcTxnValue = oResPay.rcTxnValue;
                                oResult.rcCtyDeposit = oResPay.rcCtyDeposit;
                                oResult.rcTxnValueAvb = oResPay.rcTxnValueAvb;
                                oResult.rtCtyCode = oResPay.rtCtyCode;
                                oResult.rtCrdName = oResPay.rtCrdName;
                                oResult.rdCrdExpireDate = oResPay.rdCrdExpireDate;
                                return oResult;
                            }
                            else
                            {
                                // Varify parameter value false.
                                oResult.rtCode = tErrCode;
                                oResult.rtDesc = tErrDesc;
                                return oResult;
                            }

                        }
                        else
                        {
                            // Key not allowed to use method.
                            oResult.rtCode = oMsg.tMS_RespCode904;
                            oResult.rtDesc = oMsg.tMS_RespDesc904;
                            return oResult;
                        }
                    }
                    else
                    {
                        // This time not allowed to use method.
                        oResult.rtCode = oMsg.tMS_RespCode906;
                        oResult.rtDesc = oMsg.tMS_RespDesc906;
                        return oResult;
                    }
                }
                else
                {
                    // Validate parameter model false.
                    oResult.rtCode = oMsg.tMS_RespCode701;
                    oResult.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                    return oResult;
                }
            }
            catch (Exception oEx)
            {
                // Return error.
                oResult = new cmlResPayTxn();
                oResult.rtCode = new cMS().tMS_RespCode900;
                oResult.rtDesc = new cMS().tMS_RespDesc900;
                return oResult;
            }
            finally
            {
                oFunc = null;
                oCons = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
                aoSysConfig = null;
                oResPayErr = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                oPay = null;              
                oResult = null;
                oResPay = null;
                oComp = null;
            }
        }

        /// <summary>
        /// ยกเลิกการตัดจ่าย//CancelPayTxn
        /// </summary>
        /// <param name="poPara"></param>
        /// <returns>
        ///     System process status.<br/>
        ///     1   : success.<br/>
        ///     701 : validate parameter model false.
        ///     712 : Not found Card.
        ///     713 : Card Date expired.
        ///     715 : Record is canceled.
        ///     802 : formate data incorrect..<br/>
        ///     900 : service process false.<br/>
        ///     904 : key not allowed to use method.<br/>
        ///     905 : cannot connect database.<br/>
        ///     906 : this time not allowed to use method.<br/>
        /// </returns>
        [Route("CancelPayTxn")]
        [HttpPost]
        public cmlRescancelPayTxn POST_PUNoInsCancelPayTxn([FromBody] cmlReqCancelpayTxn poPara)
        {
            cSP oFunc;
            cCS oCons;
            cMS oMsg;
            cDatabase oDatabase;
            StringBuilder oSql;
            cPay oPay;
            List<cmlTSysConfig> aoSysConfig;
            cmlRescancelPayTxn oResPayErr;
            cmlRescancelPayTxn oResult;
            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc;
            bool bVerifyPara;
            cmlTCNMComp oComp = new cmlTCNMComp();
            cmlTFNTCrdSale oCrdsale = new cmlTFNTCrdSale();
            cmlRescancelPayTxn oResCancelPay = new cmlRescancelPayTxn();

            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

                oResult = new cmlRescancelPayTxn();
                oFunc = new cSP();
                oCons = new cCS();
                oMsg = new cMS();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                // Validate parameter.
                tModelErr = "";
                if (oFunc.SP_CHKbParaModel(ref tModelErr, ModelState))
                {
                    // Load configuration.
                    aoSysConfig = oFunc.SP_SYSaLoadConfiguration();

                    // Check range time use function.
                    if (oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        // Check KeyApi.
                        if (oFunc.SP_CHKbKeyApi(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            // Varify parameter value.
                            oPay = new cPay();
                            bVerifyPara = oPay.C_DATbVerifyInsItemCancelPaytxn(poPara, aoSysConfig, out tErrCode, out tErrDesc, out oResPayErr);
                            if (bVerifyPara == true)
                            {

                                nConTme = 0;
                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                                nCmdTme = 0;
                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                                oDatabase = new cDatabase(nConTme);
                                oSql = new StringBuilder();

                                // Get Company Code
                                oSql.Clear();
                                oSql.AppendLine("SELECT TOP 1 [FTCmpCode],[FTCmpTel],[FTCmpFax]");
                                oSql.AppendLine(",[FTBchcode],[FTCmpWhsInOrEx],[FTCmpRetInOrEx]");
                                oSql.AppendLine(",[FTCmpEmail],[FTRteCode],[FTVatCode]");
                                oSql.AppendLine(" FROM TCNMComp WITH (NOLOCK) ");
                                oComp = oDatabase.C_DAToSqlQuery<cmlTCNMComp>(oSql.ToString(), nCmdTme);

                                // Get value Sale And Pos
                                oSql.Clear();
                                oSql.AppendLine("SELECT FCTxnvalue,FTTxnPosCode FROM TFNTCrdSale");
                                oSql.AppendLine("WHERE FNTxnID=" + poPara.pnTxnID +"");
                                oCrdsale = oDatabase.C_DAToSqlQuery<cmlTFNTCrdSale>(oSql.ToString(), nCmdTme);

                                oSql.Clear();
                                oSql.AppendLine("BEGIN TRANSACTION ");
                                oSql.AppendLine("  SAVE TRANSACTION SavePay ");
                                oSql.AppendLine("  BEGIN TRY ");

                                // Insert transection Sale
                                oSql.AppendLine("     INSERT INTO TFNTCrdSale WITH(ROWLOCK)");
                                oSql.AppendLine("     (");
                                oSql.AppendLine("	  FTBchCode,FTCrdCode,FTTxnDocType,");
                                oSql.AppendLine("	  FTTxnDocNoRef,FCTxnValue,FNTxnIDRef,");
                                oSql.AppendLine("	  FTTxnstaPrc,FDTxnDocDate,FTBchCodeRef,FTTxnPosCode,FTTxnStaCancel");
                                oSql.AppendLine("     )");
                                oSql.AppendLine("     VALUES");
                                oSql.AppendLine("     (");
                                oSql.AppendLine("	  '" + oComp.FTBchcode + "','" + poPara.ptCrdCode + "','4',");
                                oSql.AppendLine("	  '" + poPara.ptTxnDocNoRef + "','" + oCrdsale.FCTxnValue + "','" + poPara.pnTxnID +"',");
                                oSql.AppendLine("	  '1',GETDATE(),'" + poPara.ptBchCode + "','" + oCrdsale .FTTxnPosCode + "','1'");
                                oSql.AppendLine("     )");

                                // Update Master Card
                                oSql.AppendLine("     UPDATE TFNMCard WITH (ROWLOCK) SET ");
                                oSql.AppendLine("     FCCrdValue=(FCCrdValue + '" + oCrdsale.FCTxnValue + "')");
                                oSql.AppendLine("     WHERE FTCrdCode='" + poPara.ptCrdCode + "'");

                                // Update Ref Id Canceled
                                oSql.AppendLine("     UPDATE TFNTCrdSale WITH (ROWLOCK) SET ");
                                oSql.AppendLine("     FTTxnStaCancel='1'");
                                oSql.AppendLine("     WHERE FNTxnID=" + poPara.pnTxnID + "");

                                oSql.AppendLine("     COMMIT TRANSACTION SavePay");
                                oSql.AppendLine("  END TRY");

                                oSql.AppendLine("  BEGIN CATCH");
                                oSql.AppendLine("   ROLLBACK TRANSACTION SavePay");
                                oSql.AppendLine("  END CATCH");

                                try
                                {
                                    // Confuguration database.
                                    nConTme = 0;
                                    oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "003");
                                    nCmdTme = 0;
                                    oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "004");
                                    oDatabase = new cDatabase(nConTme);
                                    nRowEff = oDatabase.C_DATnExecuteSql(oSql.ToString(), nCmdTme);
                                    if (nRowEff == 0)
                                    {
                                        oResult.rtCode = oMsg.tMS_RespCode900;
                                        oResult.rtDesc = oMsg.tMS_RespDesc900;
                                        return oResult;
                                    }
                                    
                                }
                                catch (EntityException oEtyExn)
                                {
                                    switch (oEtyExn.HResult)
                                    {
                                        case -2146232060:
                                            // Cannot connect database..
                                            oResult.rtCode = oMsg.tMS_RespCode905;
                                            oResult.rtDesc = oMsg.tMS_RespDesc905;
                                            return oResult;
                                    }
                                }
                                oSql = new StringBuilder();
                                oSql.Clear();
                                oSql.AppendLine("SELECT TOP 1 C.FCCrdValue AS 'rcTxnValue'");
                                oSql.AppendLine(",T.FCCtyDeposit AS 'rcCtyDeposit'");
                                oSql.AppendLine(",(C.FCCrdValue-T.FCCtyDeposit) AS 'rcTxnValueAvb'");
                                oSql.AppendLine(",C.FTCtyCode AS 'rtCtyCode'");
                                oSql.AppendLine(",FDCrdExpireDate AS 'rdCrdExpireDate'");
                                oSql.AppendLine(",L.FTCrdName AS 'rtCrdName'");
                                oSql.AppendLine("FROM TFNMCard C");
                                oSql.AppendLine("LEFT JOIN TFNMCardType T ON (C.FTCtyCode=T.FTCtyCode)");
                                oSql.AppendLine("LEFT JOIN TFNMCard_L L ON (C.FTCrdCode=L.FTCrdCode)");
                                oSql.AppendLine("WHERE  C.FTCrdCode='" + poPara.ptCrdCode + "'");

                                nConTme = 0;
                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                                nCmdTme = 0;
                                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                                oDatabase = new cDatabase(nConTme);
                                oResCancelPay = oDatabase.C_DAToSqlQuery<cmlRescancelPayTxn>(oSql.ToString(), nCmdTme);
                                oResult.rtCode = oMsg.tMS_RespCode1;
                                oResult.rtDesc = oMsg.tMS_RespDesc1;
                                oResult.rcTxnValue = oResCancelPay.rcTxnValue;
                                oResult.rcCtyDeposit = oResCancelPay.rcCtyDeposit;
                                oResult.rcTxnValueAvb = oResCancelPay.rcTxnValueAvb;
                                oResult.rtCtyCode = oResCancelPay.rtCtyCode;
                                oResult.rtCrdName = oResCancelPay.rtCrdName;
                                oResult.rdCrdExpireDate = oResCancelPay.rdCrdExpireDate;
                                return oResult;
                            }
                            else
                            {
                                // Varify parameter value false.
                                oResult.rtCode = tErrCode;
                                oResult.rtDesc = tErrDesc;
                                return oResult;
                            }

                        }
                        else
                        {
                            // Key not allowed to use method.
                            oResult.rtCode = oMsg.tMS_RespCode904;
                            oResult.rtDesc = oMsg.tMS_RespDesc904;
                            return oResult;
                        }
                    }
                    else
                    {
                        // This time not allowed to use method.
                        oResult.rtCode = oMsg.tMS_RespCode906;
                        oResult.rtDesc = oMsg.tMS_RespDesc906;
                        return oResult;
                    }
                }
                else
                {
                    // Validate parameter model false.
                    oResult.rtCode = oMsg.tMS_RespCode701;
                    oResult.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                    return oResult;
                }
            }
            catch (Exception)
            {
                // Return error.
                oResult = new cmlRescancelPayTxn();
                oResult.rtCode = new cMS().tMS_RespCode900;
                oResult.rtDesc = new cMS().tMS_RespDesc900;
                return oResult;
            }
            finally
            {
                oFunc = null;
                oCons = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
                aoSysConfig = null;
                oResPayErr = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }
    }
}
