﻿using API2Wallet.Class.ResetExpired;
using API2Wallet.Class.Standard;
using API2Wallet.Models;
using API2Wallet.Models.WebService.Request.Tranfer;
using API2Wallet.Models.WebService.Response.Tranfer;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace API2Wallet.Class.Tranfer
{
    public class cTranfer
    {
        public bool C_DATbVerifyTnfOut(cmlReqTnfOut poPara, List<cmlTSysConfig> paoSysConfig,
           out string ptErrCode, out string ptErrDesc, out cmlResTnfOut poErrPay)
        {
            cSP oFunc = new cSP();
            cMS oMsg = new cMS();
            cDatabase oDatabase;
            StringBuilder oSql;
            int nConTme, nCmdTme;
            cmlTFNMCard oCard = new cmlTFNMCard();
            string tCrdExpireDate = "";
            string tExpireTopup = "";
            string tNow = "";
            DateTime dExpireTopup = DateTime.Now;
            cResetExpired oResetExpire = new cResetExpired();
       
            try
            {
                // Confuguration database.
                nConTme = 0;
                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, paoSysConfig, "002");
                nCmdTme = 0;
                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, paoSysConfig, "003");
                oDatabase = new cDatabase(nConTme);


                oSql = new StringBuilder();
                oSql.Clear();
                oSql.AppendLine("SELECT C.FTCrdCode,C.FDCrdExpireDate");
                oSql.AppendLine(",ISNULL(C.FCCrdValue,0) AS FCCrdValue");
                oSql.AppendLine(",ISNULL(T.FNCtyExpirePeriod,0) AS FNCtyExpirePeriod");
                oSql.AppendLine(",ISNULL(T.FNCtyExpiredType,0) AS FNCtyExpiredType,C.FDCrdLastTopup");
                oSql.AppendLine("FROM TFNMCard C WITH(NOLOCK) LEFT JOIN TFNMCardType T ON (C.FTCtyCode=T.FTCtyCode) ");
                oSql.AppendLine("WHERE FTCrdCode='" + poPara.ptCrdCode + "'");
                oCard = oDatabase.C_DAToSqlQuery<cmlTFNMCard>(oSql.ToString(), nCmdTme);

                // ตรวจสอบว่ามี รหัสบัตร หรือไม่
                if (oCard == null)
                {
                    poErrPay = new cmlResTnfOut();
                    ptErrCode = oMsg.tMS_RespCode712;
                    ptErrDesc = oMsg.tMS_RespDesc712;
                    return false;
                }
                else
                {
                    // ตรวจสอบวันหมดอายุรหัสบัตร
                    tNow = DateTime.Now.ToString(@"yyyy-MM-dd HH:mm:ss", new CultureInfo("en-US"));
                    tCrdExpireDate = oCard.FDCrdExpireDate.ToString("yyyy-MM-dd HH:mm:ss");
                    if (DateTime.Parse(tNow) > DateTime.Parse(tCrdExpireDate))
                    {
                        poErrPay = new cmlResTnfOut();
                        ptErrCode = oMsg.tMS_RespCode713;
                        ptErrDesc = oMsg.tMS_RespDesc713;
                        return false;
                    }

                    // ตรวจสอบอายุเงิน ถ้า FNCtyExpirePeriod = 0 ไม่ต้องตรวจสอบ
                    if (oCard.FNCtyExpirePeriod != 0)
                    {

                        // ตรวจสอบกรณี ยังไม่เคยเติมเงิน ไม่ต้องตรวจสอบวัน Expire ของยอดเงิน
                        if (oCard.FDCrdLastTopup != null)
                        {
                            switch (oCard.FNCtyExpiredType)
                            {
                                case 1:
                                    dExpireTopup = Convert.ToDateTime(oCard.FDCrdLastTopup).AddHours(Convert.ToInt32(oCard.FNCtyExpirePeriod));
                                    break;
                                case 2:
                                    dExpireTopup = Convert.ToDateTime(oCard.FDCrdLastTopup).AddDays(Convert.ToInt32(oCard.FNCtyExpirePeriod));
                                    break;
                                case 3:
                                    dExpireTopup = Convert.ToDateTime(oCard.FDCrdLastTopup).AddMonths(Convert.ToInt32(oCard.FNCtyExpirePeriod));
                                    break;
                                case 4:
                                    dExpireTopup = Convert.ToDateTime(oCard.FDCrdLastTopup).AddYears(Convert.ToInt32(oCard.FNCtyExpirePeriod));
                                    break;
                            }

                            tExpireTopup = dExpireTopup.ToString(@"yyyy-MM-dd HH:mm:ss", new CultureInfo("en-US"));
                            if (DateTime.Parse(tNow) > DateTime.Parse(tExpireTopup))
                            {
                                // ยอดเงินหมดอายุ ResetExpire
                                poErrPay = new cmlResTnfOut();
                                ptErrCode = oMsg.tMS_RespCode718;
                                ptErrDesc = oMsg.tMS_RespDesc718;
                                return false;
                            }
                        }

                    }

                    //ตรวจสอบยอดเงินคงเหลือ
                    if (Convert.ToDouble(poPara.pcTxnValue) > Convert.ToDouble(oCard.FCCrdValue)) {
                        poErrPay = new cmlResTnfOut();
                        ptErrCode = oMsg.tMS_RespCode719;
                        ptErrDesc = oMsg.tMS_RespDesc719;
                        return false;
                    }
                }

                ptErrCode = "";
                ptErrDesc = "";
                poErrPay = null;
                return true;
            }
            catch (Exception oEx)
            {
                poErrPay = new cmlResTnfOut();
                ptErrCode = oMsg.tMS_RespCode900;
                ptErrDesc = oMsg.tMS_RespDesc900;
                return false;
            }
            finally
            {
                oFunc = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        public bool C_DATbVerifyTnfIn(cmlReqTnfIn poPara, List<cmlTSysConfig> paoSysConfig,
         out string ptErrCode, out string ptErrDesc, out cmlResTnfIn poErrPay)
        {
            cSP oFunc = new cSP();
            cMS oMsg = new cMS();
            cDatabase oDatabase;
            StringBuilder oSql;
            int nConTme, nCmdTme;
            cmlTFNMCard oCard = new cmlTFNMCard();
            string tCrdExpireDate = "";
            string tExpireTopup = "";
            string tNow = "";
            DateTime dExpireTopup = DateTime.Now;
            cResetExpired oResetExpire = new cResetExpired();
            bool bResultReset;
            try
            {
                // Confuguration database.
                nConTme = 0;
                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, paoSysConfig, "002");
                nCmdTme = 0;
                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, paoSysConfig, "003");
                oDatabase = new cDatabase(nConTme);


                oSql = new StringBuilder();
                oSql.Clear();
                oSql.AppendLine("SELECT C.FTCrdCode,C.FDCrdExpireDate");
                oSql.AppendLine(",ISNULL(C.FCCrdValue,0) AS FCCrdValue");
                oSql.AppendLine(",ISNULL(T.FNCtyExpirePeriod,0) AS FNCtyExpirePeriod");
                oSql.AppendLine(",ISNULL(T.FNCtyExpiredType,0) AS FNCtyExpiredType,C.FDCrdLastTopup");
                oSql.AppendLine("FROM TFNMCard C WITH(NOLOCK) LEFT JOIN TFNMCardType T ON (C.FTCtyCode=T.FTCtyCode) ");
                oSql.AppendLine("WHERE FTCrdCode='" + poPara.ptCrdCode + "'");
                oCard = oDatabase.C_DAToSqlQuery<cmlTFNMCard>(oSql.ToString(), nCmdTme);

                // ตรวจสอบว่ามี รหัสบัตร หรือไม่
                if (oCard == null)
                {
                    poErrPay = new cmlResTnfIn();
                    ptErrCode = oMsg.tMS_RespCode712;
                    ptErrDesc = oMsg.tMS_RespDesc712;
                    return false;
                }
                else
                {
                    // ตรวจสอบวันหมดอายุรหัสบัตร
                    tNow = DateTime.Now.ToString(@"yyyy-MM-dd HH:mm:ss", new CultureInfo("en-US"));
                    tCrdExpireDate = oCard.FDCrdExpireDate.ToString("yyyy-MM-dd HH:mm:ss");
                    if (DateTime.Parse(tNow) > DateTime.Parse(tCrdExpireDate))
                    {
                        poErrPay = new cmlResTnfIn();
                        ptErrCode = oMsg.tMS_RespCode713;
                        ptErrDesc = oMsg.tMS_RespDesc713;
                        return false;
                    }

                    // ตรวจสอบอายุเงิน ถ้า FNCtyExpirePeriod = 0 ไม่ต้องตรวจสอบ
                    if (oCard.FNCtyExpirePeriod != 0)
                    {

                        // ตรวจสอบกรณี ยังไม่เคยเติมเงิน ไม่ต้องตรวจสอบวัน Expire ของยอดเงิน
                        if (oCard.FDCrdLastTopup != null)
                        {
                            switch (oCard.FNCtyExpiredType)
                            {
                                case 1:
                                    dExpireTopup = Convert.ToDateTime(oCard.FDCrdLastTopup).AddHours(Convert.ToInt32(oCard.FNCtyExpirePeriod));
                                    break;
                                case 2:
                                    dExpireTopup = Convert.ToDateTime(oCard.FDCrdLastTopup).AddDays(Convert.ToInt32(oCard.FNCtyExpirePeriod));
                                    break;
                                case 3:
                                    dExpireTopup = Convert.ToDateTime(oCard.FDCrdLastTopup).AddMonths(Convert.ToInt32(oCard.FNCtyExpirePeriod));
                                    break;
                                case 4:
                                    dExpireTopup = Convert.ToDateTime(oCard.FDCrdLastTopup).AddYears(Convert.ToInt32(oCard.FNCtyExpirePeriod));
                                    break;
                            }

                            tExpireTopup = dExpireTopup.ToString(@"yyyy-MM-dd HH:mm:ss", new CultureInfo("en-US"));
                            if (DateTime.Parse(tNow) > DateTime.Parse(tExpireTopup))
                            {
                                // ยอดเงินหมดอายุ ResetExpire
                                bResultReset = oResetExpire.C_SETbResetExpired(poPara.ptCrdCode, poPara.ptBchCode, paoSysConfig);
                                if (bResultReset == false)
                                {
                                    poErrPay = new cmlResTnfIn();
                                    ptErrCode = oMsg.tMS_RespCode716;
                                    ptErrDesc = oMsg.tMS_RespDesc716;
                                    return false;
                                }
                            }
                        }

                    }
                }

                ptErrCode = "";
                ptErrDesc = "";
                poErrPay = null;
                return true;
            }
            catch (Exception oEx)
            {
                poErrPay = new cmlResTnfIn();
                ptErrCode = oMsg.tMS_RespCode900;
                ptErrDesc = oMsg.tMS_RespDesc900;
                return false;
            }
            finally
            {
                oFunc = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        public cmlResTnfOut C_PUNoTnfOut([FromBody] cmlReqTnfOut poPara)
        {
            cSP oFunc;
            cCS oCons;
            cMS oMsg;
            cDatabase oDatabase;
            StringBuilder oSql;
            List<cmlTSysConfig> aoSysConfig;
            cmlResTnfOut oResTnfOutErr;
            cmlResTnfOut oResult;
            cmlResTnfOut oResTnfOut = new cmlResTnfOut();
            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tErrCode, tErrDesc;
            bool bVerifyPara;
            cmlTCNMComp oComp = new cmlTCNMComp();
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

                oResult = new cmlResTnfOut();
                oFunc = new cSP();
                oCons = new cCS();
                oMsg = new cMS();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                // Load configuration.
                aoSysConfig = oFunc.SP_SYSaLoadConfiguration();

                // Check range time use function.
                if (oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                {
                    // Varify parameter value.
                    bVerifyPara = C_DATbVerifyTnfOut(poPara, aoSysConfig, out tErrCode, out tErrDesc, out oResTnfOutErr);
                    if (bVerifyPara == true)
                    {
                        nConTme = 0;
                        oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                        nCmdTme = 0;
                        oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                        oDatabase = new cDatabase(nConTme);
                        oSql = new StringBuilder();

                        // Get Company Code
                        oSql.Clear();
                        oSql.AppendLine("SELECT TOP 1 [FTCmpCode],[FTCmpTel],[FTCmpFax]");
                        oSql.AppendLine(",[FTBchcode],[FTCmpWhsInOrEx],[FTCmpRetInOrEx]");
                        oSql.AppendLine(",[FTCmpEmail],[FTRteCode],[FTVatCode]");
                        oSql.AppendLine(" FROM TCNMComp WITH (NOLOCK) ");
                        oComp = oDatabase.C_DAToSqlQuery<cmlTCNMComp>(oSql.ToString(), nCmdTme);

                        oSql.Clear();
                        oSql.AppendLine("BEGIN TRANSACTION ");
                        oSql.AppendLine("  SAVE TRANSACTION TnfOut ");
                        oSql.AppendLine("  BEGIN TRY ");

                        // Insert transection Sale
                        oSql.AppendLine("     INSERT INTO TFNTCrdTopUp WITH(ROWLOCK)");
                        oSql.AppendLine("     (");
                        oSql.AppendLine("	  FTBchCode,FTTxnDocType,FTCrdCode,");
                        oSql.AppendLine("	  FDTxnDocDate,FTBchCodeRef,FCTxnValue,");
                        oSql.AppendLine("	  FTTxnStaPrc");
                        oSql.AppendLine("     )");
                        oSql.AppendLine("     VALUES");
                        oSql.AppendLine("     (");
                        oSql.AppendLine("	  '" + oComp.FTBchcode + "','8','" + poPara.ptCrdCode + "',");
                        oSql.AppendLine("	  GETDATE(),'" + poPara.ptBchCode + "','" + poPara.pcTxnValue + "',");
                        oSql.AppendLine("	  '1'");
                        oSql.AppendLine("     )");

                        // Update Master Card
                        oSql.AppendLine("     UPDATE TFNMCard WITH (ROWLOCK) SET ");
                        oSql.AppendLine("     FCCrdValue=(ISNULL(FCCrdValue,0) - " + poPara.pcTxnValue + ")");
                        //oSql.AppendLine("     ,FDCrdLastTopup=GETDATE() ");
                        oSql.AppendLine("     WHERE FTCrdCode='" + poPara.ptCrdCode + "'");
                        oSql.AppendLine("     COMMIT TRANSACTION TnfOut");
                        oSql.AppendLine("  END TRY");

                        oSql.AppendLine("  BEGIN CATCH");
                        oSql.AppendLine("   ROLLBACK TRANSACTION TnfOut");
                        oSql.AppendLine("  END CATCH");

                        try
                        {
                            // Confuguration database.
                            nConTme = 0;
                            oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                            nCmdTme = 0;
                            oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                            oDatabase = new cDatabase(nConTme);
                            nRowEff = oDatabase.C_DATnExecuteSql(oSql.ToString(), nCmdTme);

                            if (nRowEff == 0)
                            {
                                oResult.rtCode = oMsg.tMS_RespCode900;
                                oResult.rtDesc = oMsg.tMS_RespDesc900;
                                return oResult;
                            }
                        }
                        catch (EntityException oEtyExn)
                        {
                            switch (oEtyExn.HResult)
                            {
                                case -2146232060:
                                    // Cannot connect database..
                                    oResult.rtCode = oMsg.tMS_RespCode905;
                                    oResult.rtDesc = oMsg.tMS_RespDesc905;
                                    return oResult;
                            }
                        }
                        oResult.rtCode = oMsg.tMS_RespCode1;
                        oResult.rtDesc = oMsg.tMS_RespDesc1;
                        return oResult;
                    }
                    else
                    {
                        // Varify parameter value false.
                        oResult.rtCode = tErrCode;
                        oResult.rtDesc = tErrDesc;
                        return oResult;
                    }
                }
                else
                {
                    // This time not allowed to use method.
                    oResult.rtCode = oMsg.tMS_RespCode906;
                    oResult.rtDesc = oMsg.tMS_RespDesc906;
                    return oResult;
                }
            }
            catch (Exception oEx)
            {
                // Return error.
                oResult = new cmlResTnfOut();
                oResult.rtCode = new cMS().tMS_RespCode900;
                oResult.rtDesc = new cMS().tMS_RespDesc900;
                return oResult;
            }
            finally
            {
                oFunc = null;
                oCons = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
                aoSysConfig = null;
                oResTnfOutErr = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                oResult = null;
                oResTnfOut = null;
                oComp = null;
            }
        }

        public cmlResTnfIn C_PUNoTnfIn([FromBody] cmlReqTnfIn poPara)
        {
            cSP oFunc;
            cCS oCons;
            cMS oMsg;
            cDatabase oDatabase;
            StringBuilder oSql;
            List<cmlTSysConfig> aoSysConfig;
            cmlResTnfIn oResTnfInErr;
            cmlResTnfIn oResult;
            cmlResTnfIn oResTopup = new cmlResTnfIn();
            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tErrCode, tErrDesc;
            bool bVerifyPara;
            cmlTCNMComp oComp = new cmlTCNMComp();
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

                oResult = new cmlResTnfIn();
                oFunc = new cSP();
                oCons = new cCS();
                oMsg = new cMS();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                // Load configuration.
                aoSysConfig = oFunc.SP_SYSaLoadConfiguration();

                // Check range time use function.
                if (oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                {
                       
                    bVerifyPara = C_DATbVerifyTnfIn(poPara, aoSysConfig, out tErrCode, out tErrDesc, out oResTnfInErr);
                    if (bVerifyPara == true)
                    {
                        nConTme = 0;
                        oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                        nCmdTme = 0;
                        oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                        oDatabase = new cDatabase(nConTme);
                        oSql = new StringBuilder();

                        // Get Company Code
                        oSql.Clear();
                        oSql.AppendLine("SELECT TOP 1 [FTCmpCode],[FTCmpTel],[FTCmpFax]");
                        oSql.AppendLine(",[FTBchcode],[FTCmpWhsInOrEx],[FTCmpRetInOrEx]");
                        oSql.AppendLine(",[FTCmpEmail],[FTRteCode],[FTVatCode]");
                        oSql.AppendLine(" FROM TCNMComp WITH (NOLOCK) ");
                        oComp = oDatabase.C_DAToSqlQuery<cmlTCNMComp>(oSql.ToString(), nCmdTme);

                        oSql.Clear();
                        oSql.AppendLine("BEGIN TRANSACTION ");
                        oSql.AppendLine("  SAVE TRANSACTION TnfOut ");
                        oSql.AppendLine("  BEGIN TRY ");

                        // Insert transection Sale
                        oSql.AppendLine("     INSERT INTO TFNTCrdTopUp WITH(ROWLOCK)");
                        oSql.AppendLine("     (");
                        oSql.AppendLine("	  FTBchCode,FTTxnDocType,FTCrdCode,");
                        oSql.AppendLine("	  FDTxnDocDate,FTBchCodeRef,FCTxnValue,");
                        oSql.AppendLine("	  FTTxnStaPrc");
                        oSql.AppendLine("     )");
                        oSql.AppendLine("     VALUES");
                        oSql.AppendLine("     (");
                        oSql.AppendLine("	  '" + oComp.FTBchcode + "','9','" + poPara.ptCrdCode + "',");
                        oSql.AppendLine("	  GETDATE(),'" + poPara.ptBchCode + "','" + poPara.pcTxnValue + "',");
                        oSql.AppendLine("	  '1'");
                        oSql.AppendLine("     )");

                        // Update Master Card
                        oSql.AppendLine("     UPDATE TFNMCard WITH (ROWLOCK) SET ");
                        oSql.AppendLine("     FCCrdValue=(ISNULL(FCCrdValue,0) + " + poPara.pcTxnValue + ")");
                        oSql.AppendLine("     ,FDCrdLastTopup=GETDATE() ");
                        oSql.AppendLine("     WHERE FTCrdCode='" + poPara.ptCrdCode + "'");
                        oSql.AppendLine("     COMMIT TRANSACTION TnfOut");
                        oSql.AppendLine("  END TRY");

                        oSql.AppendLine("  BEGIN CATCH");
                        oSql.AppendLine("   ROLLBACK TRANSACTION TnfOut");
                        oSql.AppendLine("  END CATCH");

                        try
                        {
                            // Confuguration database.
                            nConTme = 0;
                            oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "002");
                            nCmdTme = 0;
                            oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "003");
                            oDatabase = new cDatabase(nConTme);
                            nRowEff = oDatabase.C_DATnExecuteSql(oSql.ToString(), nCmdTme);

                            if (nRowEff == 0)
                            {
                                oResult.rtCode = oMsg.tMS_RespCode900;
                                oResult.rtDesc = oMsg.tMS_RespDesc900;
                                return oResult;
                            }
                        }
                        catch (EntityException oEtyExn)
                        {
                            switch (oEtyExn.HResult)
                            {
                                case -2146232060:
                                    // Cannot connect database..
                                    oResult.rtCode = oMsg.tMS_RespCode905;
                                    oResult.rtDesc = oMsg.tMS_RespDesc905;
                                    return oResult;
                            }
                        }

                        oResult.rtCode = oMsg.tMS_RespCode1;
                        oResult.rtDesc = oMsg.tMS_RespDesc1;
                        return oResult;
                    }
                    else
                    {
                        // Varify parameter value false.
                        oResult.rtCode = tErrCode;
                        oResult.rtDesc = tErrDesc;
                        return oResult;
                    }
                }
                else
                {
                    // This time not allowed to use method.
                    oResult.rtCode = oMsg.tMS_RespCode906;
                    oResult.rtDesc = oMsg.tMS_RespDesc906;
                    return oResult;
                }
               
            }
            catch (Exception oEx)
            {
                // Return error.
                oResult = new cmlResTnfIn();
                oResult.rtCode = new cMS().tMS_RespCode900;
                oResult.rtDesc = new cMS().tMS_RespDesc900;
                return oResult;
            }
            finally
            {
                oFunc = null;
                oCons = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
                aoSysConfig = null;
                oResTnfInErr = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                oResult = null;
                oResTopup = null;
                oComp = null;
            }
        }
    }
}