﻿using API2Wallet.Class.ResetExpired;
using API2Wallet.Class.Standard;
using API2Wallet.Class.Tranfer;
using API2Wallet.Models;
using API2Wallet.Models.WebService.Request.Card;
using API2Wallet.Models.WebService.Request.ChangeCard;
using API2Wallet.Models.WebService.Request.Tranfer;
using API2Wallet.Models.WebService.Response.Card;
using API2Wallet.Models.WebService.Response.ChangeCard;
using API2Wallet.Models.WebService.Response.Tranfer;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace API2Wallet.Class.Log
{
    public class cChangeCard
    {

        public bool C_DATbVerifyChangeCard(cmlReqChangeCard poPara, List<cmlTSysConfig> paoSysConfig,
           out string ptErrCode, out string ptErrDesc, out cmlResChangeCard poErrChangeCard)
        {
            cSP oFunc = new cSP();
            cMS oMsg = new cMS();
            cDatabase oDatabase;
            StringBuilder oSql;
            int nConTme, nCmdTme;
            cmlTFNMCard oCardOut = new cmlTFNMCard();
            cmlTFNMCard oCardIn = new cmlTFNMCard();
            DateTime dExpireTopup = DateTime.Now;
            cResetExpired oResetExpire = new cResetExpired();
            cmlReqTnfOut oReqTnfOut = new cmlReqTnfOut();
            cmlResTnfOut oResTnfOut = new cmlResTnfOut();
            cmlReqTnfIn oReqTnfIn = new cmlReqTnfIn();
            cmlResTnfIn oResTnfIn = new cmlResTnfIn();
            cTranfer oTranfer = new cTranfer();
            string tNow;
            string tFrmCrdExp;
            string tFrmCrdTopupExp;
            bool bResetFrmCrdTopup = true;
            string tToCrdExp;
            string tToCrdTopupExp;
            bool bResetToCrdTopup = true;

            try
            {
                ////===================== Confuguration database.=========================================///
                nConTme = 0;
                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, paoSysConfig, "002");
                nCmdTme = 0;
                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, paoSysConfig, "003");
                oDatabase = new cDatabase(nConTme);

                oSql = new StringBuilder();
                oSql.Clear();
                oSql.AppendLine("SELECT C.FTCrdCode,C.FDCrdExpireDate");
                oSql.AppendLine(",ISNULL(C.FCCrdValue,0.0000) AS FCCrdValue");
                oSql.AppendLine(",ISNULL(T.FNCtyExpirePeriod,0) AS FNCtyExpirePeriod");
                oSql.AppendLine(",ISNULL(T.FNCtyExpiredType,0) AS FNCtyExpiredType,C.FDCrdLastTopup ");
                oSql.AppendLine("FROM TFNMCard C WITH(NOLOCK) LEFT JOIN TFNMCardType T ON (C.FTCtyCode=T.FTCtyCode) ");
                oSql.AppendLine("WHERE FTCrdCode='" + poPara.ptFrmCrdCode + "'");
                oCardOut = oDatabase.C_DAToSqlQuery<cmlTFNMCard>(oSql.ToString(), nCmdTme);

                oSql = new StringBuilder();
                oSql.Clear();
                oSql.AppendLine("SELECT C.FTCrdCode,C.FDCrdExpireDate");
                oSql.AppendLine(",ISNULL(C.FCCrdValue,0.0000) AS FCCrdValue");
                oSql.AppendLine(",ISNULL(T.FNCtyExpirePeriod,0) AS FNCtyExpirePeriod");
                oSql.AppendLine(",ISNULL(T.FNCtyExpiredType,0) AS FNCtyExpiredType,C.FDCrdLastTopup,FTCrdStaLocate ");
                oSql.AppendLine("FROM TFNMCard C WITH(NOLOCK) LEFT JOIN TFNMCardType T ON (C.FTCtyCode=T.FTCtyCode) ");
                oSql.AppendLine("WHERE FTCrdCode='" + poPara.ptToCrdCode + "'");
                oCardIn = oDatabase.C_DAToSqlQuery<cmlTFNMCard>(oSql.ToString(), nCmdTme);

                // ตรวจสอบวันหมดอายุรหัสบัตร จากรหัสบัตร
                tNow = DateTime.Now.ToString(@"yyyy-MM-dd HH:mm:ss", new CultureInfo("en-US"));
                tFrmCrdExp = oCardOut.FDCrdExpireDate.ToString("yyyy-MM-dd HH:mm:ss");
                if (DateTime.Parse(tNow) > DateTime.Parse(tFrmCrdExp))
                {
                    poErrChangeCard = new cmlResChangeCard();
                    ptErrCode = oMsg.tMS_RespCode713 + " (ptFrmCrdCode)";
                    ptErrDesc = oMsg.tMS_RespDesc713;
                    return false;
                }

                // ตรวจสอบวันหมดอายุรหัสบัตร จากรหัสบัตร
                tNow = DateTime.Now.ToString(@"yyyy-MM-dd HH:mm:ss", new CultureInfo("en-US"));
                tToCrdExp = oCardIn.FDCrdExpireDate.ToString("yyyy-MM-dd HH:mm:ss");
                if (DateTime.Parse(tNow) > DateTime.Parse(tToCrdExp))
                {
                    poErrChangeCard = new cmlResChangeCard();
                    ptErrCode = oMsg.tMS_RespCode713 + " (ptToCrdCode)";
                    ptErrDesc = oMsg.tMS_RespDesc713;
                    return false;
                }

                // ตรวจสอบบัตรที่รับโอน เบิกใช้งานแล้วหรือยัง
                if (oCardIn.FTCrdStaLocate != "1") {
                    poErrChangeCard = new cmlResChangeCard();
                    ptErrCode = oMsg.tMS_RespCode720 + " (ptToCrdCode)";
                    ptErrDesc = oMsg.tMS_RespDesc720;
                    return false;
                }

                // ตรวจสอบอายุเงิน ตรวจสอบอายุเงิน บัตร Form
                // ตรวจสอบอายุเงิน ถ้า FNCtyExpirePeriod = 0 ไม่ต้องตรวจสอบ
                if (oCardOut.FNCtyExpirePeriod != 0)
                {

                    // ตรวจสอบกรณี ยังไม่เคยเติมเงิน ไม่ต้องตรวจสอบวัน Expire ของยอดเงิน
                    if (oCardOut.FDCrdLastTopup != null)
                    {
                        switch (oCardOut.FNCtyExpiredType)
                        {
                            case 1:
                                dExpireTopup = Convert.ToDateTime(oCardOut.FDCrdLastTopup).AddHours(Convert.ToInt32(oCardOut.FNCtyExpirePeriod));
                                break;
                            case 2:
                                dExpireTopup = Convert.ToDateTime(oCardOut.FDCrdLastTopup).AddDays(Convert.ToInt32(oCardOut.FNCtyExpirePeriod));
                                break;
                            case 3:
                                dExpireTopup = Convert.ToDateTime(oCardOut.FDCrdLastTopup).AddMonths(Convert.ToInt32(oCardOut.FNCtyExpirePeriod));
                                break;
                            case 4:
                                dExpireTopup = Convert.ToDateTime(oCardOut.FDCrdLastTopup).AddYears(Convert.ToInt32(oCardOut.FNCtyExpirePeriod));
                                break;
                        }

                        tFrmCrdTopupExp = dExpireTopup.ToString(@"yyyy-MM-dd HH:mm:ss", new CultureInfo("en-US"));
                        if (DateTime.Parse(tNow) > DateTime.Parse(tFrmCrdTopupExp))
                        {
                            // ยอดเงินหมดอายุ ResetExpire
                            bResetFrmCrdTopup = oResetExpire.C_SETbResetExpired(poPara.ptFrmCrdCode, poPara.ptBchCode, paoSysConfig);
                            if (bResetFrmCrdTopup == false)
                            {
                                poErrChangeCard = new cmlResChangeCard();
                                ptErrCode = oMsg.tMS_RespCode716 + " (ptFrmCrdCode)";
                                ptErrDesc = oMsg.tMS_RespDesc716;
                                return false;
                            }
                        }
                    }
                }

                // ตรวจสอบอายุเงิน ตรวจสอบอายุเงิน บัตร To
                // ตรวจสอบอายุเงิน ถ้า FNCtyExpirePeriod = 0 ไม่ต้องตรวจสอบ
                if (oCardIn.FNCtyExpirePeriod != 0)
                {
                    // ตรวจสอบกรณี ยังไม่เคยเติมเงิน ไม่ต้องตรวจสอบวัน Expire ของยอดเงิน
                    if (oCardIn.FDCrdLastTopup != null)
                    {
                        switch (oCardIn.FNCtyExpiredType)
                        {
                            case 1:
                                dExpireTopup = Convert.ToDateTime(oCardOut.FDCrdLastTopup).AddHours(Convert.ToInt32(oCardIn.FNCtyExpirePeriod));
                                break;
                            case 2:
                                dExpireTopup = Convert.ToDateTime(oCardOut.FDCrdLastTopup).AddDays(Convert.ToInt32(oCardIn.FNCtyExpirePeriod));
                                break;
                            case 3:
                                dExpireTopup = Convert.ToDateTime(oCardOut.FDCrdLastTopup).AddMonths(Convert.ToInt32(oCardIn.FNCtyExpirePeriod));
                                break;
                            case 4:
                                dExpireTopup = Convert.ToDateTime(oCardOut.FDCrdLastTopup).AddYears(Convert.ToInt32(oCardIn.FNCtyExpirePeriod));
                                break;
                        }

                        tToCrdTopupExp = dExpireTopup.ToString(@"yyyy-MM-dd HH:mm:ss", new CultureInfo("en-US"));
                        if (DateTime.Parse(tNow) > DateTime.Parse(tToCrdTopupExp))
                        {
                            // ยอดเงินหมดอายุ ResetExpire
                            bResetToCrdTopup = oResetExpire.C_SETbResetExpired(poPara.ptToCrdCode, poPara.ptBchCode, paoSysConfig);
                            if (bResetToCrdTopup == false)
                            {
                                poErrChangeCard = new cmlResChangeCard();
                                ptErrCode = oMsg.tMS_RespCode716 + " (ptToCrdCode)";
                                ptErrDesc = oMsg.tMS_RespDesc716;
                                return false;
                            }
                        }
                    }
                }

                ////===================== Confuguration database.=========================================///

                //================================ Tnf Out ===============================================///
                oReqTnfOut.ptBchCode = poPara.ptBchCode;
                oReqTnfOut.ptCrdCode = poPara.ptFrmCrdCode;
                oReqTnfOut.pcTxnValue = oCardOut.FCCrdValue;
                oReqTnfOut.ptRemark = "";

                oResTnfOut = oTranfer.C_PUNoTnfOut(oReqTnfOut);
                if (oResTnfOut.rtCode != "1") {
                    poErrChangeCard = new cmlResChangeCard();
                    ptErrCode = oResTnfOut.rtCode + " (ptFrmCrdCode)";
                    ptErrDesc = oResTnfOut.rtDesc;
                    return false;
                }
                //================================ Tnf Out ===========================================///

                //================================ Tnf IN ============================================///
                oReqTnfIn.ptBchCode = poPara.ptBchCode;
                oReqTnfIn.ptCrdCode = poPara.ptToCrdCode;
                oReqTnfIn.pcTxnValue = oCardOut.FCCrdValue;
                oReqTnfIn.ptRemark = "";

                oResTnfIn = oTranfer.C_PUNoTnfIn(oReqTnfIn);
                if (oResTnfIn.rtCode != "1")
                {
                    poErrChangeCard = new cmlResChangeCard();
                    ptErrCode = oResTnfOut.rtCode + " (ptToCrdCode)";
                    ptErrDesc = oResTnfOut.rtDesc;
                    return false;
                }
                //================================ Tnf IN ============================================///

                ptErrCode = "";
                ptErrDesc = "";
                poErrChangeCard = null;
                return true;
            }
            catch (Exception oEx)
            {
                poErrChangeCard = new cmlResChangeCard();
                ptErrCode = oMsg.tMS_RespCode900;
                ptErrDesc = oMsg.tMS_RespDesc900;
                return false;
            }
            finally
            {
                oFunc = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        public bool C_DATbVerifyCancelCard(cmlReqCancelCard poPara, List<cmlTSysConfig> paoSysConfig,
           out string ptErrCode, out string ptErrDesc, out cmlResCancelCard poErrCancelCard)
        {
            cSP oFunc = new cSP();
            cMS oMsg = new cMS();
            cDatabase oDatabase;
            StringBuilder oSql;
            int nConTme, nCmdTme;
            cmlTFNMCard oCard = new cmlTFNMCard();
            cmlTFNMCard oCardIn = new cmlTFNMCard();
            DateTime dExpireTopup = DateTime.Now;
            cResetExpired oResetExpire = new cResetExpired();
            cTranfer oTranfer = new cTranfer();
            bool bResetExpired = true;


            try
            {
                //////===================== Confuguration database.=========================================///
                nConTme = 0;
                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, paoSysConfig, "002");
                nCmdTme = 0;
                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, paoSysConfig, "003");
                oDatabase = new cDatabase(nConTme);

                oSql = new StringBuilder();
                oSql.Clear();
                oSql.AppendLine("SELECT C.FTCrdCode,C.FDCrdExpireDate");
                oSql.AppendLine(",ISNULL(C.FCCrdValue,0.0000) AS FCCrdValue");
                oSql.AppendLine(",ISNULL(T.FNCtyExpirePeriod,0) AS FNCtyExpirePeriod");
                oSql.AppendLine(",ISNULL(T.FNCtyExpiredType,0) AS FNCtyExpiredType,C.FDCrdLastTopup ");
                oSql.AppendLine("FROM TFNMCard C WITH(NOLOCK) LEFT JOIN TFNMCardType T ON (C.FTCtyCode=T.FTCtyCode) ");
                oSql.AppendLine("WHERE FTCrdCode='" + poPara.ptCrdCode + "'");
                oCard = oDatabase.C_DAToSqlQuery<cmlTFNMCard>(oSql.ToString(), nCmdTme);

                // ตรวจสอบว่ามี รหัสบัตร หรือไม่
                if (oCard == null)
                {
                    poErrCancelCard = new cmlResCancelCard();
                    ptErrCode = oMsg.tMS_RespCode712;
                    ptErrDesc = oMsg.tMS_RespDesc712;
                    return false;
                }
                else
                {
                    if (oCard.FCCrdValue > 0)
                    {
                        //ถ้าบัตรมียอดเงินมากกว่า 0 
                        bResetExpired = oResetExpire.C_SETbResetExpired(poPara.ptCrdCode, poPara.ptBchCode, paoSysConfig);
                        if (bResetExpired == false)
                        {
                            poErrCancelCard = new cmlResCancelCard();
                            ptErrCode = oMsg.tMS_RespCode716;
                            ptErrDesc = oMsg.tMS_RespDesc716;
                            return false;
                        }
                    }
                }

                ptErrCode = "";
                ptErrDesc = "";
                poErrCancelCard = null;
                return true;
            }
            catch (Exception oEx)
            {
                poErrCancelCard = new cmlResCancelCard();
                ptErrCode = oMsg.tMS_RespCode900;
                ptErrDesc = oMsg.tMS_RespDesc900;
                return false;
            }
            finally
            {
                oFunc = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }
    }
}