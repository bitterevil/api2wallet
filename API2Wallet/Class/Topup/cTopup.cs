﻿using API2Wallet.Class.ResetExpired;
using API2Wallet.Class.Standard;
using API2Wallet.Models;
using API2Wallet.Models.WebService.Request.Topup;
using API2Wallet.Models.WebService.Response.Topup;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace API2Wallet.Class.Topup
{
    public class cTopup
    {

        public bool C_DATbVerifyTopup(cmlReqTopup poPara, List<cmlTSysConfig> paoSysConfig,
           out string ptErrCode, out string ptErrDesc, out cmlResTopup poErrPay)
        {
            cSP oFunc = new cSP();
            cMS oMsg = new cMS();
            cDatabase oDatabase;
            StringBuilder oSql;
            int nConTme, nCmdTme;
            cmlTFNMCard oCard = new cmlTFNMCard();
            string tCrdExpireDate = "";
            string tExpireTopup = "";
            string tNow = "";
            DateTime dExpireTopup = DateTime.Now;
            cResetExpired oResetExpire = new cResetExpired();
            bool bResultReset;
            try
            {
                // Confuguration database.
                nConTme = 0;
                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, paoSysConfig, "002");
                nCmdTme = 0;
                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, paoSysConfig, "003");
                oDatabase = new cDatabase(nConTme);


                oSql = new StringBuilder();
                oSql.Clear();
                oSql.AppendLine("SELECT C.FTCrdCode,C.FDCrdExpireDate");
                oSql.AppendLine(",ISNULL(C.FCCrdValue-T.FCCtyDeposit,0.0000) AS FCCrdValue");
                oSql.AppendLine(",ISNULL(T.FNCtyExpirePeriod,0) AS FNCtyExpirePeriod");
                oSql.AppendLine(",ISNULL(T.FNCtyExpiredType,0) AS FNCtyExpiredType,C.FDCrdLastTopup");
                oSql.AppendLine("FROM TFNMCard C WITH(NOLOCK) LEFT JOIN TFNMCardType T ON (C.FTCtyCode=T.FTCtyCode) ");
                oSql.AppendLine("WHERE FTCrdCode='" + poPara.ptCrdCode + "'");
                oCard = oDatabase.C_DAToSqlQuery<cmlTFNMCard>(oSql.ToString(), nCmdTme);

                // ตรวจสอบว่ามี รหัสบัตร หรือไม่
                if (oCard == null)
                {
                    poErrPay = new cmlResTopup();
                    ptErrCode = oMsg.tMS_RespCode712;
                    ptErrDesc = oMsg.tMS_RespDesc712;
                    return false;
                }
                else
                {
                    // ตรวจสอบวันหมดอายุรหัสบัตร
                    tNow = DateTime.Now.ToString(@"yyyy-MM-dd HH:mm:ss", new CultureInfo("en-US"));
                    tCrdExpireDate = oCard.FDCrdExpireDate.ToString("yyyy-MM-dd HH:mm:ss");
                    if (DateTime.Parse(tNow) > DateTime.Parse(tCrdExpireDate))
                    {
                        poErrPay = new cmlResTopup();
                        ptErrCode = oMsg.tMS_RespCode713;
                        ptErrDesc = oMsg.tMS_RespDesc713;
                        return false;
                    }

                    // ตรวจสอบอายุเงิน ถ้า FNCtyExpirePeriod = 0 ไม่ต้องตรวจสอบ
                    if (oCard.FNCtyExpirePeriod != 0) {

                        // ตรวจสอบกรณี ยังไม่เคยเติมเงิน ไม่ต้องตรวจสอบวัน Expire ของยอดเงิน
                       if (oCard.FDCrdLastTopup != null) {
                            switch (oCard.FNCtyExpiredType)
                            {
                                case 1:
                                     dExpireTopup = Convert.ToDateTime(oCard.FDCrdLastTopup).AddHours(Convert.ToInt32(oCard.FNCtyExpirePeriod));
                                    break;
                                case 2:
                                     dExpireTopup = Convert.ToDateTime(oCard.FDCrdLastTopup).AddDays(Convert.ToInt32(oCard.FNCtyExpirePeriod));
                                    break;
                                case 3:
                                     dExpireTopup = Convert.ToDateTime(oCard.FDCrdLastTopup).AddMonths(Convert.ToInt32(oCard.FNCtyExpirePeriod));
                                    break;
                                case 4:
                                     dExpireTopup = Convert.ToDateTime(oCard.FDCrdLastTopup).AddYears(Convert.ToInt32(oCard.FNCtyExpirePeriod));
                                    break;
                            }

                            tExpireTopup = dExpireTopup.ToString(@"yyyy-MM-dd HH:mm:ss", new CultureInfo("en-US"));

                            if (DateTime.Parse(tNow) > DateTime.Parse(tExpireTopup))
                            {
                                // ยอดเงินหมดอายุ ResetExpire
                                bResultReset = oResetExpire.C_SETbResetExpired(poPara.ptCrdCode, poPara.ptBchCode, paoSysConfig);
                                if (bResultReset == false) {
                                    poErrPay = new cmlResTopup();
                                    ptErrCode = oMsg.tMS_RespCode716;
                                    ptErrDesc = oMsg.tMS_RespDesc716;
                                    return false;
                                }
                            }
                        }

                    }
                }

                ptErrCode = "";
                ptErrDesc = "";
                poErrPay = null;
                return true;
            }
            catch (Exception oEx)
            {
                poErrPay = new cmlResTopup();
                ptErrCode = oMsg.tMS_RespCode900;
                ptErrDesc = oMsg.tMS_RespDesc900;
                return false;
            }
            finally
            {
                oFunc = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        public bool C_DATbVerifyCancelTopup(cmlReqcancelTopup poPara, List<cmlTSysConfig> paoSysConfig,
          out string ptErrCode, out string ptErrDesc, out cmlResResetAvi poErrPay)
        {
            cSP oFunc = new cSP();
            cMS oMsg = new cMS();
            cDatabase oDatabase;
            StringBuilder oSql;
            int nConTme, nCmdTme;
            cmlTFNMCard oCard = new cmlTFNMCard();
            string tCrdExpireDate = "";
            string tNow = "";
            DateTime dExpireTopup = DateTime.Now;
            cResetExpired oResetExpire = new cResetExpired();
            try
            {
                // Confuguration database.
                nConTme = 0;
                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, paoSysConfig, "002");
                nCmdTme = 0;
                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, paoSysConfig, "003");
                oDatabase = new cDatabase(nConTme);


                oSql = new StringBuilder();
                oSql.Clear();
                oSql.AppendLine("SELECT C.FTCrdCode,C.FDCrdExpireDate");
                oSql.AppendLine(",ISNULL(C.FCCrdValue-T.FCCtyDeposit,0.0000) AS FCCrdValue");
                oSql.AppendLine(",ISNULL(T.FNCtyExpirePeriod,0) AS FNCtyExpirePeriod");
                oSql.AppendLine(",ISNULL(T.FNCtyExpiredType,0) AS FNCtyExpiredType,C.FDCrdLastTopup");
                oSql.AppendLine("FROM TFNMCard C WITH(NOLOCK) LEFT JOIN TFNMCardType T ON (C.FTCtyCode=T.FTCtyCode) ");
                oSql.AppendLine("WHERE FTCrdCode='" + poPara.ptCrdCode + "'");
                oCard = oDatabase.C_DAToSqlQuery<cmlTFNMCard>(oSql.ToString(), nCmdTme);

                //ตรวจสอบว่ามี รหัสบัตร หรือไม่
                if (oCard == null)
                {
                    poErrPay = new cmlResResetAvi();
                    ptErrCode = oMsg.tMS_RespCode712;
                    ptErrDesc = oMsg.tMS_RespDesc712;
                    return false;
                }
                else
                {
                    // ตรวจสอบวันหมดอายุรหัสบัตร
                    tNow = DateTime.Now.ToString(@"yyyy-MM-dd HH:mm:ss", new CultureInfo("en-US"));
                    tCrdExpireDate = oCard.FDCrdExpireDate.ToString("yyyy-MM-dd HH:mm:ss");
                    if (DateTime.Parse(tNow) > DateTime.Parse(tCrdExpireDate))
                    {
                        poErrPay = new cmlResResetAvi();
                        ptErrCode = oMsg.tMS_RespCode713;
                        ptErrDesc = oMsg.tMS_RespDesc713;
                        return false;
                    }
                }

                ptErrCode = "";
                ptErrDesc = "";
                poErrPay = null;
                return true;
            }
            catch (Exception oEx)
            {
                poErrPay = new cmlResResetAvi();
                ptErrCode = oMsg.tMS_RespCode900;
                ptErrDesc = oMsg.tMS_RespDesc900;
                return false;
            }
            finally
            {
                oFunc = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        public bool C_DATbVerifyResetAvi(cmlReqResetAvi poPara, List<cmlTSysConfig> paoSysConfig,
          out string ptErrCode, out string ptErrDesc, out cmlResResetAvi poErrPay)
        {
            cSP oFunc = new cSP();
            cMS oMsg = new cMS();
            cDatabase oDatabase;
            StringBuilder oSql;
            int nConTme, nCmdTme;
            cmlTFNMCard oCard = new cmlTFNMCard();
            string tCrdExpireDate = "";
            string tNow = "";
            DateTime dExpireTopup = DateTime.Now;
            cResetExpired oResetExpire = new cResetExpired();
            string tExpireTopup = "";
            bool bResultReset;
            try
            {
                // Confuguration database.
                nConTme = 0;
                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, paoSysConfig, "002");
                nCmdTme = 0;
                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, paoSysConfig, "003");
                oDatabase = new cDatabase(nConTme);

                oSql = new StringBuilder();
                oSql.Clear();
                oSql.AppendLine("SELECT C.FTCrdCode,C.FDCrdExpireDate");
                oSql.AppendLine(",ISNULL(C.FCCrdValue-T.FCCtyDeposit,0.0000) AS FCCrdValue");
                oSql.AppendLine(",ISNULL(T.FNCtyExpirePeriod,0) AS FNCtyExpirePeriod");
                oSql.AppendLine(",T.FNCtyExpiredType,C.FDCrdLastTopup,ISNULL(T.FTCtyStaAlwRet,0) AS FTCtyStaAlwRet ");
                oSql.AppendLine("FROM TFNMCard C WITH(NOLOCK) LEFT JOIN TFNMCardType T ON (C.FTCtyCode=T.FTCtyCode) ");
                oSql.AppendLine("WHERE FTCrdCode='" + poPara.ptCrdCode + "'");
                oCard = oDatabase.C_DAToSqlQuery<cmlTFNMCard>(oSql.ToString(), nCmdTme);

                //ตรวจสอบว่ามี รหัสบัตร หรือไม่
                if (oCard == null)
                {
                    poErrPay = new cmlResResetAvi();
                    ptErrCode = oMsg.tMS_RespCode712;
                    ptErrDesc = oMsg.tMS_RespDesc712;
                    return false;
                }
                else
                {
                    // ตรวจสอบประเภทบัตรคืนได้หรือไม่
                    if (oCard.FTCtyStaAlwRet == "1")  // คืนได้
                    {
                        // ตรวจสอบวันหมดอายุรหัสบัตร
                        tNow = DateTime.Now.ToString(@"yyyy-MM-dd HH:mm:ss", new CultureInfo("en-US"));
                        tCrdExpireDate = oCard.FDCrdExpireDate.ToString("yyyy-MM-dd HH:mm:ss");
                        if (DateTime.Parse(tNow) > DateTime.Parse(tCrdExpireDate))
                        {
                            poErrPay = new cmlResResetAvi();
                            ptErrCode = oMsg.tMS_RespCode713;
                            ptErrDesc = oMsg.tMS_RespDesc713;
                            return false;
                        }

                        // ตรวจสอบอายุเงิน ถ้า FNCtyExpirePeriod = 0 ไม่ต้องตรวจสอบ
                        if (oCard.FNCtyExpirePeriod != 0)
                        {

                            // ตรวจสอบกรณี ยังไม่เคยเติมเงิน ไม่ต้องตรวจสอบวัน Expire ของยอดเงิน
                            if (oCard.FDCrdLastTopup != null)
                            {
                                switch (oCard.FNCtyExpiredType)
                                {
                                    case 1:
                                        dExpireTopup = Convert.ToDateTime(oCard.FDCrdLastTopup).AddHours(Convert.ToInt32(oCard.FNCtyExpirePeriod));
                                        break;
                                    case 2:
                                        dExpireTopup = Convert.ToDateTime(oCard.FDCrdLastTopup).AddDays(Convert.ToInt32(oCard.FNCtyExpirePeriod));
                                        break;
                                    case 3:
                                        dExpireTopup = Convert.ToDateTime(oCard.FDCrdLastTopup).AddMonths(Convert.ToInt32(oCard.FNCtyExpirePeriod));
                                        break;
                                    case 4:
                                        dExpireTopup = Convert.ToDateTime(oCard.FDCrdLastTopup).AddYears(Convert.ToInt32(oCard.FNCtyExpirePeriod));
                                        break;
                                }

                                tExpireTopup = dExpireTopup.ToString(@"yyyy-MM-dd HH:mm:ss", new CultureInfo("en-US"));

                                if (DateTime.Parse(tNow) > DateTime.Parse(tExpireTopup))
                                {
                                    // ยอดเงินหมดอายุ ResetExpire
                                    bResultReset = oResetExpire.C_SETbResetExpired(poPara.ptCrdCode, poPara.ptBchCode, paoSysConfig);
                                    if (bResultReset == false)
                                    {
                                        poErrPay = new cmlResResetAvi();
                                        ptErrCode = oMsg.tMS_RespCode716;
                                        ptErrDesc = oMsg.tMS_RespDesc716;
                                        return false;
                                    }
                                }
                            }

                        }


                    }
                    else
                    {
                        // คืนไม่ได้ ResetExpire
                        tExpireTopup = dExpireTopup.ToString(@"yyyy-MM-dd HH:mm:ss", new CultureInfo("en-US"));
                        if (DateTime.Parse(tNow) > DateTime.Parse(tExpireTopup))
                        {
                            // ยอดเงินหมดอายุ ResetExpire
                            bResultReset = oResetExpire.C_SETbResetExpired(poPara.ptCrdCode, poPara.ptBchCode, paoSysConfig);
                            if (bResultReset == false)
                            {
                                poErrPay = new cmlResResetAvi();
                                ptErrCode = oMsg.tMS_RespCode716;
                                ptErrDesc = oMsg.tMS_RespDesc716;
                                return false;
                            }
                        }
                        poErrPay = new cmlResResetAvi();
                        ptErrCode = oMsg.tMS_RespCode717;
                        ptErrDesc = oMsg.tMS_RespDesc717;
                        return false;
                    }
                }

                ptErrCode = "";
                ptErrDesc = "";
                poErrPay = null;
                return true;
            }
            catch (Exception oEx)
            {
                poErrPay = new cmlResResetAvi();
                ptErrCode = oMsg.tMS_RespCode900;
                ptErrDesc = oMsg.tMS_RespDesc900;
                return false;
            }
            finally
            {
                oFunc = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

    }
}