﻿using API2Wallet.Class.Standard;
using API2Wallet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace API2Wallet.Class.ResetExpired
{
    public class cResetExpired
    {

        public bool C_SETbResetExpired(string ptCrdCode,string ptBchCode, List<cmlTSysConfig> paoSysConfig)
        {
            bool bResult = true;
            cDatabase oDatabase;
            StringBuilder oSql = new StringBuilder();
            cmlTCNMComp oComp = new cmlTCNMComp();
            int nConTme, nCmdTme, nRowEff;
            cSP oFunc = new cSP();
            cmlTFNMCard oCard = new cmlTFNMCard();
            try
            {
                nConTme = 0;
                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, paoSysConfig, "002");
                nCmdTme = 0;
                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, paoSysConfig, "003");
                oDatabase = new cDatabase(nConTme);

                // Get Company Code
                oSql.Clear();
                oSql.AppendLine("SELECT TOP 1 [FTCmpCode],[FTCmpTel],[FTCmpFax]");
                oSql.AppendLine(",[FTBchcode],[FTCmpWhsInOrEx],[FTCmpRetInOrEx]");
                oSql.AppendLine(",[FTCmpEmail],[FTRteCode],[FTVatCode]");
                oSql.AppendLine(" FROM TCNMComp WITH (NOLOCK) ");
                oComp = oDatabase.C_DAToSqlQuery<cmlTCNMComp>(oSql.ToString(), nCmdTme);

                // Get value Card 
                oSql.Clear();
                oSql.AppendLine("SELECT TOP 1 ISNULL(FCCrdValue,0) AS FCCrdValue");
                oSql.AppendLine(" FROM TFNMCard WITH (NOLOCK) WHERE FTCrdCode='" + ptCrdCode + "'");
                oCard = oDatabase.C_DAToSqlQuery<cmlTFNMCard>(oSql.ToString(), nCmdTme);

                oSql.Clear();
                oSql.AppendLine("BEGIN TRANSACTION ");
                oSql.AppendLine("  SAVE TRANSACTION SavePay ");
                oSql.AppendLine("  BEGIN TRY ");

                // Insert transection Sale
                oSql.AppendLine("     INSERT INTO TFNTCrdTopUp WITH(ROWLOCK)");
                oSql.AppendLine("     (");
                oSql.AppendLine("	  FTBchCode,FTTxnDocType,FTCrdCode,");
                oSql.AppendLine("	  FDTxnDocDate,FTBchCodeRef,FCTxnValue,");
                oSql.AppendLine("	  FTTxnStaPrc");
                oSql.AppendLine("     )");
                oSql.AppendLine("     VALUES");
                oSql.AppendLine("     (");
                oSql.AppendLine("	  '" + oComp.FTBchcode + "','10','" + ptCrdCode + "',");
                oSql.AppendLine("	  GETDATE(),'" + ptBchCode + "','" + oCard.FCCrdValue + "',");
                oSql.AppendLine("	  '1'");
                oSql.AppendLine("     )");

                // Update Master Card
                oSql.AppendLine("     UPDATE TFNMCard WITH (ROWLOCK) SET ");
                oSql.AppendLine("     FCCrdValue=0,");
                oSql.AppendLine("     FDCrdResetDate=GETDATE()");
                oSql.AppendLine("     WHERE FTCrdCode='" + ptCrdCode + "'");
                oSql.AppendLine("     COMMIT TRANSACTION SavePay");
                oSql.AppendLine("  END TRY");

                oSql.AppendLine("  BEGIN CATCH");
                oSql.AppendLine("   ROLLBACK TRANSACTION SavePay");
                oSql.AppendLine("  END CATCH");
                nRowEff = oDatabase.C_DATnExecuteSql(oSql.ToString(), nCmdTme);

                if (nRowEff == 0)
                {
                    bResult = false;
                }
                return bResult;
            }
            catch (Exception oEx)
            {
                bResult = false;
                return bResult;
            }
            finally
            {

            }
        }


    }
}
