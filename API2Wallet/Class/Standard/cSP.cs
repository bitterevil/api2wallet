﻿using API2Wallet.EF;
using API2Wallet.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http.ModelBinding;

namespace API2Wallet.Class.Standard
{
    public class cSP
    {
        public const int SP_nCmdTimeOut = 300;
        
        public DataTable SP_GEToDataQuery(string ptSql, ref Exception poException)
        {
            poException = null;
            System.Data.SqlClient.SqlConnection oDbCon = new System.Data.SqlClient.SqlConnection();
            System.Data.SqlClient.SqlDataAdapter oDbAdt;
            DataTable oDbTbl = new DataTable();
            try
            {
                oDbCon.ConnectionString = SP_GETtConn();
                oDbCon.Open();
                oDbTbl = new DataTable();
                oDbAdt = new System.Data.SqlClient.SqlDataAdapter(ptSql, oDbCon);
                oDbAdt.SelectCommand.CommandTimeout = SP_nCmdTimeOut;
                oDbAdt.Fill(oDbTbl);

                return oDbTbl;
            }
            catch (Exception ex)
            {
                poException = ex;
                return oDbTbl;
            }
            finally
            {
                if (oDbCon.State == ConnectionState.Open)
                    oDbCon.Close();
                oDbCon = null/* TODO Change to default(_) if this is not a reference type */;
                oDbAdt = null/* TODO Change to default(_) if this is not a reference type */;
                oDbTbl = null/* TODO Change to default(_) if this is not a reference type */;
            }
        }

        /// <summary>
        /// Get Connection
        /// </summary>
        /// <returns>*CH 14-12-2016</returns>
        public string SP_GETtConn()
        {
            string tConn = "";
            try
            {
                tConn = new AdaFCEntities().Database.Connection.ConnectionString.ToString();
            }
            // Dim atConn As String() = ConfigurationManager.ConnectionStrings("AdaAccFCEntities").ConnectionString.Split("=")
            // If atConn.Count = 10 Then
            // tConn = Right(atConn(3), atConn(3).Length - 1)
            // tConn &= "=" & atConn(4)
            // tConn &= "=" & atConn(5)
            // tConn &= "=" & atConn(6)
            // tConn &= "=" & atConn(7)
            // tConn &= "=" & atConn(8)
            // tConn &= "=" & Left(atConn(9), atConn(9).Length - 1)
            // End If
            catch (Exception ex)
            {
            }
            return tConn;
        }

        /// <summary>
        ///     Validate model.
        /// </summary>
        /// 
        /// <param name="ptModelErr">ref Error message.</param>
        /// <param name="poModelState">Parameter model.</param>
        /// 
        /// <returns>
        ///     true : validate pass.<br/>
        ///     false : validate false.
        /// </returns>
        public bool SP_CHKbParaModel(ref string ptModelErr, ModelStateDictionary poModelState)
        {
            try
            {
                if (poModelState.IsValid)
                {
                    // Validate pass.
                    return true;
                }
                else
                {
                    // Validate false.
                    IEnumerable<string> atErrList = from oState in poModelState.Values
                                                    from oError in oState.Errors
                                                    where !string.IsNullOrEmpty(oError.ErrorMessage)
                                                    select oError.ErrorMessage;

                    ptModelErr = string.Join("|", atErrList);
                }
            }
            catch (Exception)
            {

            }

            return false;
        }

        /// <summary>
        ///     Load configuration.
        /// </summary>
        /// 
        /// <returns>
        ///     Configuration.
        /// </returns>
        public List<cmlTSysConfig> SP_SYSaLoadConfiguration()
        {
            cDatabase oDatabase;
            cCacheInMem oCache = new cCacheInMem();
            List<cmlTSysConfig> aoSysConfig;
            StringBuilder oSql;
           // Nullable<int> nCacheTme;
            Nullable<int> nCacheTme = 1;
            try
            {
                //// Check cache.
                //if (oCache.C_CAHbCheckKey("TSysConfig"))
                //{
                //    aoSysConfig = oCache.C_CAHoGetValue<List<cmlTSysConfig>>("TSysConfig");
                //}
                //else
                //{
                //    oSql = new StringBuilder();
                //    oSql.AppendLine("SELECT FTSysCode, FTSysSeq,FTSysStaUsrValue,FTSysStaUsrRef,FTSysKey ");
                //    oSql.AppendLine("FROM TSysConfig WITH(NOLOCK) ");
                //    oSql.AppendLine("WHERE FTSysCode='PAPI2Wellet' ");
                //    oSql.AppendLine("ORDER BY FTSysSeq");
                //    oDatabase = new cDatabase();
                //    aoSysConfig = oDatabase.C_DATaSqlQuery<cmlTSysConfig>(oSql.ToString());
                //    if (aoSysConfig != null && aoSysConfig.Count > 0)
                //    {
                //        ////nCacheTme = null;
                //        ////SP_DATxGetConfigurationFromMem(ref nCacheTme, 1, aoSysConfig, "005");

                //        // Add cache.
                //        oCache.C_CAHbAddValue("TSysConfig", aoSysConfig, nCacheTme.GetValueOrDefault());
                //    }
                //}


                // Check cache.
                    oSql = new StringBuilder();
                    oSql.AppendLine("SELECT FTSysCode, FTSysSeq,FTSysStaUsrValue,FTSysStaUsrRef,FTSysKey ");
                    oSql.AppendLine("FROM TSysConfig WITH(NOLOCK) ");
                    oSql.AppendLine("WHERE FTSysCode='PAPI2Wellet' ");
                    oSql.AppendLine("ORDER BY FTSysSeq");
                    oDatabase = new cDatabase();
                    aoSysConfig = oDatabase.C_DATaSqlQuery<cmlTSysConfig>(oSql.ToString());

                    //if (aoSysConfig != null && aoSysConfig.Count > 0)
                    //{
                    //    oCache.C_CAHbAddValue("TSysConfig", aoSysConfig, nCacheTme.GetValueOrDefault());
                    //}
                
                return aoSysConfig;
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                oDatabase = null;
                oCache = null;
                oSql = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }

            
        }

        /// <summary>
        ///     Get configuration from memory.
        /// </summary>
        /// 
        /// <typeparam name="T">Type data user value.</typeparam>
        /// <param name="poUsrValue">ref Data user value.</param>
        /// <param name="poDefValue">Default data user value.</param>
        /// <param name="paoSysConfig">Valiable TSysConfig in memory.</param>
        /// <param name="ptSysSeq">Sequence.</param>
        public void SP_DATxGetConfigurationFromMem<T>(ref T poUsrValue, T poDefValue, List<cmlTSysConfig> paoSysConfig, string ptSysSeq)
        {
            try
            {
                // UsrValue.
                try
                {
                    poUsrValue = (T)Convert.ChangeType(paoSysConfig.Where(
                        oItem => string.Equals(oItem.FTSysSeq, ptSysSeq)).Select(oItem => oItem.FTSysStaUsrValue).FirstOrDefault(), typeof(T));
                }
                catch (Exception)
                {
                    poUsrValue = poDefValue;
                }
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        ///     Check allow to user function in range time.
        /// </summary>
        /// 
        /// <param name="paoSysConfig">Configuration.</param>
        /// 
        /// <returns>
        ///     true : Allow to use.<br/>
        ///     false : Not allow to use.
        /// </returns>
        public bool SP_CHKbAllowRangeTime(List<cmlTSysConfig> paoSysConfig)
        {
            TimeSpan oTmeStart, oTmeEnd, oTmeNow;
            string tTmeStart, tTmeEnd, tTmeNow;

            try
            {
                //oTmeNow = DateTime.Now.TimeOfDay;
                //tTmeNow = oTmeNow.ToString("hh\\:mm");
                //tTmeStart = "";
                //tTmeEnd = "";

                //SP_DATxGetConfigurationFromMem<string, string>(ref tTmeStart, tTmeNow, ref tTmeEnd, tTmeNow, paoSysConfig, "002");

                //oTmeStart = TimeSpan.Parse(tTmeStart);
                //oTmeEnd = TimeSpan.Parse(tTmeEnd);

                //// Check time use function.
                //if ((oTmeNow >= oTmeStart) && (oTmeNow <= oTmeEnd))
                //{
                //    return true;
                //}

                // Default return True ไปก่อน
                return true;
            }
            catch (Exception)
            {

            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }

            return false;
        }

        /// <summary>
        ///     Check key API.
        /// </summary>
        /// 
        /// <param name="ptKeyApi">ref Key API.</param>
        /// <param name="ptFuncName">Function name.</param>
        /// <param name="poHttpContext">HttpContext.</param>
        /// <param name="paoSysConfig">Valiable TSysConfig in memory.</param>
        /// 
        /// <returns>
        ///     true : Verify key API pass.
        ///     false : Verify key API false.
        /// </returns>
        public bool SP_CHKbKeyApi(ref string ptKeyApi, string ptFuncName, HttpContext poHttpContext, List<cmlTSysConfig> paoSysConfig)
        {
            NameValueCollection oReqHeaders;
          //  cCacheInMem oCache;
           // Nullable<int> nCacheTme;
            string tXKeyApi, tKeyApi ;

            try
            {
                //oReqHeaders = poHttpContext.Request.Headers;
                //tXKeyApi = oReqHeaders.Get("X-Api-Key");

                //// ถ้ามี Key Api ส่งมาใน Header
                //if (!string.IsNullOrEmpty(tXKeyApi))
                //{
                //    oCache = new cCacheInMem();
                //    tCacheKey = tXKeyApi + "|" + ptFuncName;

                //    // Check cache.
                //    if (oCache.C_CAHbCheckKey(tCacheKey))
                //    {
                //        ptKeyApi = tXKeyApi;
                //        return oCache.C_CAHoGetValue<bool>(tCacheKey);
                //    }
                //    else
                //    {
                //        tKeyApi = "";
                //        SP_DATxGetConfigurationFromMem<string>(ref tKeyApi, "", paoSysConfig, "001");

                //        if (!string.IsNullOrEmpty(tKeyApi))
                //        {
                //            // Add cache.
                //            nCacheTme = null;
                //            SP_DATxGetConfigurationFromMem(ref nCacheTme, 1, paoSysConfig, "005");

                //            oCache.C_CAHbAddValue("tCacheKey", true, nCacheTme.GetValueOrDefault());

                //            return true;
                //        }
                //    }
                //}

                oReqHeaders = poHttpContext.Request.Headers;
                tXKeyApi = oReqHeaders.Get("X-Api-Key");

                tKeyApi = "";
                SP_DATxGetConfigurationFromMem<string>(ref tKeyApi, "", paoSysConfig, "001");
                if (tXKeyApi == tKeyApi)
                {
                    return true;
                }
                else
                {
                    return false;
                }


            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                oReqHeaders = null;
            //    oCache = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }
    }
}