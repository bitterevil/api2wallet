﻿using API2Wallet.Class.Standard;
using API2Wallet.Models;
using API2Wallet.Models.WebService.Request.Pay;
using API2Wallet.Models.WebService.Response.Pay;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace API2Wallet.Class.Pay
{
    public class cPay
    {

        public bool C_DATbVerifyInsItemPaytxn(cmlReqPayTxn  poPara, List<cmlTSysConfig> paoSysConfig,
           out string ptErrCode, out string ptErrDesc, out cmlResPayTxn poErrPay)
        {
            cSP oFunc = new cSP();
            cMS oMsg = new cMS();
            cDatabase oDatabase;
            StringBuilder oSql;
            int nConTme, nCmdTme;
            cmlTFNMCard oCard = new cmlTFNMCard();
            string tCrdExpireDate = "";
            string tNow = "";
            try
            {
                // Confuguration database.
                nConTme = 0;
                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, paoSysConfig, "002");
                nCmdTme = 0;
                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, paoSysConfig, "003");
                oDatabase = new cDatabase(nConTme);

                
                oSql = new StringBuilder();
                oSql.Clear();
                oSql.AppendLine("SELECT C.FTCrdCode,C.FDCrdExpireDate,ISNULL(C.FCCrdValue-T.FCCtyDeposit,0.0000) AS FCCrdValue ");
                oSql.AppendLine("FROM TFNMCard C WITH(NOLOCK) LEFT JOIN TFNMCardType T ON (C.FTCtyCode=T.FTCtyCode) ");
                oSql.AppendLine("WHERE FTCrdCode='" + poPara.ptCrdCode + "'");
                oCard = oDatabase.C_DAToSqlQuery<cmlTFNMCard>(oSql.ToString(), nCmdTme);

                // ตรวจสอบว่ามี รหัสบัตร หรือไม่
                if (oCard == null)
                {
                    poErrPay = new cmlResPayTxn();
                    ptErrCode = oMsg.tMS_RespCode712;
                    ptErrDesc = oMsg.tMS_RespDesc712;
                    return false;
                } else {
                    // ตรวจสอบวันหมดอายุรหัสบัตร
                    tNow = DateTime.Now.ToString(@"yyyy-MM-dd", new CultureInfo("en-US"));
                    tCrdExpireDate = oCard.FDCrdExpireDate.ToString("yyyy-MM-dd");
                    if (DateTime.Parse(tNow) > DateTime.Parse(tCrdExpireDate))
                    {
                        poErrPay = new cmlResPayTxn();
                        ptErrCode = oMsg.tMS_RespCode713;
                        ptErrDesc = oMsg.tMS_RespDesc713;
                        return false;
                    }

                    //เช็คยอดคงเหลือในบัตร
                    if (poPara.pcTxnValue > Convert.ToDouble(oCard.FCCrdValue)) {
                        poErrPay = new cmlResPayTxn();
                        ptErrCode = oMsg.tMS_RespCode714;
                        ptErrDesc = oMsg.tMS_RespDesc714;
                        return false;
                    }
                }

                ptErrCode = "";
                ptErrDesc = "";
                poErrPay = null;
                return true;
            }
            catch (Exception)
            {
                poErrPay = new cmlResPayTxn();
                ptErrCode = oMsg.tMS_RespCode900;
                ptErrDesc = oMsg.tMS_RespDesc900;
                return false; 
            }
            finally
            {
                oFunc = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        public bool C_DATbVerifyInsItemCancelPaytxn(cmlReqCancelpayTxn poPara, List<cmlTSysConfig> paoSysConfig,
           out string ptErrCode, out string ptErrDesc, out cmlRescancelPayTxn poErrPay)
        {
            cSP oFunc = new cSP();
            cMS oMsg = new cMS();
            cDatabase oDatabase;
            StringBuilder oSql;
            int nConTme, nCmdTme;
            cmlTFNMCard oCard = new cmlTFNMCard();
            string tCrdExpireDate = "";
            string tNow = "";
            cmlTFNTCrdSale oCrdsale = new cmlTFNTCrdSale();
            try
            {
                // Confuguration database.
                nConTme = 0;
                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, paoSysConfig, "002");
                nCmdTme = 0;
                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, paoSysConfig, "003");
                oDatabase = new cDatabase(nConTme);

                oSql = new StringBuilder();
                oSql.Clear();
                oSql.AppendLine("SELECT C.FTCrdCode,C.FDCrdExpireDate,ISNULL(C.FCCrdValue-T.FCCtyDeposit,0.0000) AS FCCrdValue ");
                oSql.AppendLine("FROM TFNMCard C WITH(NOLOCK) LEFT JOIN TFNMCardType T ON (C.FTCtyCode=T.FTCtyCode) ");
                oSql.AppendLine("WHERE FTCrdCode='" + poPara.ptCrdCode + "'");
                oCard = oDatabase.C_DAToSqlQuery<cmlTFNMCard>(oSql.ToString(), nCmdTme);

                // ตรวจสอบว่ามี รหัสบัตร หรือไม่
                if (oCard == null)
                {
                    poErrPay = new cmlRescancelPayTxn();
                    ptErrCode = oMsg.tMS_RespCode712;
                    ptErrDesc = oMsg.tMS_RespDesc712;
                    return false;
                }
                else
                {
                    // ตรวจสอบวันหมดอายุรหัสบัตร
                    tNow = DateTime.Now.ToString(@"yyyy-MM-dd", new CultureInfo("en-US"));
                    tCrdExpireDate = oCard.FDCrdExpireDate.ToString("yyyy-MM-dd");
                    if (DateTime.Parse(tNow) > DateTime.Parse(tCrdExpireDate))
                    {
                        poErrPay = new cmlRescancelPayTxn();
                        ptErrCode = oMsg.tMS_RespCode713;
                        ptErrDesc = oMsg.tMS_RespDesc713;
                        return false;
                    }

                }

                // Get สถานะเคยยกเลิกแล้วหรือยัง
                oSql.Clear();
                oSql.AppendLine("SELECT ISNULL(FTTxnStaCancel,0) AS FTTxnStaCancel FROM TFNTCrdSale");
                oSql.AppendLine("WHERE FNTxnID=" + poPara.pnTxnID + "");
                oCrdsale = oDatabase.C_DAToSqlQuery<cmlTFNTCrdSale>(oSql.ToString(), nCmdTme);

                if (oCrdsale.FTTxnStaCancel == "1")
                {
                    poErrPay = new cmlRescancelPayTxn();
                    ptErrCode = oMsg.tMS_RespCode715;
                    ptErrDesc = oMsg.tMS_RespDesc715;
                    return false;
                }
                ptErrCode = "";
                ptErrDesc = "";
                poErrPay = null;
                return true;
            }
            catch (Exception)
            {
                poErrPay = new cmlRescancelPayTxn();
                ptErrCode = oMsg.tMS_RespCode900;
                ptErrDesc = oMsg.tMS_RespDesc900;
                return false;
            }
            finally
            {
                oFunc = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

    }
}