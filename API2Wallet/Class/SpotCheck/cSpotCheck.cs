﻿using API2Wallet.Class.Standard;
using API2Wallet.Models;
using API2Wallet.Models.WebService.Request.SpotCheck;
using API2Wallet.Models.WebService.Response.SpotCheck;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace API2Wallet.Class.SpotCheck
{
    public class cSpotCheck
    {
        public bool C_DATbVerifySpotCheck(cmlReqSpotChk poPara, List<cmlTSysConfig> paoSysConfig,
           out string ptErrCode, out string ptErrDesc, out cmlResSpotChk poErrPay)
        {
            cSP oFunc = new cSP();
            cMS oMsg = new cMS();
            cDatabase oDatabase;
            StringBuilder oSql;
            int nConTme, nCmdTme;
            cmlTFNMCard oCard = new cmlTFNMCard();
            DateTime dExpireTopup = DateTime.Now;
            try
            {
                // Confuguration database.
                nConTme = 0;
                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nConTme, cCS.nCS_ConTme, paoSysConfig, "002");
                nCmdTme = 0;
                oFunc.SP_DATxGetConfigurationFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, paoSysConfig, "003");
                oDatabase = new cDatabase(nConTme);

                oSql = new StringBuilder();
                oSql.Clear();
                oSql.AppendLine("SELECT C.FTCrdCode");
                oSql.AppendLine(",ISNULL(C.FCCrdValue-T.FCCtyDeposit,0.0000) AS FCCrdValue");
                oSql.AppendLine(",ISNULL(T.FNCtyExpirePeriod,0) AS FNCtyExpirePeriod");
                oSql.AppendLine(",T.FNCtyExpiredType");
                oSql.AppendLine("FROM TFNMCard C WITH(NOLOCK) LEFT JOIN TFNMCardType T ON (C.FTCtyCode=T.FTCtyCode) ");
                oSql.AppendLine("WHERE FTCrdCode='" + poPara.ptCrdCode + "'");
                oCard = oDatabase.C_DAToSqlQuery<cmlTFNMCard>(oSql.ToString(), nCmdTme);

                // ตรวจสอบว่ามี รหัสบัตร หรือไม่
                if (oCard == null)
                {
                    poErrPay = new cmlResSpotChk();
                    ptErrCode = oMsg.tMS_RespCode712;
                    ptErrDesc = oMsg.tMS_RespDesc712;
                    return false;
                }
               
                ptErrCode = "";
                ptErrDesc = "";
                poErrPay = null;
                return true;
            }
            catch (Exception oEx)
            {
                poErrPay = new cmlResSpotChk();
                ptErrCode = oMsg.tMS_RespCode900;
                ptErrDesc = oMsg.tMS_RespDesc900;
                return false;
            }
            finally
            {
                oFunc = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }
    }
}